<%-- 
    Document   : inventory_management
    Created on : Jul 21, 2013, 2:16:58 PM
    Author     : US083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="to_fba/CommonHeader.jsp" %> &gt; Inventory Management
        <ol class="list">
            <li><a href="incoming_to_shelf.jsp">To The Shelf (From distributor to our warehouse).</a></li>
            <li><a href="outgoing.jsp">From The Shelf (To fba or to customer) </a></li>
            <li><a href="inventory_query.jsp">Check Balance Inventory for an item/Bin, Audit an Item/Bin </a></li>
            <li><a href="editprice.jsp">Edit Price</a></li>
            <li><a href="/HB5/ManualQtyUpdate">Manual Qty Update</a></li>
            <li><a href="/HB5/FinalPackingServlet">Final Packing Quantity Update</a></li>
            <li><a href="/HB5/ManualQtyUpdate">Update Actual Units in Bin</a></li>   
            <li><a href="/HB5/GoogelShoppingUrl">Populate Google Shopping URL for each product being sold in HB</a></li>
            <li><a href="/HB5/ExpiryDates.jsp">Expiry Dates</a></li>
            <li><a href="/HB5/SearchOrderByProduct.jsp">Search Order</a></li>
            <li><a href="/HBPublic/refund.jsp"> Manage Customer Returns</a></li>
            <li><a href="/HB5/StoreCustomerPacking"> Track Customer Orders Packing</a></li>
        </ol>
        
    </body>
</html>
