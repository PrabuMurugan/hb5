<%-- 
    Document   : BoxDetails
    Created on : Sep 13, 2013, 11:49:05 PM
    Author     : debasish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="u" uri="http://www.tajplaza.com/util" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Box Details</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>
        <h1>Box Details</h1>
        <u:messages/>
        <c:forEach items="${boxes}" var="box">
            <div> Box ID : 
                <c:if test="${box.value[0].sent}">
                    <div style="color:green">
                </c:if>
                <c:if test="${!(box.value[0].sent)}">
                    <div style="color:red">
                </c:if>
                ${box.key}
                    </div>
                <br/>
                <table border="1" class="details">
                    <tr>
                        <td>
                            SKU
                        </td>
                        <td>
                            FNSKU
                        </td>
                        <td>
                            Item Name
                        </td>
                        <td>
                            Quantity
                        </td>
                        <td>
                            Actual Weight
                        </td>
                    </tr>
                    <c:forEach items="${box.value}" var="item">
                        <tr>
                            <td>
                                ${item.sku}
                            </td>
                            <td>
                                ${item.fnsku}
                            </td>
                            <td>
                                ${item.name}
                            </td>
                            <td>
                                ${item.quantity}
                            </td>

                            <td>
                                ${item.weight}
                            </td>
                        </tr>

                    </c:forEach>
                </table>
            </div>
            <br/>
        </c:forEach>


    </body>
</html>
