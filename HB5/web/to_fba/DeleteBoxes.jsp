<%-- 
    Document   : DeleteBoxes
    Created on : Sep 11, 2013, 9:28:04 PM
    Author     : debasish
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="u" uri="http://www.tajplaza.com/util" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="constants.ActionConstants" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Boxes</title>
       
         <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
          <script>
            
            function getData(e) {
                if(e.keyCode === 13) {
                    
                 var el = document.activeElement;
                 if (el && (el.tagName.toLowerCase() === 'input' && el.type === 'text' ||
                    el.tagName.toLowerCase() === 'textarea')) {
                    document.deleteBoxForm.box[el.tabIndex+1].focus();
                  }
                }
             }
             
             function submitForm() {
                  document.deleteBoxForm.submit();
             }
        </script>
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt;Delete Boxes
        <h1>Archive Boxes</h1>
        <span class="description">Archive your boxes so that the same names can be used again</span>
        
        <u:messages/>
        <form action="${pageContext.request.contextPath}/ArchiveBoxes" method="post" name="deleteBoxForm">
            <input type="hidden" name="actionid"  value="<%=ActionConstants.ARCHIVE_BOX%>" >
            <div>
                <input type="button" value="Delete Boxes" onclick="submitForm()"/>
            </div>
            <table>
                <c:forEach begin="0" end="29" var="index">
                    <tr>
                        <td>
                            <input type="text" id="box" name="box" onkeyup="getData(event)" tabindex="${index}">
                        </td>
                    </td>
                </c:forEach>
            </table>
            
        </form>
    </body>
</html>
