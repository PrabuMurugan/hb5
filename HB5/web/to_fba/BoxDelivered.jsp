<%-- 
    Document   : BoxDelivered
    Created on : Sep 17, 2013, 12:25:12 AM
    Author     : debasish
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="u" uri="http://www.tajplaza.com/util" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt;Weekly Report - Boxes/Labels done
        <h1>Boxes Prepared</h1>
        <u:messages/>
        <u:dateSelector baseURI="${pageContext.request.contextPath}/BoxesPreparedByDateServlet" timestampParameter="ts" dataList="boxList" rowHeaderProperty="name" colValueProperty="count" rowValueParameter="boxInit"/>
        <br/>
        <h1>Orders Shipped</h1>
        <u:dateSelector baseURI="${pageContext.request.contextPath}/BoxesPreparedByDateServlet" timestampParameter="ts" dataList="orderList" rowHeaderProperty="name" colValueProperty="count" rowValueParameter="boxInit"/>
        
        
        <h1>FBA labels prepared</h1>
        <u:dateSelector baseURI="${pageContext.request.contextPath}/ViewFBAbyDate" timestampParameter="ts" dataList="fbaList" rowHeaderProperty="name" colValueProperty="count" rowValueParameter="userName"/>
    </body>
</html>
