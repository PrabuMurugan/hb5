<%-- 
    Document   : BoxDetails
    Created on : Sep 13, 2013, 11:49:05 PM
    Author     : debasish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="u" uri="http://www.tajplaza.com/util" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Box Details</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt;View boxes prepared by date
        <h1>Box Details</h1>
        <form action="${pageContext.request.contextPath}/BoxDetailsSave" method="post">
            <u:messages/>
            Box ID : ${boxID}
            <input type="hidden" name="box" value="${boxID}" />
            <br/>
            <input type="submit" value="Submit"/>
            <table border="1">
                <tr>
                    <td>
                        SKU
                    </td>
                    <td>
                        FNSKU
                    </td>
                    <td>
                        Item Name
                    </td>
                    <td>
                        Quantity
                    </td>
                    <td>
                        Expected Weight
                    </td>
                    <td>
                        Actual Weight
                    </td>
                </tr>
                <c:forEach items="${items}" var="item" varStatus="varStat">
                    <tr>
                        <td>
                            ${item.sku}
                        </td>
                        <td>
                            <input type="hidden" name="fnsku-${varStat.index}" value="${item.fnsku}"/>
                            ${item.fnsku}
                        </td>
                        <td>
                            ${item.name}
                        </td>
                        <td>
                            <input type='text' name='quantity-${varStat.index}' value='${item.quantity}' />
                        </td>
                        <td>
                            ${u:formatDouble(item.expectedWeight)}
                        </td>
                        <td>
                            <input type='text' name='weight-${varStat.index}' value='${item.weight}' />
                        </td>
                    </tr>

                </c:forEach>
            </table>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
