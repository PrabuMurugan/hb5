<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="jquery.autotab-1.1b.js"></script>

        <script type="text/javascript">
            function stopRKey(evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) {
                    if ($('input:focus').length > 0) {
                        var focusable = $('input').filter(':visible');

                        focusable.eq(focusable.index($('input:focus')[0]) + 1).focus();
                        //$('input:focus:first-child').next('input').focus(); 
                    }

                    return false;
                }
            }



            $(document).ready(function() {
                $(document).keypress(stopRKey);
                $(document).find(".progress").hide();
                //debugger;
                $('.box').get(0).focus();
                $('.box').blur(function(evt) {
                    var elem_id = this.id;
                    var boxLabel = this.value;
                    if(boxLabel==null || boxLabel==""){
                        return;
                    }
                    $.ajax({
                        url: "../GetBoxWeightFromBoxLabel?box=" + boxLabel,
                        elem_id: elem_id,                        
                        beforeSend: function(hdr){
                            var progress_name = $("#" + this.elem_id.replace("box", "progress"));
                            var boxLabel = this.value;
                            
                            progress_name.show();
                            
                        }

                    }).done(function(data) {

                        var weight_name = $("#" + this.elem_id.replace("box", "weight"))[0];
                        var progress_name = $("#" + this.elem_id.replace("box", "progress"));
                        var data_items = data.split("|"); 
                        weight_name.innerHTML = data_items[0];
                        
                        if(data_items[1] != "null"){
                            var notification = $("#" + this.elem_id.replace("box", "notification"))[0];
                            notification.innerHTML = "Box Already Sent On Pallet : "+data_items[1];
                            $("#" + this.elem_id.replace("box", "notification")).show();
                        }else{
                            $("#" + this.elem_id.replace("box", "notification")).hide();
                        }
                        
                        $(weight_name).css("color", "black");
                        progress_name.hide();
                        
                        //to_speech = data.substring(variable.indexOf('||'));

                    }).error(function(data){
                        var weight_name = $("#" + this.elem_id.replace("box", "weight"))[0];
                        var progress_name = $("#" + this.elem_id.replace("box", "progress"));
                        weight_name.innerHTML = "Error";
                        $(weight_name).css("color", "red");
                        progress_name.hide();
                        $("#" + this.elem_id.replace("box", "notification")).hide();

                    });
                });

        });
 
        </script>
    </head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    <body onload="document.forms[0].reset()">
        <br/>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt; Prepare Pallet

        <form action="../PrepareFBAPallet" id="store" method="post">
            <input type="submit" value="Submit" />
            <br/>

            <table width="100%">
                <thead>

                <td>
                    <h1>Box</h1>
                </td>
                <td>
                    <h1>Weight</h1>
                </td>

                </thead>
                <tbody id="data">
                    <c:forEach begin="0" end="299" var="index">
                        <tr class="row">
                            <td width="20%">
                                <input type="text"  id="box-${index}" name="box" class="box" data-to-speak="BOX" maxlength="15" size="15" value="" />             
                            </td>
                            <td>
                                <span id="weight-${index}" class="weight"></span>
                                <progress id="progress-${index}" max="100" class="progress"></progress>
                                <span style="color: red;display: none;margin-left: 5px;" id="notification-${index}"></span>
                            </td>

                        </tr>
                    </c:forEach>
                <tbody>
            </table>



        </form>




    </body>


</html>