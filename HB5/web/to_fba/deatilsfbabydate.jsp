
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>
        > <a href='/HB5/to_fba'>Scan FBA labels and prepare FBA shipment </a> > <a href="/HB5/BoxDeliveredServlet">Weekly Report - Boxes/Labels done </a>
        <h1>FBA SKU Stored</h1>
        User : <c:out value="${requestScope.userName}" />   <br>
        Date: <c:out value="${requestScope.forDate}" />  <br>
        <table>
            <tr><th class="bordered" >FNSKU</th><th class="bordered">Item Name</th><th class="bordered">Qty</th><th class="bordered">Packaged & Labelled </th></</tr>
        
        <c:forEach items="${requestScope.fbaList}" var="fba">
             <tr><td class="bordered"><c:out value="${fba.fnsku}" /></td><td class="bordered"><c:out value="${fba.itemName}" /></td><td class="bordered"><c:out value="${fba.qty}" /></td>
                 <td class="bordered">
                     <c:out value="${fba.packaged_and_label}" />
                     
                 </td>
             </tr>
        </c:forEach>
            </table>
    </body>
</html>
