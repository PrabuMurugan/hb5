<%-- 
    Document   : prepare_items.jsp
    Created on : Aug 20, 2013, 11:03:01 AM
    Author     : us083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body onload="document.forms[0].reset()">
        <br/>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt;Prepare Items
        <h1>This module is not automated yet. So you pick the items manually, search for the item in FBA inventory.
            <br>If same UPC exists more than once (one for 1 pack, another for pack 6), Ask Rathna. 
            <br>She should be using restock summary to determine how many to send on each.
           <br> If the item was never sent before, just split the units between 1 and 6 packs and send to FBA.!
        </h1>
    </body>
</html>
