<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="jquery.autotab-1.1b.js"></script>
        
        <script src="common.js"></script>
        <style>
            a.error{
                color:red!important;
            }
        </style>
        <script type="text/javascript">
            
            function clearColors(){
                $(document).find("input[type='text'][readonly!='true']").css("background-color","white");
                $(document).find("input[type='text'][readonly='true']").css("background-color","#eeeeee");
                
            }
            
            function stopRKey(evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) {
                    if ($('input:focus').length > 0) {
                        var focusable = $('input').filter(':visible');
                        if (focusable.index($('input:focus')[0]) == 1) {
                            focusable.eq(focusable.index($('input:focus')[0]) + 2).focus();
                        }else if (focusable.index($('input:focus')[0]) % 4 != 0) {
                            focusable.eq(focusable.index($('input:focus')[0]) + 1).focus();
                        } else {
                            focusable.eq(focusable.index($('input:focus')[0]) + 3).focus();
                        }
                        //$('input:focus:first-child').next('input').focus(); 
                    }

                    return false;
                }
            }

            function updateWeights() {
                var sumWeight = 0;
                var i;
                var unknown = false;
                for (i = 0; i < 50; i++) {
                    var weight_name = $("#weight-" + i)[0];
                    var quantity_name = $("#qty-" + i)[0];
                   
                    if (weight_name.value == "Not Found") {
                        unknown = true;
                        break;
                    }
                    if (weight_name.value != null && quantity_name.value != null && weight_name.value != "" && quantity_name.value != "") {
                        sumWeight += (Number(weight_name.value) * Number(quantity_name.value));
                    }

                }
                if (unknown) {
                    document.forms[0].weight.value = "Unknown";
                    $(document).find("#weight").css("color", "red");
                    $(document).find("#weight").css("font-size", "40%!important");
                    
                } else {
                    document.forms[0].weight.value = sumWeight.toFixed(2);
                    if (sumWeight > 50) {
                        $(document).find("#weight").css("color", "red");
                    } else {
                        $(document).find("#weight").css("color", "green");
                    }
                }

                
                clearColors();
            }

            $(document).ready(function() {
                
               $( "#unknown_item_dialog" ).dialog({autoOpen:false,width:"500px"});
               $( "#overweight_item_dialog" ).dialog({autoOpen:false,width:"500px"});
               
                clearColors();
                $(document).keypress(stopRKey);

                //debugger;
                $('.box').get(0).focus();

                /*if( window.location.href.indexOf("outgoing")>0){
                 $("select[name=type] option")[0].value="outgoing_units";
                 $("select[name=type] option")[0].innerHTML="Outgoing";
                 }
                 else{
                 $("select[name=type] option")[0].value="incoming_units";
                 $("select[name=type] option")[0].innerHTML="Incoming";
                 }*/
                $("input").focus(function() {
                    var to_speak = "";
                    to_speak = $(this).attr("data-to-speak");
                    //alert(to_speak);
                    /* var n=this.name.substring(3,8);
                     for(var i=0;i<item_names.length;i++){
                     
                     if(item_names[i].upc==this.value){weight
                     $("span[id='item_name"+n+"']")[0].innerHTML=item_names[i].item_name;  
                     g_item_name=item_names[i].item_name;
                     
                     }
                     
                     }*/
                    if (to_speak.length > 0) {
                        $("#audio_file").attr("src", "voice/" + to_speak + ".mp3");

                    }
                    clearColors();

                });
                $("input.fnsku").change(function() {

                    var fnsku = $("#" + this.id)[0].value;
                    var elem_id = this.id;
                    $.ajax({
                        url: "../GetItemNameFromFNSKU?fnsku=" + fnsku,
                        elem_id: elem_id,                        
                        beforeSend: function(hdr){
                            var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                            var weight_name = $("#" + this.elem_id.replace("fnsku", "weight"))[0];
                            var weight_obtained_name = $("#" + this.elem_id.replace("fnsku", "weight-obtained"))[0];
                            
                            
                            weight_name.value = "";
                            weight_obtained_name.value = "";
                            item_name.innerHTML = "";
                            
                            var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                            var fnsku = $("#" + this.elem_id)[0].value;
                            if(fnsku!=null && fnsku!=""){
                                progress_name.css("visibility","visible");
                            }
                        }

                    }).done(function(data) {
                       if(eval(data.noerror)){
                        var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                            var weight_name = $("#" + this.elem_id.replace("fnsku", "weight"))[0];
                            var weight_obtained_name = $("#" + this.elem_id.replace("fnsku", "weight-obtained"))[0];
                            var quantity_name = $("#" + this.elem_id.replace("fnsku", "qty"))[0];
                            var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                            item_name.innerHTML = data.name;
                            $(item_name).css("font-size","50%!important");
                            weight_name.value = data.weight;
                            weight_obtained_name.value = data.weight;
                            updateWeights();
                            progress_name.css("visibility","hidden");
                            $(weight_obtained_name).css("color","black");
                            if(data.weight>20){
                                 item_name.innerHTML = "<a class='error' href=\"javascript:$( '#overweight_item_dialog' ).dialog('open');\"> Overweight, Remove this item (Click Here) </a>";
                            }
                        //to_speech = data.substring(variable.indexOf('||'));
                       }else{
                             var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                              progress_name.css("visibility","hidden");
                             alert("System error:"+data.error);
                       }

                    }).error(function(data){                
                        var weight_name = $("#" + this.elem_id.replace("fnsku", "weight-obtained"))[0];
                        var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                         weight_name.value = "Not Found";
                         $(weight_name).css("color","orange");
                         item_name.innerHTML = "<a class='error' href='#' onclick=\"javascript:$( '#unknown_item_dialog' ).dialog('open');\"> Remove this item (Click Here) </a>";
                         $(item_name).css("color","red");
                         var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                         progress_name.css("visibility","hidden");
                         updateWeights();
                         
                    });

                });
                $(document).find("input[type='text'][readonly!='true']").focus(clearColors);
                $("input.qty").change(updateWeights);
                $("input.weight").change(updateWeights);

            });

            function checkValues() {
                var i;
                var boxLabel = $("#box")[0];
                var weight = $("#weight")[0];
                if(boxLabel.value == null || boxLabel.value == ""){
                    alert("Please insert the box label");
                     $(document).find("#box").css("background-color","lightpink");
                     return false;
                }
                
                if(weight.value == null || weight.value == "" || weight.value=="Unknown"){
                     alert("Total weight not known");
                     $(document).find("#weight").css("background-color","lightpink");
                     return false;
                }
                for (i = 0; i < 50; i++) {
                    var item_name = $("#fnsku-" + i)[0];
                    var weight_name = $("#weight-" + i)[0];
                    var weight_obtained_name = $("#weight-obtained-" + i)[0];
                    var quantity_name = $("#qty-" + i)[0];

                    if ((item_name.value != null && item_name.value != "")) {
                        if (quantity_name.value == null || quantity_name.value == "") {
                            alert("Please insert the quantity");
                            $(document).find("#qty-" + i).css("background-color","lightpink");
                            return false;
                        }
                        
                        var intRegex = /^\d+$/;
                        if(!intRegex.test(quantity_name.value)) {
                           alert("Quantity must be an integer");
                           $(document).find("#qty-" + i).css("background-color","lightpink");
                           return false;
                        }

                        if (weight_obtained_name.value == null || weight_obtained_name.value == "") {
                            alert("Please wait for the weight to populate");
                            $(document).find("#weight-obtained-" + i).css("background-color","lightpink");
                            return false;
                        }
                        
                        if (isNaN(weight_obtained_name.value) == null) {
                            alert("Weight must be a number");
                            $(document).find("#weight-obtained-" + i).css("background-color","lightpink");
                            return false;
                        }
                        
                        if (weight_name.value == null || weight_name.value == "") {
                            alert("Please fill a weight value");
                            $(document).find("#weight-" + i).css("background-color","lightpink");
                            return false;
                        }
                        
                        
                        if (isNaN(weight_name.value)) {
                            alert("Weight must be a number");
                            $(document).find("#weight-" + i).css("background-color","lightpink");
                            return false;
                        }
                        
                        if(!isNaN(weight_obtained_name.value) && weight_name.value < weight_obtained_name.value){
                            alert("The actual weight "+ weight_name.value +" is smaller than the registered weight "+weight_obtained_name.value+" for item "+item_name.value+", you must open ticket with FBA support to re-weigh the item in FBA warehouse and calculate the proper weight. This transaction will now proceed");
                            $(document).find("#weight-" + i).css("background-color","lightpink");
                            
                        }
                        
                    }
                }
                return true;
            };
function watchAAFromScanner(){
    $("input").each(function( index ) {
        if($( this ).val().indexOf("AA")>=0){
            $(this).blur();
            val=$( this ).val();
            val=val.replace("AA","");
             $( this ).val(val);
            var inputs = $(this).closest('form').find(':input');
            inputs.eq( inputs.index(this)+ 1 ).focus().select();             
        }
});
    
    
    
}
setInterval(function(){watchAAFromScanner()}, 1000);
        </script>
        
        <script type="text/javascript">
            
            
        </script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        <style>
            body{
            font-family: sans-serif;
            font-size: medium;
            color: cadetblue;
            background-color: #fff; 
            /*background-image: 
                linear-gradient(90deg, black 0%, black 10%);
            /*radial-gradient(50px 50px, circle closest-side, black, white);*/



        }
        
        a{
            color: darkgrey;
            font-size: 20px;
        }

        a:hover{
            color: blue;
            font-size: 20px;
        }
        
        input, select, option{
            font-size: 20px;
        }

        .description{
            color: cadetblue;
            margin-bottom: 20px;

        }
        
        button, input[type="submit"]{
            font-size:20px;
            padding: 10px;
        }
        table{
            margin-top: 0px;
        }
        
     
        
        #data td{
            position: fixed;
                
        }
        
        </style>
    </head>
    
    <body onload="document.forms[0].reset()" onsubmit="return checkValues();">
        <div id="unknown_item_dialog" title="Unknown Item">
        This item was not found. <br/>
        1) Check of FBA calculator has correct weight & dimension. If not, go to seller central , search for the item , edit/reslist. Go to More Details tab and enter the shipment weight and dimension and also product weight and dimension. If the product is created today, wait for a day before sending it. 
        <br/> <br/>
        2) If the FBA calculator has correct weight, check if the item exists in seller central -> FBA inventory. Sometimes when SKU is created today, it takes 25 hours before you can send it.
        <br/><br/>
         Regardless, let Rathna/Prabu know and keep it aside
        </div>
        <div id="overweight_item_dialog" title="Unknown Item">
        This item  can not be sent in this shipment. <br/>
        1) This is an oversize item. So it has to go in separate shipment. It can not go in standard shipment.
        <br/><br/>
        2) As long as your this shipment is specific for oversize items, you can send it in this shipment.
        <br/><br/>
         Regardless, let Rathna/Prabu know and keep it aside
        </div>        
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt; Prepare Box
        <form action="../PrepareFBABox" id="store" method="post">
            <div id="header">
                <%@include file="CommonHeader.jsp" %>
                <table id="table">
                    <tr>
                        <td>
                            <input type="submit" value="Submit" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="box" >Scan the box label first</label>
                        </td>
                        <td>
                            <input type="text" name="box" id="box" class="box" autocomplete="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="weight" >Total weight in lbs</label>
                        </td>
                        <td>
                            <input type="text" name="weight" id="weight" class="weight" autocomplete="false" readonly="true"/>
                        </td>
                        
                    </tr>
                    
                </table>
            </div>
            <div id="content">
                <br/>
                <table width="100%">
                    <tr>
                            
                    <td>
                        FBA SKU
                    </td>
                    <td>
                        Quantity
                    </td>

                    <td>
                        Weight per Item
                    </td> 
                    
                     <td>
                        Weight per Item per record
                    </td> 
                    
                   

                    <td>
                        Item Name
                    </td>  


                    </tr>
                    
                        <c:forEach begin="0" end="49" var="index">
                            <tr class="row">
                                <td width="20%">
                                    <input type="text"  id="fnsku-${index}" name="fnsku-${index}" class="fnsku" data-to-speak="Scan the FBA SKU" maxlength="15" size="15" value="" />             
                                </td>
                                <td width="20%">
                                    <input type="text" id="qty-${index}" name="qty-${index}" class="qty" data-to-speak="Enter Quantity"  maxlength="8" size="8"  value=""/> 
                                </td>

                                <td width="20%">
                                    <input type="text" id="weight-${index}" name="weight-${index}" class="weight" data-to-speak=""  maxlength="8" size="8"  value="" /> 
                                </td>
                                
                                <td width="20%">
                                    <input type="text" id="weight-obtained-${index}" name="weight-obtained-${index}" class="weight" data-to-speak=""  maxlength="8" size="8"  value="" readonly="true"/> 
                                </td>
                               
                                <td>
                                    <table>
                                        <tr>
                                            <td> 
                                                <progress id="progress-${index}" style="visibility: hidden" max="100"/>                                            
                                            </td>
                                            <td>
                                                <span id="item_name-${index}"  /> 
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                                
                            </tr>
                        </c:forEach>
                    
                </table>

            </div>

        </form>

        <iframe id="audio_file" style="visibility: hidden"> </iframe>


    </body>


</html>