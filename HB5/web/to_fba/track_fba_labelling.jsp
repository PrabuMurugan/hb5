<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="jquery.autotab-1.1b.js"></script>

        <script src="common.js"></script>
        <style>
            a.error{
                color:red!important;
            }
        </style>
        <script type="text/javascript">
            
            function clearColors(){
                $(document).find("input[type='text'][readonly!='true']").css("background-color","white");
                $(document).find("input[type='text'][readonly='true']").css("background-color","#eeeeee");
                
            }
            
            function stopRKey(evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) {
                    if ($('input:focus').length > 0) {
                        var focusable = $('input').filter(':visible');
                        if (focusable.index($('input:focus')[0]) == 1) {
                            focusable.eq(focusable.index($('input:focus')[0]) + 2).focus();
                        }else if (focusable.index($('input:focus')[0]) % 4 != 0) {
                            focusable.eq(focusable.index($('input:focus')[0]) + 1).focus();
                        } else {
                            focusable.eq(focusable.index($('input:focus')[0]) + 3).focus();
                        }
                        //$('input:focus:first-child').next('input').focus(); 
                    }

                    return false;
                }
            }
            function updateWeights() {
                var sumWeight = 0;
                var i;
                var unknown = false;
                for (i = 0; i < 50; i++) {
                    var quantity_name = $("#qty-" + i)[0];
                   
                    clearColors();
                }
            }

                $(document).ready(function() {
                
                    $( "#unknown_item_dialog" ).dialog({autoOpen:false,width:"500px"});
               
                    clearColors();
                    $(document).keypress(stopRKey);


                    /*if( window.location.href.indexOf("outgoing")>0){
                 $("select[name=type] option")[0].value="outgoing_units";
                 $("select[name=type] option")[0].innerHTML="Outgoing";
                 }
                 else{
                 $("select[name=type] option")[0].value="incoming_units";
                 $("select[name=type] option")[0].innerHTML="Incoming";
                 }*/
                    $("input").focus(function() {
                        var to_speak = "";
                        to_speak = $(this).attr("data-to-speak");
                        //alert(to_speak);
                        /* var n=this.name.substring(3,8);
                     for(var i=0;i<item_names.length;i++){
                     
                     if(item_names[i].upc==this.value){weight
                     $("span[id='item_name"+n+"']")[0].innerHTML=item_names[i].item_name;  
                     g_item_name=item_names[i].item_name;
                     
                     }
                     
                     }*/
                        if (to_speak.length > 0) {
                            $("#audio_file").attr("src", "voice/" + to_speak + ".mp3");

                        }
                        clearColors();

                    });
                    $("input.fnsku").change(function() {
                        
                        var fnsku = $("#" + this.id)[0].value;
                        var elem_id = this.id;
                        if(jQuery.trim(fnsku).length > 1){
                        $.ajax({
                            url: "/HB5/GetItemNameFromFNSKU?fnsku=" + fnsku,
                            elem_id: elem_id,                        
                            beforeSend: function(hdr){
                                var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                                item_name.innerHTML = "";
                            
                                var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                                var fnsku = $("#" + this.elem_id)[0].value;
                                if(fnsku!=null && fnsku!=""){
                                    progress_name.css("visibility","visible");
                                }
                            }

                        }).done(function(data) {
                            if(eval(data.noerror)){itemname
                                var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                                var itemname = $("#" + this.elem_id.replace("fnsku", "itemname")).val(data.name);
                                var quantity_name = $("#" + this.elem_id.replace("fnsku", "qty"))[0];
                                var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                               item_name.innerHTML = data.name;
                                $(item_name).css("font-size","50%!important");
                                 $(item_name).css("color","#000000");
                                progress_name.css("visibility","hidden");
                                //to_speech = data.substring(variable.indexOf('||'));
                            }else{
                                var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                                progress_name.css("visibility","hidden");
                                alert("System error:"+data.error);
                            }

                        }).error(function(data){                
                            var item_name = $("#" + this.elem_id.replace("fnsku", "item_name"))[0];
                        
                            item_name.innerHTML = "<a class='error' href='#' onclick=\"javascript:$( '#unknown_item_dialog' ).dialog('open');\"> Remove this item (Click Here) </a>";
                            $(item_name).css("color","red");
                            var progress_name = $("#" + this.elem_id.replace("fnsku", "progress"));
                            progress_name.css("visibility","hidden");
                         
                        });
                        }
                    });
                    
                    $(document).find("input[type='text'][readonly!='true']").focus(clearColors);
                    $("input.qty").change(updateWeights);

                });

                function checkValues() {
                    var i;
                   
                    for (i = 0; i < 50; i++) {
                        var item_name = $("#itemname-" + i)[0];
                        var fnsku_name = $("#fnsku-" + i)[0];
                        var quantity_name = $("#qty-" + i)[0];

                        if ((fnsku_name.value != null && fnsku_name.value != "")) {
                            if (quantity_name.value == null || quantity_name.value == "") {
                                alert("Please insert the quantity");
                                $(document).find("#qty-" + i).css("background-color","lightpink");
                                return false;
                            }
                        
                            var intRegex = /^\d+$/;
                            if(!intRegex.test(quantity_name.value)) {
                                alert("Quantity must be an integer");
                                $(document).find("#qty-" + i).css("background-color","lightpink");
                                return false;
                            }
                            
                           if (item_name.value == null || item_name.value == "") {
                                alert("Please insert a correct SKU");
                                $(document).find("#fnsku-" + i).css("background-color","lightpink");
                                return false;
                            }
                            
                          
                        
                        }
                    }
                    return true;
                };

        </script>


        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        <style>
            body{
                font-family: sans-serif;
                font-size: medium;
                background-color: #fff; 
                /*background-image: 
                    linear-gradient(90deg, black 0%, black 10%);
                /*radial-gradient(50px 50px, circle closest-side, black, white);*/



            }

            a{
                 font-size: 20px;
            }

            a:hover{
                color: blue;
                font-size: 20px;
            }

            input, select, option{
                font-size: 20px;
            }

            .description{
                color: cadetblue;
                margin-bottom: 20px;

            }

            button, input[type="submit"]{
                font-size:20px;
                padding: 10px;
            }
            table{
                margin-top: 0px;
            }



            #data td{
                position: fixed;

            }
            
            input[type=checkbox]
            {
             /* Double-sized Checkboxes */
             -ms-transform: scale(2); /* IE */
             -moz-transform: scale(2); /* FF */
             -webkit-transform: scale(2); /* Safari and Chrome */
             -o-transform: scale(2); /* Opera */
             padding: 10px;
            }


        </style>
         <script type="text/javascript">
            jQuery(document).ready(function(){
               jQuery("#userID").change(function(){
                   jQuery("#userName").val(jQuery("#userID option:selected").text());
               }) ;
            });
        </script>
    </head>

    <body onload="document.forms[0].reset()" onsubmit="return checkValues();">
   
        <%@include file="CommonHeader.jsp" %>
        > <a href='/HB5/to_fba'>Scan FBA labels and prepare FBA shipment </a> > Track new FNSKUs labelled
        <form action="/HB5/StoreTrackFBALabelling" id="store" method="post" onsubmit="checkValues();">
                <table id="table">
                    <tr>
                        <td>Created By : </td>
                        <td><select id="userID" name="userID">
                                <c:forEach items="${requestScope.users}" var="user" >
                                    <c:choose>
                                    <c:when test="${requestScope.user_id eq user.userID}">
                                        <option value="${user.userID}" selected="selected">${user.userName}</option>
                                    </c:when>
                                    <c:otherwise>
                                         <option value="${user.userID}">${user.userName}</option>
                                    </c:otherwise>
                                   
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <input type="hidden" name="userName" id="userName" value="${requestScope.username}"  data-userid="${requestScope.user_id}">
                        </td></tr><tr><td></td>
                        <td>
                            <input type="submit" value="Submit" />
                        </td>
                    </tr>
                    
                </table>
            <div id="content">
                <br/>
                <table width="100%">
                    <tr>
                       
                        <td>
                            FBA SKU
                        </td>
                        <td>
                            Quantity
                        </td>

                        <td>
                            Item Name
                        </td>  
                        <td width="400px">Packaged & Labelled - Check this checkbox if the product involves packing also. If the product involves just putting FBA label, then do not check this checkbox</td>

                    </tr>

                    <c:forEach begin="0" end="49" var="index">
                        <tr class="row">
                            
                            <td width="20%">
                                <input type="text"  id="fnsku-${index}" name="fnsku-${index}" class="fnsku" data-to-speak="Scan the FBA SKU" maxlength="15" size="15" value="" />             
                            </td>
                            <td width="20%">
                                <input type="text" id="qty-${index}" name="qty-${index}" class="qty" data-to-speak="Enter Quantity"  maxlength="8" size="8"  value=""/> 
                            </td>


                            <td>
                                <table>
                                    <tr>
                                        <td> 
                                            <progress id="progress-${index}" style="visibility: hidden" max="100"/>                                            
                                        </td>
                                        <td>
                                            <input type="hidden" id="itemname-${index}"  name="itemname-${index}">
                                            <span id="item_name-${index}"  /> 
                                        </td>
                                    </tr>
                                </table>

                            </td>
                             <td >
                                <input type="checkbox"  id="packaged_and_label-${index}" name="packaged_and_label-${index}" class="packaged_and_label"  value="1" />             
                            </td>
                        </tr>
                    </c:forEach>

                </table>

            </div>

        </form>

        <iframe id="audio_file" style="visibility: hidden"> </iframe>

       
    </body>


</html>