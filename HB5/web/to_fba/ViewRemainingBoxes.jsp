<%-- 
    Document   : ViewRemainingBoxes
    Created on : Aug 23, 2013, 12:41:58 AM
    Author     : debasish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="u" uri="http://www.tajplaza.com/util" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Undelivered Boxes</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %> &gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt; View boxes that are not sent
        <h1>List of Boxes not yet delivered</h1> 
        <form name="checkboxfrm" action="/HB5/ShowUndeliveredBoxes" method="post">
                <input type="checkbox" name='hide' <%=(request.getParameter("hide") != null && request.getParameter("hide").equals("on") ? "checked" : "" )%>> Hide SKID
                <input type="submit" value="Apply">
        </form>
        
        <table border="1">
            <tr>
                <td>S.N.</td>
                <td>Box Name</td>
                <td>Box Weight</td>
            </tr>
            <c:forEach items="${BoxList}" var="box" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td><a href="${pageContext.request.contextPath}/BoxDetailsServlet?box=${box.name}">${box.name}</a></td>
                    <td>${u:formatDouble(box.weight)}</td>
                </tr>
                
            </c:forEach>
            
        </table>
    </body>
</html>
