<%-- 
    Document   : balance_units_in_shelf
    Created on : Aug 2, 2013, 3:45:10 PM
    Author     : prabu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Identify Items to send</title>
           <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<style>
    table{
        font-size:125%!important;
    }
    body,table{
        margin:0px!important;
    }
  
    </style>
    
<script>
    $(document).ready(function() {
        $('input[name=q1]').get(0).focus();
        
    $("form").submit(function() {
       
        v=$("input[name='q1']").val();
        
        $("#results").attr("src","../Q?sno=18&q1q2same=true&q1="+v);
        
        $("input[name='q1']").val("");
       $("input[name='q1']").focus();
        return false;
        
});        
    });
    
</script>        
    </head>
    <body>
        <br/>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt; Identify which Item to send
        </br><font color="red">
        <h4>DO NOT ENTER TITLE TO SEND ANYTHING. SINCE SAME TITLE MAY PULL DIFFERENT ITEMS. PLEASE SCAN/ENTER THE UPC</h4>
        </font>
        <table>
            <tr>
                <td>
<form action="../Q" id="qform">
              <input type="hidden" name="sno" value="18"/>
              <input type="hidden" name="q1q2same" value="true"/>  
            <input type="text" name="q1" value=""   />
 
            <input type="submit"  value="Submit"/>
           
            
        </form>                    
                </td>
                <td>
                     <div style="font-size: 70%">
                <ul>
                    <li>
                        Scan the upc or enter portion item name like tophe 
                    </li>
                    <li>
                        Take a look at how many asins we sold in past month, how much profit and how many are requried.
                    </li>
                    <li>
                        If rank is above 40,000 do not send. It is going to be very hard to sell above 40,000 rank.
                    </li>
                    <li>
                        Also take a look how many we already have in FBA. DO not send more than 30-40 item of same at a time.
                    </li>
                    <li>
                        If the item is negative profit, pick the one with lowest profit and send. We can not afford to keep the item and not selling. Neither we can sell at drastic loss. Make your judgement and inform Rathna also.
                    </li>                    
                    <li>
                        Click on the asin column to print the label on seller central. It opens new page for seller central, where you can print the label.
                    </li>  
                    <li>
                        If you see any kind of error message in Notes Column, make sure u see the image on amazon and compare before sending
                    </li>
                </ul>
            </div>
                </td>
            </tr>
        </table>
                
        
        <iframe src="" id="results" style="width:100%;height:600px"/>
    </body>
</html>
