<%-- 
    Document   : CommonHeader
    Created on : Sep 16, 2013, 12:53:51 PM
    Author     : debasish
--%>
<a href="${pageContext.request.contextPath}">Home</a>
<%--
<span class="navigation">
<ul>
    <li>
        <a href="${pageContext.request.contextPath}/incoming_to_shelf.jsp">To The Shelf</a>        
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/outgoing.jsp">From The Shelf</a>
    </li>
    <li>
        <a href="${pageContext.request.contextPath}/inventory_query.jsp">Check Balance Inventory/Audit for an item/Bin</a>
    </li>
    <li><a href="#">FBA Shipment</a>
        <ul>
           <li><a href="${pageContext.request.contextPath}/to_fba/identify_item_to_send.jsp">Identify items to send</a></li>
            <li><a href="${pageContext.request.contextPath}/to_fba/prepare_items.jsp">Prepare Items</a></li>
            <li><a href="${pageContext.request.contextPath}/to_fba/prepare_boxlabels.jsp">Prepare labels for boxes</a></li>
            <li><a href="${pageContext.request.contextPath}/to_fba/prepare_box.jsp">Put items into a box </a></li>
            <li><a href="${pageContext.request.contextPath}/to_fba/prepare_pallet.jsp">Scan the boxes and prepare createfbashipment file </a></li>
            <li><a href="${pageContext.request.contextPath}/ShowUndeliveredBoxes">View boxes that are not sent </a></li>
            <li><a href="${pageContext.request.contextPath}/to_fba/copy_box.jsp">Make copies of existing box </a></li>
            <li><a href="${pageContext.request.contextPath}/FNSKUScanner">See details of FNSKU  </a></li>
            <li><a href="${pageContext.request.contextPath}/ArchiveBoxes">Delete Boxes  </a></li>
            <li><a href="${pageContext.request.contextPath}/BoxDeliveredSerlvet">Delete Boxes  </a></li>
        </ul>
    </li>
    <li class="menupad">
        <a ref=""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
    </li>
    
</ul>
</span>
<br/>
--%>