<%-- 
    Document   : send_to_fba
    Created on : Aug 20, 2013, 11:00:14 AM
    Author     : us083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        
    </head>
    <body>
       <%@include file="CommonHeader.jsp" %> &gt; Scan FBA labels and prepare FBA shipment  
       <ol class="list">
           <li><a href="/HB5/ExpiryDates_Incoming.jsp">Products Entering Warehouse - Enter Expiry Dates</a></li>
            <li><a href="identify_item_to_send.jsp">Identify which Item to send</a></li>
            <li><a href="prepare_items.jsp">Prepare Items</a></li>
            <li><a href="prepare_boxlabels.jsp">Prepare 30 labels for boxes and use them to paste on boxes (24 18 18 Boxes) </a></li>
            <li><a href="prepare_box.jsp">Put items into a box and make it to around 50 lbs total box weight </a></li>
            <li><a href="prepare_pallet.jsp">Scan the boxes and prepare createfbashipment file </a></li>
            <li><a href="../ShowUndeliveredBoxes">View boxes that are not sent </a></li>
            <li><a href="copy_box.jsp">Make copies of existing box </a></li>
            <li><a href="../FNSKUScanner">See details of FNSKU inside which box</a></li>
            <li><a href="../ArchiveBoxes">Delete Boxes  </a></li>
            <li><a href="../BoxDeliveredServlet">Weekly Report - Boxes/Labels done</a></li>
            <li><a href="../StoreTrackFBALabelling">Track new FNSKUs labelled</a></li>
            <li><a href="../StoreTrackFBALabelling?action=update">Update/Delete existing FNSKUs scanned</a></li>
           <!-- <li><a href="../ViewFBAbyDate">View FBA labels by date</a></li> -->
        </ol>  
             
    </body>
</html>
