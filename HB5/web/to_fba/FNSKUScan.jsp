<%-- 
    Document   : FNSKUScan
    Created on : Sep 11, 2013, 12:26:58 AM
    Author     : debasish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Check FNSKU Details</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="CommonHeader.jsp" %>&gt; <a href="/HB5/to_fba">Scan FBA labels and prepare FBA shipment  </a> &gt;See details of FNSKU inside which box
        <h1>Check FNSKU details</h1>

        <form action="${pageContext.request.contextPath}/FNSKUScanner" method="post">
            Enter FNSKU: <input type="text" name="fnsku" /> 
            <input type="submit" value="View Details"/>
        </form>
        <c:if test="${fnskuDetails != null}" >
            <table class="bordered">
                <tr>
                    <td>Item Name</td>
                    <td>${fnskuDetails.details}</td>
                </tr>
                <tr>
                    <td>Total Quantity</td>
                    <td>${fnskuDetails.totalQuantity}</td>
                </tr>
                <tr>
                    <td>Package Type</td>
                    <td>${fnskuDetails.packageType}</td>
                </tr>
            </table>

                <table class="bordered">
                <tr>
                    <td>Box Name</td>
                    <td>Quantity In Box</td>
                    <td>Date Sent on Pallet</td>
                </tr>
                <c:forEach items="${fnskuDetails.boxes}" var="box">
                    <tr>
                        <td>${box.boxName}</td>
                        <td>${box.quantityInBox}</td>
                        <td>${box.timeSent}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </body>


</html>
