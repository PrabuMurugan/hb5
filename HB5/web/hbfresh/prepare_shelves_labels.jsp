<%-- 
    Document   : prepare_items.jsp
    Created on : Aug 20, 2013, 11:03:01 AM
    Author     : us083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body onload="document.forms[0].reset()">
        <h1> This module prints 30 labels to be printed on the same row shelves. 
            <br>Enter the rack (A - bottom row, B-second row, C - third row)
            <br>01 for first row near the wall , 02 on the second row (Still same shelve but other side row), 03 - third row
            
            <br>So enter A01 for bottom row , first row 
            <br>So enter A02 for bottom row , second row 
            
            <br/>
            <form action="shelves_labels.jsp">
            <input type="text" value="" name="loc" value="A01"/>
            <br/>
            <input type="submit" value="submit"/>
            </form>
            <br/>
        </h1>
    </body>
</html>
