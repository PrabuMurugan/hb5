<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="database.DBWrapperNew"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html  >
<!--<html >-->
<head>
<title>Request FBA Labels</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
<script>
    var expiryMap = {};
	<%
	 Connection con0= DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
         String sql=" select   f.fnsku,max(date_format(STR_TO_DATE(concat(expirydate,'15'),'%m%y%d'),'%m-%d-%Y')) from tajplaza.fba_expiry_dates e,   tajplaza.amazon_catalog a, tajplaza.existing_inventory_fba f  where   e.upc=a.upc and a.asin=f.asin  and                   length(expirydate) = 4  and STR_TO_DATE(expirydate,'%m%y')>DATE_ADD(curdate(),INTERVAL 3 MONTH)                    and (e.upc,e.ts) in (select upc,max(ts) from tajplaza.fba_expiry_dates group by upc)     group by f.fnsku ";
         System.out.println("Executing sql"+sql);
       PreparedStatement ps0=con0.prepareStatement(sql);
         
       ResultSet rs0=ps0.executeQuery();
      while(rs0.next()){
    %>
        expiryMap["<%=rs0.getString(1)%>"]= "<%=rs0.getString(2)%>";
    <% }%>


    $( document ).ready(function() {
        $( "input#upc" ).focus(function() {
            $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=/HBPublic/voice/scan_upc.mp3 type="audio/mpeg"></audio>'); 
           // $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=beep.mp3 type="audio/mpeg"></audio>');
        });
        $( "input#upc" ).change(function() {
            printExpiryDateLabel();
             
                          
        })        
        $( "input#upc").focus();
    		setInterval(function(){watchAAFromScanner()}, 1000);
   });    
 function printExpiryDateLabel(){
     upc=$("input#upc").val();
     if(upc==null)
         return;
     exp_url="http://www.listlabelship.com/quicklabels-zebra-print.php?label=bestby&qty=1&date=MM-DD-YYYY&st=0&top=10";
     expdate=expiryMap[upc];
     if(expdate!=null){
         exp_url=exp_url.replace("MM-DD-YYYY",expdate);
     }else{
         exp_url="https://www.listlabelship.com/quicklabels-zebra.php";
     }
     window.open(exp_url);
     $("input#upc").blur();
     $( "input#upc" ).focus().select();    
 }
   prev_val="";
  val="";
function watchAAFromScanner(){
	val=$("input#upc").val();
	if(prev_val.length>0 && val.indexOf(prev_val)>=0)
		val=val.substring(prev_val.length,val.length);
	if(val.length>0 && val.indexOf("AA")>=0)
		prev_val=$("input#upc").val().replace("AA","");	
	if(val!=null && val.indexOf("AA")>=0){
		 printExpiryDateLabel();

	}
  
}  
 </script>

 <style> input {font-size:200%} </style>
</head>
<body >
    <a href="/HB5/to_fba_new">Scan FBA labels and prepare FBA shipment  </a>
    <h4>Scan FNSKU to print expiry date label</h4>
   
    <ul>
        <li>Scan the FNSKU which has expiry date but the expiry date is not clearly visible.</li>
        <li>It opens the list label site through which you can print the best by date label on Dymo printer.</li>
        <li>Print it next to the FNSKU label</li>
    
    </ul>
		
	 
    
    <table>
        <tr>    <td>  FNSKU : </td><td> <input id=upc name=upc value=""/> 
     
     
      </table>
    
<span id="dummy" style="display:none"></span><br/>   
<div id="results"> </div>
</body>
</html>
