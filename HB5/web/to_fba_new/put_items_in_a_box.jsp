<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html  >
<!--<html >-->
<head>
<title>Request FBA Labels</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
<script>
    $( document ).ready(function() {
        $( "input#upc" ).focus(function() {
            $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=/HBPublic/voice/scan_upc.mp3 type="audio/mpeg"></audio>'); 
           // $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=beep.mp3 type="audio/mpeg"></audio>');
        });
       
        $( "input#qty" ).focus(function() {
            $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=/HBPublic/voice/enter_qty.mp3 type="audio/mpeg"></audio>');
         //   $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=beep.mp3 type="audio/mpeg"></audio>');
        });        
         $( "input#shipmentid" ).focus().select();
         $( "input#upc" ).change(function() {
                $("input#qty").select().focus();
        });
        $( "input#shipment_id" ).blur(function() {
             sid=$( "input#shipment_id" ).val();
             if(sid.indexOf("FBA")<0){
                 alert("Shipment ID starts with FBA");
                  $( "input#shipment_id" ).select().focus();
                // return false;
             }
             
                          
        });        
        $( "input#qty" ).change(function() {
            store();
             
                          
        });
		setInterval(function(){watchAAFromScanner()}, 1000);
   });    
   function store(){
        shipment_id= $( "input#shipment_id" ).val();
         upc= $( "input#upc" ).val();
	box_label= $( "input#box_label" ).val();
	qty= $( "input#qty" ).val();
	var jqxhr = $.get("/HBPublic/StoreDataFromScanner?shipment_id="+shipment_id+"&upc="+upc+"&box_label="+box_label+"&qty="+qty , function(data) {
		if(data.indexOf("ERROR")>=0){
			alert(data);
		}
	});       
	  jqxhr.always(function() {
          h=$("div#results").html();
          h= " Stored "+ $( "input#upc" ).val()+","+","+$( "input#qty" ).val()+"<br/>"+h;;
          $("div#results").html(h);              
		  $( "input#upc" ).val("");
		  $( "input#qty" ).val("")
		$( "input#upc" ).focus().select(); 
	  });
   }
  prev_val="";
  val="";
function watchAAFromScanner(){
	val=$("input#box_label").val();
	if(prev_val.length>0 && val.indexOf(prev_val)>=0)
		val=val.substring(prev_val.length,val.length);
	if(val.length>0 && val.indexOf("AA")>=0)
		prev_val=$("input#box_label").val().replace("AA","");	
	if(val!=null && val.indexOf("AA")>=0){
		$("input#box_label").val(val.replace("AA",""));
		 $("input#box_label").blur();
		 $( "input#upc" ).focus().select();
	}
 
    
	val=$("input#upc").val();
	if(prev_val.length>0 && val.indexOf(prev_val)>=0)
		val=val.substring(prev_val.length,val.length);
	if(val.length>0 && val.indexOf("AA")>=0)
		prev_val=$("input#upc").val().replace("AA","");	
	if(val!=null && val.indexOf("AA")>=0){
		$("input#upc").val(val.replace("AA",""));
		 $("input#upc").blur();
		 $( "input#qty" ).focus().select();
	}
 
 	val=$("input#qty").val();
	if(prev_val.length>0 && val.indexOf(prev_val)>=0)
		val=val.substring(prev_val.length,val.length);
	if(val.length>0&& val.indexOf("AA")>=0)
		prev_val=$("input#qty").val().replace("AA","");	
	if(val!=null && val.indexOf("AA")>=0){
		$("input#qty").val(val.replace("AA",""));
		debugger;
		store();
	}	
}  
function startnewbox(){
    $("input#box_label").val("").select().focus();
}
</script>

 <style> input {font-size:200%} </style>
</head>
<body >
    <a href="/HB5/to_fba_new">Scan FBA labels and prepare FBA shipment  </a>
    <h4>Put FNSKUs in a box</h4>
   
	
	 
    
    <table>
        <tr>    <td>   FBA Shipment ID : </td><td> <input id=shipment_id name=shipment_id value=""/> Put the shipment ID from  <a target="_blank " href="https://sellercentral.amazon.com/gp/fba/inbound-queue/index.html/ref=ag_xx_cont_fbashipq">https://sellercentral.amazon.com/gp/fba/inbound-queue/index.html/ref=ag_xx_cont_fbashipq</a></td></tr>
        
        <tr>    <td>   Scan input box label  : </td><td> <input id=box_label name=box_label value=""/></td></tr>
     
	<tr>    <td>  FNSKU : </td><td> <input id=upc name=upc value=""/></td></tr>
        <tr>    <td>Quantity   : </td><td> <input id=qty name=qty value=""/></td></tr>
     
      </table>
    <a href="#" onclick="javascript:startnewbox()"> Start New Box</a>
   
    <br/>  <br/>
   
<span id="dummy" style="display:none"></span><br/>   
<div id="results"> </div>
</body>
</html>
