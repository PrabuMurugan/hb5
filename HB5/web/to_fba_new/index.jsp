<%-- 
    Document   : send_to_fba
    Created on : Aug 20, 2013, 11:00:14 AM
    Author     : us083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        
    </head>
    <body>
       Scan,Print FBA labels and prepare FBA shipment  
       <ol class="list">
           <li><a href="../to_fba/prepare_boxlabels.jsp">Prepare 30 labels for boxes and use them to paste on boxes (24 18 18 Boxes) </a></li>
           <li><a href="print_FBA_labels.jsp"> Scan and print FBA labels</a></li>
           <li><a href="/HBPublic/storage2/place_FBA_labelled_products.jsp"> Place FBA labelled products in shelf and ready for fba shipment creation</a></li>
          
           
           <li><a href="/HB5/Q?sno=161"> Audit and verify all FBA labelled products count</a></li>
           <li><a href="/HB5/to_fba_new/print_expirydates_labels.jsp"> Print Expiry date labels</a></li>
         <li><a href="put_items_in_a_box.jsp">Put items in a box </a></li>
         <li><a href="print_box_2D_labels.jsp"> Print 2D labels for box content </a></li>
        </ol>  
             
    </body>
</html>
