<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html  >
<!--<html >-->
<head>
<title>Request FBA Labels</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
<script>
    $( document ).ready(function() {
        $( "input#upc" ).focus(function() {
            setTimeout(function(){
                 $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=/HBPublic/voice/scan_upc.mp3 type="audio/mpeg"></audio>');
            }, 100);
           
        });
        $( "input#expirydate" ).focus(function() {
                        setTimeout(function(){
                $("#dummy").html('<audio controls  autoplay="autoplay">    <source src=/HB5/voice/enter-expiry-date.mp3 type="audio/mpeg"></audio>');
            }, 100);
        });        
         $( "input#upc" ).focus().select();
 
        $( "input#upc" ).change(function() {
           $("input#expirydate").select().focus();
                          
        });
        $( "input#expirydate" ).change(function() {
           store();
                          
        });        
        
        setInterval(function(){watchAAFromScanner()}, 500);
   }); 
   String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}
exp_url="";
   function store(){
    
    upc= $( "input#upc" ).val();
    upc=upc.replace("AA","");
	
    expirydate= $( "input#expirydate" ).val();
    expirydate=expirydate.replace("AA","");
    if(expirydate!='00' && expirydate.length!=4){
        $("div#results").html("<h1 style=color:red>The expiry date has to be either 00 or 4 digits </h1>");        
        $( "input#expirydate" ).focus().select();
        return;
    }
	debugger;
        exp_url="http://www.listlabelship.com/quicklabels-zebra-print.php?label=bestby&qty=1&date=MM-DD-YYYY&st=0&top=10";
    if(expirydate!="00"){
        var dt=new Date();
       var  yr=parseInt(expirydate.substring(2,4));
       var  mn=parseInt(expirydate.substring(0,2));
        dt.setYear(yr+2000);
        dt.setMonth( mn-1);
        var today=new Date();
        var diff =  (dt-today ) / 86400000;
        if(diff<80||diff>10*365 ||yr <16 || yr>50 ||mn<1||mn>12 ){
        $("div#results").html("<h1 style=color:red>The expiry date has to be  from next 3 months till 10 years from today. <br/> Example values : 1119 OR 0122</h1>");        
        $( "input#expirydate" ).focus().select();
            
            return;
        }
        exp_url=exp_url.replace("MM",expirydate.substring(0,2).lpad('0',2));
        exp_url=exp_url.replace("YYYY",yr+2000);
        exp_url=exp_url.replace("DD",15);
		debugger;
		/*if($("input#expirydate_label").is(":checked")){
			$("input#expirydate_label").attr("checked",true);
			setTimeout(function(){ window.open(exp_url); }, 6000); 
			
		}*/
			
        
    }
    sc_url="https://sellercentral.amazon.com/hz/inventory/ref=ag_invmgr_dnav_xx_?tbla_myitable=sort:%7B%22sortOrder%22%3A%22DESCENDING%22%2C%22sortedColumnId%22%3A%22date%22%7D;search:UPC;pagination:1;#AUTO_PRINT"
     sc_url=sc_url.replace("UPC",$( "input#upc" ).val());
     if($( "input#upc" ).val().length>0){
		 window.open(sc_url);
		
	 }
       
 
    
    
    var jqxhr = $.get("/HBPublic/StoreDataFromScanner?request_fba_upc="+upc+"&expirydate="+expirydate , function(data) {
        if(data.indexOf("ERROR")>=0){
            alert(data);
        }
    });     
      h=$("div#results").html();
      h=" Printing "+ $( "input#upc" ).val()+","+$( "input#expirydate" ).val()+"<br/>"+h;
      $("div#results").html(h);
      $( "input#upc" ).val("");
      $( "input#expirydate" ).val("")
    $( "input#upc" ).focus().select(); 


   }
  prev_val="";
  val="";   
function watchAAFromScanner(){
        if($("input#upc").val().indexOf("AA")<0 && $("input#expirydate").val().indexOf("AA")<0)
            return;
	val=$("input#upc").val(); 
	if(val.indexOf("AA")>=0){
            $("input#upc").val(val.replace("AA",""));
            prev_val=$("input#upc").val(); 
             $("input#expirydate").select().focus();
        }
	val=$("input#expirydate").val(); 
	if(prev_val.length>0 && val.indexOf(prev_val)>=0)
		val=val.substring(prev_val.length,val.length);        
	if(val.indexOf("AA")>=0){
            $("input#expirydate").val(val.replace("AA",""));
             prev_val=$("input#expirydate").val(); 
            store();
        }
		
	
}   
function printExpLabel(){
	 window.open(exp_url);
}
</script>

 <style> input {font-size:200%} </style>
</head>
<body >
    <a href="/HB5/to_fba_new">Scan FBA labels and prepare FBA shipment  </a>
	<h4>Scan UPC for FBA label</h4>
	 
    
    <table>
    
	<tr>    <td>  UPC : </td><td> <input id=upc name=upc value=""/></td></tr>
        <tr >    <td>Expiry Date   : </td><td> <input  type="text" id=expirydate name=expirydate value=""/>(Enter digits month and 2 digits year, 0419. Enter 00 if there is no expirydate) </td></tr>
	 
     
      </table>
	 <br/>  <br/> 
   <a href="javascript:printExpLabel()"> Print Best By Date Label  for the last scanned product:  </a> Click to Print best by label,   if  the Best By Date on the product is not clear or may get hidden while polybagging. You do not need to check this if there is no expiry date.
   
 
<span id="dummy" style="display:none"></span><br/>   
<div id="results"> </div>
</body>
</html>
