<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page import="database.DBWrapperNew"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ page language="java" import="java.sql.*"%> 
 <%@ page language="java" import= "util.DB"%> 
<!DOCTYPE html>
<html  >
<!--<html >-->
<head>
<title>Request FBA Labels</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
<script>
</script>

 <style> input {font-size:200%} td {border :1px solid;} </style>
</head>
<body >
    <a href="/HB5/to_fba_new">Scan FBA labels and prepare FBA shipment  </a>
    <h4>Print 2D labels for boxes</h4>
    <ol>
        <li> Read through each box that was prepared in last 1 month. </li>
        <li>Click Box label link to verify if you have any doubt what the box is supposed to contain </li>
        <li>Check the checkbox for all boxes that needs 2 D labels and click submit button</li>
    </ol>
    
    <form id="2dlabels" name="2dlabels" action="print_box_2D_labels_results.jsp">
        <button type="submit" form="2dlabels" value="Submit">Submit</button> 
        <table> <tr><th>Shipment ID</th><th>Box Name</th><th>Box made day</th></tr>
           
	<%
	 Connection con0= DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
         String sql=" update tajplaza.fba_boxes box  set "
               + "expirydate=( select   max(date_format(STR_TO_DATE(concat(expirydate,'15'),'%m%y%d'),'%y%m%d')) from tajplaza.fba_expiry_dates e, "
               + "tajplaza.amazon_catalog a, tajplaza.existing_inventory_fba f  where   e.upc=a.upc and a.asin=f.asin  and  "
               + "length(expirydate) = 4  and STR_TO_DATE(expirydate,'%m%y')>DATE_ADD(curdate(),INTERVAL 3 MONTH)  "
               + "and (e.upc,e.ts) in (select upc,max(ts) from tajplaza.fba_expiry_dates group by upc)  and f.fnsku=box.fnsku   group by f.fnsku)";
         System.out.println("Executing sql"+sql);
       PreparedStatement ps0=con0.prepareStatement(sql);
       ps0.executeUpdate();
        sql=" update  tajplaza.fba_boxes  f, tajplaza.existing_inventory_fba e set f.item_name=e.product_name where f.fnsku=e.fnsku";
         System.out.println("Executing sql"+sql);
        ps0=con0.prepareStatement(sql);
       ps0.executeUpdate();
               
        ps0=con0.prepareStatement("select distinct shipmentid,box_label,date_format(ts,'%m/%d') boxed_day from tajplaza.fba_boxes order by ts desc");
       ResultSet rs0=ps0.executeQuery();
      while(rs0.next()){
    %>

    <tr> <td><%=rs0.getString(1)%> </td>  <td> <input type="checkbox"  name="<%=rs0.getString(2)%>" value="<%=rs0.getString(2)%>"> <a target="_blank" href="http://localhost/HB5/Q?sno=164&q1=<%=rs0.getString(2)%>" ><%=rs0.getString(2)%> </a></input></td><td> <%=rs0.getString(3)%></td></tr>
      
    <% }%>
      </table>
</form>
	
   
    <br/>  <br/>
   
<span id="dummy" style="display:none"></span><br/>   
<div id="results"> </div>
</body>
</html>
