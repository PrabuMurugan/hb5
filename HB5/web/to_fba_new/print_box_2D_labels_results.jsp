<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="database.DBWrapperNew"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%@ page language="java" import="java.sql.*"%> 
 <%@ page language="java" import= "util.DB"%> 
<!DOCTYPE html>
<html  >
<!--<html >-->
<head>
<title>Request FBA Labels</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
<script>
</script>

 <style> input {font-size:200%} </style>
        <style>

 
 table td{
     width: 3.8in;
     height: 3.4in;
       
 }
 
     </style> 
     <style type="text/css" media="print">
         .explain{display:none}
      </style>
      
</head>
<body >
    
    
    <h1 class="explain">Print this page. Just uncheck header/footer while printing. Then use these labels on each box that you will be using to pack items.</h1>
    <table  cellspacing="0">  <tr>
	<%
	 Connection con0= DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
 
                     
       PreparedStatement  ps0=con0.prepareStatement("select shipmentid,box_label,fnsku,sum(cast(qty as decimal)) qty,expirydate,item_name from tajplaza.fba_boxes where box_label=? group by shipmentid,box_label,fnsku");
        
        HashMap<String,String> params=(HashMap)request.getParameterMap();
        Iterator<String> iter=params.keySet().iterator();
        int count=-1,openCount=0;;
        while(iter.hasNext()){
            count++;
            String key=iter.next();
            ps0.setString(1, key);
             ResultSet rs0=ps0.executeQuery();
             boolean boxContent=false;
             String img_src="https://chart.googleapis.com/chart?cht=qr&chs=200x200&chl=AMZN,PO: ";
              while(rs0.next()){
                  String shipmentid=rs0.getString("shipmentid");
                  if(boxContent==false){
                      //First record put shipment id
                      img_src+=""+shipmentid;
                  }
                  boxContent=true;
                 
                 String box_label=rs0.getString("box_label");
                 String item_name=rs0.getString("item_name");
                 String fnsku=rs0.getString("fnsku");
                 String qty=rs0.getString("qty");
                 String expirydate=rs0.getString("expirydate");
                 img_src+=", FNSKU:"+fnsku+", QTY: "+qty;
                 if(expirydate!=null && expirydate.length()==6){
                     img_src+=", EXP: "+expirydate;
                 }
                }//End of while loop to fetch box contents
                  
            if(count%2==0){
               %> <tr> <%
            }
            %>
            <td align="center"><%=key%><br/> <img src="<%=img_src%>"/> </td>
            <%if(count%2!=0){
               %> </tr> <%
            }  
             
                          
        }%>
   
      </table>
	  
   
<span id="dummy" style="display:none"></span><br/>   
<div id="results"> </div>
</body>
</html>
