<%-- 
    Document   : inventory_management
    Created on : Jul 21, 2013, 2:16:58 PM
    Author     : US083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="to_fba/CommonHeader.jsp" %>
        <ol>
            <li><a href="inventory_management.jsp">Inventory Management</a></li>
            <li><a href="to_fba_new">(NEW)Scan FBA labels and prepare FBA shipment</a></li>
            <li><a href="to_fba">(OLD DO NOT USE) Scan FBA labels and prepare FBA shipment</a></li>
            <li><a href="/HB5/Q">All Queries</a></li>
            <li><a href="harrisburg_fresh.jsp">Harrisburg Fresh</a></li>
            
        </ol>
        
    </body>
</html>
