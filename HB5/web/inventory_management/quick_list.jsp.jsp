<%-- 
    Document   : index
    Created on : Apr 16, 2014, 2:30:08 AM
    Author     : Raj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Cache-Control", "public,max-age=3600,public");
    
%>
<!DOCTYPE html>
<html  manifest="example9.appcache">
 
<head>
<title>Costco/BJs Scan5</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
<META name="HandheldFriendly" content="true" />
<meta charset="UTF-8">
<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS">
<meta name="author" content="WebCodeGeeks.com">

<script src="jquery-2.1.4.min.js"></script>
 

 <style> input {font-size:200%} </style>
</head>
<body >
	<h4>Scan Row Num,UPC, Item Code, Price</h4>
	<form name=ShoppingList>
    
    <table>
    
	<tr>    <td> UPC : </td><td> <input id=upc name=upc value=""/></td></tr>
      <tr>    <td> Asin : </td><td> <input id=item_code name=asin value=""/></td></tr>
      <tr>    <td> Pkg Qty: </td><td> <input id=pkg_qty name=price value=""/></td></tr>
      </table>
	</form>
    <button id="clear" onclick="clear()">  Clear </button>
    <br/>   <br/>
       <button id="send_all_data" onclick="sendAllData()">  Send All Data To Server </button>
    <br/> 
    <!--<button id="clear_all_stored_data" onclick="localStorage.clear()"  >  Clear All Stored Data </button>-->

    <a href="store_bubblewrap.jsp"><h3>Buble Wrap Items</h3></a>
    <a href="javascript:hideItemCode()"  ><h3>BJs Scan</h3></a> 
   <a href="javascript:showItemCode()"  ><h3>Costco Scan</h3></a> 
   
   <a href="place_FBA_labelled_products.jsp"><h3>Placed FBA labels</h3></a>
   <a href="/HB5/to_fba_new/put_items_in_a_box.jsp"><h3>Put Items In a Box</h3></a>
   <a href="add_to_shelf.jsp"><h3>Add To Shelf</h3></a>
    <span id="msg"> </span>
    <span id="dummy" style="display:none"></span><br/>
  Rows Stored : <span id="row_count"></span>	 
	
</body>
</html>
