<!DOCTYPE html>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="jquery.autotab-1.1b.js"></script>

        <script type="text/javascript">
            function stopRKey(evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) {
                    if ($('input:focus').length > 0) {
                        var focusable = $('input').filter(':visible');

                        focusable.eq(focusable.index($('input:focus')[0]) + 1).focus();
                        //$('input:focus:first-child').next('input').focus(); 
                    }

                    return false;
                }
            }

            document.onkeypress = stopRKey;

            $(document).ready(function() {
                document.getElementById("store").reset();

                // OR


                orig_htm = $("tr.row").parent().html();
                for (var i = 2; i < 20; i++) {
                    htm = orig_htm.replace("-1", "-" + i);
                    htm = htm.replace("-1", "-" + i);
                    htm = htm.replace("-1", "-" + i);
                    htm = htm.replace("-1", "-" + i);
                    htm = htm.replace("-1", "-" + i);

                    $('tbody#data').parent().append(htm);
                }
                //debugger;
                $('.upc').get(0).focus();

   
                $("input").focus(function() {
                    var to_speak = "";
                    to_speak = $(this).attr("data-to-speak");
                    //alert(to_speak);
                    /* var n=this.name.substring(3,8);
                     for(var i=0;i<item_names.length;i++){
                     
                     if(item_names[i].upc==this.value){
                     $("span[id='item_name"+n+"']")[0].innerHTML=item_names[i].item_name;  
                     g_item_name=item_names[i].item_name;
                     
                     }
                     
                     }*/
                    if (to_speak.length > 0) {
                        $("#dummy").html('<audio controls  autoplay="autoplay">    <source src="voice/' + to_speak + '.mp3" type="audio/mpeg"></audio>');

                    }

                });


                $(".upc").change(function() {
                    var elem_id = this.id;
                    $.ajax({
                        url: "Restock_HB_ItemName?upc="+encodeURIComponent(this.value),
                        elem_id:elem_id,
                        beforeSend: function(xhr) {
                            xhr.overrideMimeType("text/json; charset=x-user-defined");
                        }
                    }).done(function(data) {
                        item_names = data.item_names;
                        var item_name = $("#" + this.elem_id.replace("upc", "item_name"));
                        item_name.html(data.item_name);
                        
                    });
                });
            });
            item_names = null;


        </script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>

    <body>
        <%@include file="to_fba/CommonHeader.jsp" %>
        &gt;
        <a href="inventory_management.jsp">Inventory Management</a>

        <form action="StoreExpiryDates" id="store" method="post">
 
            <input type="submit" value="Submit" />

            <table width="100%">
                <thead>

                <td>
                    <h1>UPC</h1>
                </td>
                <td>
                    <h1> Expiry Date (Enter 2 digit Month and 2 Digit Year like 0215)</h1>
                </td>
                </thead>
                <tbody id="data">
                    <tr class="row">
                        <td>
                            <input type="text" name="upc-1" id="upc-1" class="upc" data-to-speak="upc" maxlength="17" size="17" value="" />             
                        </td>
                        <td>
                            <input type="text" name="exp-1" id="loc-1" class="loc" data-to-speak="binlocation"  maxlength="8" size="8"  value=""/> 
                        </td>
                        <td>
                            <span id="item_name-1"  /> 
                        </td>
                    </tr>

                <tbody>
            </table>



        </form>

        <div id="dummy"></div>


    </body>


</html>