<%-- 
    Document   : saveprice
    Created on : 18 Oct, 2013, 3:26:54 PM
    Author     : chand
--%>

<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    </head>
    <body>
        <%@include file="to_fba/CommonHeader.jsp" %>
        &gt;
        <a href="inventory_management.jsp">Inventory Management</a> &gt; Edit Price
        <% if(request.getParameter("result") != null){%><%=request.getParameter("result")%><%}%>
        <br>
        <form action="SearchItem" method="GET">
     
             <!--<input type="hidden" name="origin" value="editprice.jsp"/>-->
             
             <br/><br/><br/>
             
             <% 
            String searchItem =  "";   
            if(request != null && request.getParameter("item") != null)
               searchItem = request.getParameter("item");  
            
                %>
            <input type="text" name="item" value="<%=searchItem%>"/>  
             
            <input type="submit"  value="Search"/>
            
        </form>
        <br><br><br>
        <form action="SaveData" method="POST">
            <% 
            java.util.LinkedList data =  new java.util.LinkedList();   
            if(request != null && request.getAttribute("data") != null)
               data = (java.util.LinkedList)request.getAttribute("data");  
            
                %>
            
            <table border="1" width="75%">
                <tr>
                    <th>id</th>
                    <th>item name</th>
                    <th>barcode</th>
                    <th>price</th>
                </tr>
                <% 
                for(Object row : data){
                    int i = 0;
                    %>
                    <tr>
                    <% for(Object coloumn : (String[])row){    %>
                    <td>
                        
                        <%if(i == 0) { %> 
                        <input style="border:none"  type="text" name="id" value="<%=coloumn%>" readonly="readonly">
                        <%} else if(i == 1){%> 
                        <input style="border:none" type="text" name="title" value="<%=coloumn%>" size="<%=coloumn.toString().length()+2%>" readonly="readonly">
                        <%} else if(i == 2){%> 
                        <input style="border:none" type="text" name="barcode" value="<%=coloumn%>" readonly="readonly">
                        <%} else if(i == 3){%> 
                        <input type="text" name="price" value="<%=coloumn%>">
                        <%  } i++; %>
                        </td>
                        <% } %>
                    </tr>   
                    <% } %>
            </table>
            <input type="submit" value="Save"/>
            </form>
    </body>
</html>
