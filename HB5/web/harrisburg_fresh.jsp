<%-- 
    Document   : inventory_management
    Created on : Jul 21, 2013, 2:16:58 PM
    Author     : US083274
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
    </head>
    <body>
        <%@include file="to_fba/CommonHeader.jsp" %> &gt; Inventory Management
        <ol class="list">
            <li><a href="/HB5/HBFreshAwaitingOrdersSummary?request_type=unique_ordered_products">  Meals Order Form</a></li>
             <li><a href="/HB5/HBFreshAwaitingOrdersSummary?request_type=customer_ordered_products"> Customers Delivery Form</a></li>
             <li><a href="/HB5/HBFreshAwaitingOrdersSummary?request_type=customer_ordered_products&label_print=true"> Customers Delivery Labels</a>
             
             Print these labels on Dymo 450 label printer.
             <br/>
             Open the link on chrome. Do Control + P to print the page. Pick Dymo 450 Label printer. Select paper size as 30256 Shipping.
             <br/> Select Margin as None. Then Print.
             <br/>
             </li>
        </ol>
        
    </body>
</html>
