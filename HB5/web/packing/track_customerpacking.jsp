<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
    <head>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        	<script src="packing/jquery/jquery-linedtextarea.js"></script>
	<link href="packing/jquery/jquery-linedtextarea.css" type="text/css" rel="stylesheet" />
        
          


        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        <style>
            body{
                font-family: sans-serif;
                font-size: medium;
                background-color: #fff; 
                /*background-image: 
                    linear-gradient(90deg, black 0%, black 10%);
                /*radial-gradient(50px 50px, circle closest-side, black, white);*/



            }

            a{
                 font-size: 20px;
            }

            a:hover{
                color: blue;
                font-size: 20px;
            }

            input, select, option{
                font-size: 20px;
            }

            .description{
                color: cadetblue;
                margin-bottom: 20px;

            }

            button, input[type="submit"]{
                font-size:20px;
                padding: 10px;
            }
            table{
                margin-top: 0px;
            }



            #data td{
                position: fixed;

            }
            
            input[type=checkbox]
            {
             /* Double-sized Checkboxes */
             -ms-transform: scale(2); /* IE */
             -moz-transform: scale(2); /* FF */
             -webkit-transform: scale(2); /* Safari and Chrome */
             -o-transform: scale(2); /* Opera */
             padding: 10px;
            }


        </style>
         <script type="text/javascript">
            jQuery(document).ready(function(){
               jQuery("#userID").change(function(){
                   jQuery("#userName").val(jQuery("#userID option:selected").text());
               }) ;
            });
        </script>
    </head>

    <body onload="document.forms[0].reset()" onsubmit="return checkValues();">
   
        <%@include file="/to_fba/CommonHeader.jsp" %>
        > <a href='/HB5/inventory_management.jsp'>Inventory Management </a> >  Track all orders packed today
        <form action="/HB5/StoreCustomerPacking" id="store" method="post" >
                <table id="table">
                    <tr>
                        <td>Created By : </td>
                        <td><select id="userID" name="userID">
                                <c:forEach items="${requestScope.users}" var="user" >
                                    <c:choose>
                                    <c:when test="${requestScope.user_id eq user.userID}">
                                        <option value="${user.userID}" selected="selected">${user.userName}</option>
                                    </c:when>
                                    <c:otherwise>
                                         <option value="${user.userID}">${user.userName}</option>
                                    </c:otherwise>
                                   
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <input type="hidden" name="userName" id="userName" value="${requestScope.username}"  data-userid="${requestScope.user_id}">
                        </td></tr><tr><td></td>
                        <td>
                            <input type="submit" value="Submit" />
                        </td>
                    </tr>
                    
                </table>
            <div id="content">
                <br/>
                Keep the cursor at below box and transmit the data from your scanner.
                <br/>

<textarea class="lined" rows="300" cols="40" name="order_ids"> 
</textarea>


            </div>

        </form>
 <script>
$(function() {
	$(".lined").linedtextarea(
		{selectedLine: 1}
	);
});
</script>
       
    </body>


</html>