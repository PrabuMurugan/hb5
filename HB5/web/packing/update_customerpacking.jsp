<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
    <head>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="jquery.autotab-1.1b.js"></script>

        <script src="common.js"></script>
        <style>
            a.error{
                color:red!important;
            }
        </style>
        


        <link rel="stylesheet" href="${pageContext.request.contextPath}/to_fba/common.css" type="text/css" />
        <style>
            body{
                font-family: sans-serif;
                font-size: medium;
                background-color: #fff; 
                /*background-image: 
                    linear-gradient(90deg, black 0%, black 10%);
                /*radial-gradient(50px 50px, circle closest-side, black, white);*/



            }

            a{
                 font-size: 20px;
            }

            a:hover{
                color: blue;
                font-size: 20px;
            }

            input, select, option{
                font-size: 20px;
            }

            .description{
                color: cadetblue;
                margin-bottom: 20px;

            }

            button, input[type="submit"]{
                font-size:20px;
                padding: 10px;
            }
            table{
                margin-top: 0px;
            }

            tr.border_bottom td {
                border-bottom:1pt solid black;
                font-weight: bold;
            }

            #data td{
                position: fixed;

            }
            
            input[type=checkbox]
            {
             /* Double-sized Checkboxes */
             -ms-transform: scale(2); /* IE */
             -moz-transform: scale(2); /* FF */
             -webkit-transform: scale(2); /* Safari and Chrome */
             -o-transform: scale(2); /* Opera */
             padding: 10px;
            }


        </style>
         <script type="text/javascript">
            jQuery(document).ready(function(){
               jQuery("#search").click(function(){
                   if(jQuery("#fnsku").val().length > 0){
                       $("#store-search").submit();
                   }else{
                       alert("Enter the FNSKU to search");
                   }
               }) ;
            });
        </script>
    </head>

    <body>
   
        <%@include file="CommonHeader.jsp" %>
        > <a href='/HB5/to_fba'>Scan FBA labels and prepare FBA shipment  </a> > Update/Delete existing FNSKUs scanned
        <form action="/HB5/StoreTrackFBALabelling" id="store-search" method="post" >
            <input type="hidden" name="action" value="search">
            <br>
                <table id="table">
                    <tr >
                        <td>FNSKU : </td>
                        <td>
                            <input type="text" name="fnsku" id="fnsku" value="${requestScope.fnsku}"  >
                        </td></tr><tr><td></td>
                        <td>
                            <input type="button" value="Submit" id="search" />
                        </td>
                    </tr>
                    
                </table>
        </form>
                            <c:if test="${requestScope.updated}">
                                <h1 style='color:green'> INFO : Updated in the database successfully </h1>
                            </c:if>
                            <c:if test="${requestScope.showResult}">
      <form action="/HB5/StoreTrackFBALabelling" id="store-update" method="post" >
          <input type="hidden" name="action" value="update">
            <div id="content">
                <br/>
                <table width="100%">
                    <tr class="border_bottom">
                       
                        <td>
                            ID
                        </td>
                        <td>
                            Date
                        </td>

                        <td>
                            User Name
                        </td>  
                        <td width="400px">Quantity</td>

                    </tr>

                    <c:forEach items="${requestScope.fBADetails}" var="fBADetail" varStatus="count">
                        <tr class="row">
                            
                            <td width="20%">
                                <input type="hidden"  id="id-${count.index}" name="id-${count.index}" class="id"  value="${fBADetail.id}" /> ${fBADetail.id}            
                            </td>
                            <td width="20%">
                                <fmt:formatDate value="${fBADetail.addDate}" pattern="dd/MM/yyyy" />
                                
                            </td>


                            <td>
                               ${fBADetail.userName}

                            </td>
                             <td >
                                 <input type="hidden" id="qty-old-${count.index}" name="qty-old-${count.index}" class="qty"    value="${fBADetail.qty}"/> 
                                 <input type="text" id="qty-${count.index}" name="qty-${count.index}" class="qty"   maxlength="8" size="12"  value="${fBADetail.qty}"/> 
                            </td>
                        </tr>
                    </c:forEach>

                </table>

            </div>
          <input type="hidden" value="${requestScope.totalResult}" name="total">
                <input type="submit" value="Save" />
        </form>
        </c:if>
        <iframe id="audio_file" style="visibility: hidden"> </iframe>

       
    </body>


</html>