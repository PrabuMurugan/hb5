<%-- 
    Document   : balance_units_in_shelf
    Created on : Aug 2, 2013, 3:45:10 PM
    Author     : prabu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>
    $(document).ready(function() {
        $('input[name=q1]').get(0).focus();
                $("form").submit(function() {

                    v=$("input[name='q1']").val();
                    sel=$( "select.foo option:selected").val();
                    if(sel=="6" || sel=="8")
                        $("#results").attr("src","Q?sno="+sel+"&q1="+v+"&q1q2same=true");
                    else
                        $("#results").attr("src","Q?sno="+sel+"&q1="+v);

                    $("input[name='q1']").focus(function() { $(this).select(); } );
                   $("input[name='q1']").focus();
                    return false;

            });         
    });

    function playsound(sound) {
    
      $("#dummy").html('<audio autoplay="autoplay">    <source src="voice/' + sound + '.wav" type="audio/mpeg"></audio>');
    
    }
</script>        
    </head>
    <body>
        <%@include file="to_fba/CommonHeader.jsp" %>
        &gt;
        <a href="inventory_management.jsp">Inventory Management</a> &gt; Check Balance Inventory for an item/Bin, Audit an Item/Bin
        <h1>Scan the bin location to audit the bin</h1>
        <form action="Q">
     
             <input type="hidden" name="origin" value="inventory_query.jsp"/>
             <select name="sno" class="foo">
                 <option value="6"> Scan an UPC or part of item name like tophe and check the balance of the item</option>
                 <option value="8">  Scan an UPC  and audit the item's history</option>
                 <option value="NA"> ------------------</option>
                 <option value="10"> Scan a bin and check the balance</option>
                 <option value="11"> Scan a bin and audit the bin's history</option>
                 
                 
                 
             </select>
             <br/><br/><br/>
            <input type="text" name="q1" value=""/>  
             
            <input type="submit"  value="Submit"/>
            
        </form>
         <div id="dummy"></div>
 <iframe src="" id="results" style="width:100%;height:400px"/>     
 
    </body>
</html>
