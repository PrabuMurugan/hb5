<%-- 
    Document   : FinalPackingQty
    Created on : Oct 18, 2013, 2:11:24 AM
    Author     : Raj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Success Page</title>
        
    </head>
    <body>
        
        <h3>Response Message : </h3><%=(String)request.getAttribute("responseMessage")%><br/>
        <h3>Response Code : </h3><%=(String)request.getAttribute("responseCode")%><br/><br/>
        
        <h3><a href="<%=(String)request.getAttribute("responsePage")%>">Home</a></h3><br/>
    </body>
</html>
