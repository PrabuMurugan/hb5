<%-- 
    Document   : Init.jsp
    Created on : Oct 18, 2013, 2:11:24 AM
    Author     : Raj
--%>

<%@page import="shipstation.Properties"%>
<!DOCTYPE html>
<%@page import="java.util.ArrayList" %>
<%@page import="shipstation.Feed" %>
<%@page import="shipstation.Entry" %>
<%@page import="shipstation.Properties" %>
<%@page import="constants.ActionConstants" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- DC DataGrid CSS -->
        <link rel="stylesheet" href="http://www.dreamtemplate.com/dreamcodes/datagrid/css/tsc_datagrid.css" />
 
        <!-- jQuery Library (skip this step if already called on page ) -->
        <script type="text/javascript" src="http://www.dreamtemplate.com/dreamcodes/jquery.min.js"></script> <!-- (do not call twice) -->

        <!-- DC DataGrid JS -->
        <script type="text/javascript" src="http://www.dreamtemplate.com/dreamcodes/datagrid/js/tsc_datagrid.js"></script>
        
        <!-- DC Gradient Buttons CSS -->
        <link type="text/css" rel="stylesheet" href="http://www.dreamtemplate.com/dreamcodes/gradient_buttons/css/tsc_gradient_buttons.css" />
        <script>
            function submitForm() {
                document.forms[0].submit();
            }
            
        </script>
    </head>
    <body>
        <form name="FinalPackingQtyForm" id="OrderListForm" action="/HB5/ShipStationServlet" method="post">     
            <input type="hidden" name="actionid" id="actionid" value="2">
       <!-- DC DataGrid Start -->
                <div id="tsort-tablewrapper" style="width:90%;">
                  <div id="tsort-tableheader">
                    <div class="tsort-search">
                      <select id="tsort-columns" onchange="sorter.search('query')">
                      </select>
                      <input type="text" id="query" onkeyup="sorter.search('query')" />
                    </div>
                    <span class="tsort-details">
                    <div>Records <span id="tsort-startrecord"></span>-<span id="tsort-endrecord"></span> of <span id="tsort-totalrecords"></span></div>
                    <div><a href="javascript:sorter.reset()">reset</a></div>
                    </span> </div>
                  <table cellpadding="0" cellspacing="0" border="0" id="tsctablesort1" class="tinytable">
                    <thead>
                      <tr>
                        <th><h4>Order #</h4></th>
                        <th><h4>Items</h4></th>
                        <th><h4>Amount Paid</h4></th>
                        <th><h4>Rate</h4></th>
                        <th><h4>Weight(Ozs)</h4></th>
                        <th><h4>Dimensions(L x W x H)</h4></th>
                      </tr>
                    </thead>
                    <tbody>
                      <%
                          Feed feed = (Feed) request.getAttribute("feed");
                          if(feed != null) {
                              System.out.println(feed.getEntries().size());
                          for(Entry entry : feed.getEntries()) {
                              Properties prop = entry.getContent().getProperties();
                      %>
                      <tr>
                        <td width="17%"><font size="4"><%=prop.getOrdernumber() %></font></td>
                        <td width="17%"><font size="4"><%=prop.getQuantity() %></font></td>
                        <td width="17%"><font size="4"><%=prop.getOrdertotal() %></font></td>
                        <td width="17%"><font size="4"><%=prop.getCustomerid() %></font></td>
                        <td width="17%"><font size="4"><%=prop.getWeightoz() %></font></td>
                        
                        <%
                        
                              String length = prop.getLength();
                              String width = prop.getWidth();
                              String height = prop.getHeight();
                              String display = length + " x " + width + " x " + height;
                              display = display.length()>7 ? display : "-";
                        %>
                        <td width="17%"><font size="4"><%=display %></font></td>
                      </tr>

                      <%
                          }
                          
                          }
                      %>
                    </tbody>
                  </table>
                  <div id="tsort-tablefooter">
                    <div id="tsort-tablenav">
                      <div> <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" /> 
                          <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" /> 
                          <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" /> 
                          <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" /> </div>
                      <div>
                        <select id="tsort-pagedropdown">
                        </select>
                      </div>
                      <div> <a href="javascript:sorter.showall()">view all</a> </div>
                    </div>
                    <div id="tsort-tablelocation">
                      <div>
                        <select onchange="sorter.size(this.value)">
                          <option value="5">5</option>
                          <option value="10" selected="selected">10</option>
                          <option value="20">20</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span> </div>
                      <div class="tsort-page">Page <span id="tsort-currentpage"></span> of <span id="tsort-totalpages"></span></div>
                    </div>
                  </div>
                </div>
                    
    <!-- DC Gradient Buttons Start -->
    <div>
       <br/>
    <a href="javascript:submitForm()" class="tsc_buttons2 large grey">Update Quentity</a>
    <!-- DC Gradient Buttons End -->
    </div>
     <%
                              String strRes = (String)request.getAttribute("responseMessage");
                              if(strRes != null && !strRes.equalsIgnoreCase("")) {
        %>
           <br/> <center><h2><b>Message :</b><%=(String)request.getAttribute("responseMessage")%> <br/><br/>
               <b>Code    :</b><%=(String)request.getAttribute("responseCode")%></h2></center><br/>
           <%
                              }
           %>
        </form>
<!-- DC DataGrid Settings -->
<script type="text/javascript">
    var sorter = new TINY.table.sorter('sorter','tsctablesort1',{
        headclass:'head',
        ascclass:'asc',
        descclass:'desc',
        evenclass:'tsort-evenrow',
        oddclass:'tsort-oddrow',
        evenselclass:'tsort-evenselected',
        oddselclass:'tsort-oddselected',
        paginate:true, // pagination (true,false)
        size:10, // show 10 results per page
        colddid:'tsort-columns',
        currentid:'tsort-currentpage',
        totalid:'tsort-totalpages',
        startingrecid:'tsort-startrecord',
        endingrecid:'tsort-endrecord',
        totalrecid:'tsort-totalrecords',
        hoverid:'tsort-selectedrow',
        pageddid:'tsort-pagedropdown',
        navid:'tsort-tablenav',
        sortcolumn:1, // sort column 1
        sortdir:1, // sort direction
        columns:[{index:6, format:'%', decimals:1},{index:7, format:'$', decimals:0}], // classify for proper sorting
        init:true // activate datagrid (true,false)
    });
  </script>
<!-- DC DataGrid End -->
<div class="tsc_clear"></div> <!-- line break/clear line -->
    </body>
</html>

