    <%-- 
        Document   : FinalPackingQty
        Created on : Oct 18, 2013, 2:11:24 AM
        Author     : Raj
    --%>

    <!DOCTYPE html>
    <%@page import="java.util.ArrayList" %>
    <%@page import="entities.Products" %>
    <%@page import="constants.ActionConstants" %>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
            <!-- DC DataGrid CSS -->
            <link rel="stylesheet" href="http://www.dreamtemplate.com/dreamcodes/datagrid/css/tsc_datagrid.css" />

            <!-- jQuery Library (skip this step if already called on page ) -->
            <script type="text/javascript" src="http://www.dreamtemplate.com/dreamcodes/jquery.min.js"></script> <!-- (do not call twice) -->
             <script type="text/javascript" src="mespeak/mespeak.js?v=1.9.2.2"></script>
            <script type="text/javascript">
              meSpeak.loadConfig("mespeak/mespeak_config.json");
              meSpeak.loadVoice("mespeak/voices/en/en.json");

              function loadVoice(id) {
                var fname="mespeak/voices/"+id+".json";
                meSpeak.loadVoice(fname, voiceLoaded);
              }

              function voiceLoaded(success, message) {
                if (success) {
                  alert("Voice loaded: "+message+".");
                }
                else {
                  alert("Failed to load a voice: "+message);
                }
              }

              /*
                auto-speak glue:
                additional functions for generating a link and parsing any url-params provided for auto-speak
              */

              var formFields = ['text','amplitude','wordgap','pitch','speed'];

              function autoSpeak() {
                // checks url for speech params, sets and plays them, if found.
                // also adds eventListeners to update a link with those params using current values
                var i,l,n,params,pairs,pair,
                    speakNow=null,
                    useDefaultVoice=true,
                    q=document.location.search,
                    f=document.getElementById('speakData'),
                    s1=document.getElementById('variantSelect'),
                    s2=document.getElementById('voiceSelect');
                if (!f || !s2) return; // form and/or select not found
                if (q.length>1) {
                  // parse url-params
                  params={};
                  pairs=q.substring(1).split('&');
                  for (i=0, l=pairs.length; i<l; i++) {
                    pair=pairs[i].split('=');
                    if (pair.length==2) params[pair[0]]=decodeURIComponent(pair[1]);
                  }
                  // insert params into the form or complete them from defaults in form
                  for (i=0, l=formFields.length; i<l; i++) {
                    n=formFields[i];
                    if (params[n]) {
                      f.elements[n].value=params[n];
                    }
                    else {
                      params[n]=f.elements[n].value;
                    }
                  }
                  if (params.variant) {
                    for (i=0, l=s1.options.length; i<l; i++) {
                          if (s1.options[i].value==params.variant) {
                            s1.selectedIndex=i;
                            break;
                          }
                    }
                  }
                  else {
                    params.variant='';
                  }
                  // compile a function to speak with given params for later use
                  // play only, if param "auto" is set to "true" or "1"
                  if (params.auto=='true' || params.auto=='1') {
                    speakNow = function() {
                      meSpeak.speak(params.text, {
                        amplitude: params.amplitude,
                        wordgap: params.wordgap,
                        pitch: params.pitch,
                        speed: params.speed,
                        variant: params.variant
                      });
                    };
                  }
                  // check for any voice specified by the params (other than the default)
                  if (params.voice && params.voice!=s2.options[s2.selectedIndex].value) {
                    // search selected voice in selector
                    for (i=0, l=s2.options.length; i<l; i++) {
                      if (s2.options[i].value==params.voice) {
                        // voice found: adjust the form, load voice-data and provide a callback to speak
                        s2.selectedIndex=i;
                        meSpeak.loadVoice('mespeak/voices/'+params.voice+'.json', function(success, message) {
                          if (success) {
                            if (speakNow) speakNow();
                          }
                          else {
                            if (window.console) console.log('Failed to load requested voice: '+message);
                          }
                        });
                        useDefaultVoice=false;
                        break;
                      }
                    }
                  }
                  // standard voice: speak (deferred until config is loaded)
                  if (speakNow && useDefaultVoice) speakNow();
                }
                // initial url-processing done, add eventListeners for updating the link
                for (i=0, l=formFields.length; i<l; i++) {
                  f.elements[formFields[i]].addEventListener('change', updateSpeakLink, false);
                }
                s1.addEventListener('change', updateSpeakLink, false);
                s2.addEventListener('change', updateSpeakLink, false);
                // finally, inject a link with current values into the page
                updateSpeakLink();
              }

              function updateSpeakLink() {
                // injects a link for auto-execution using current values into the page
                var i,l,n,f,s,v,url,el,params=new Array();
                // collect values from form
                f=document.getElementById('speakData');
                for (i=0, l=formFields.length; i<l; i++) {
                  n=formFields[i];
                  params.push(n+'='+encodeURIComponent(f.elements[n].value));
                }
                // get variant
                s=document.getElementById('variantSelect');
                if (s.selectedIndex>=0) params.push('variant='+s.options[s.selectedIndex].value);
                // get current voice, default to 'en/en' as a last resort
                s=document.getElementById('voiceSelect');
                if (s.selectedIndex>=0) v=s.options[s.selectedIndex].value;
                if (!v) v=meSpeak.getDefaultVoice() || 'en/en';
                params.push('voice='+encodeURIComponent(v));
                params.push('auto=true');
                // assemble the url and add it as GET-link to the page
                url='?'+params.join('&');
                url=url.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\"/g, '&quot;');
                el=document.getElementById('linkdisplay');
                if (el) el.innerHTML='Instant Link: <a href="'+url+'">Speak this</a>.';
              }

              // trigger auto-speak at DOMContentLoaded
              if (document.addEventListener) document.addEventListener( "DOMContentLoaded", autoSpeak, false );

              /*
                end of auto-speak glue
              */

            </script>

     <script>
           jQuery(document).ready(function(){
            jQuery("#searchProductForm #searchBtn").click(function(){
                if(jQuery.trim(jQuery("#searchProductForm #searchText").val()) !="" && jQuery.trim(jQuery("#searchText").val()).length >0){
                    jQuery("#searchProductForm").submit();
                }else{
                    alert("Please enter Location to search.");
                }
            });
            
            
            
            
        });
        function addRow() { 
            jQuery("#tsctablesort1").find("td:last").append("<span style='cursor:pointer;color:blue;float:right;' id='addNewRow' onclick='bindAddRow()'>+</span>") 
        }
        
     function deleteQty() { 
            jQuery("#actionid").val('<%=ActionConstants.DELETE_FINAL_PACKAGIN_QTY%>');
            
            var records = document.getElementsByName('delete_row');
            var selectedvalues='';
            for(i=0; i<records.length;i++) {
                if(records[i].checked) {
                    selectedvalues+= "'"+records[i].value+"',";
                }
             
            }
            jQuery("#h_upc_to_delete").val(selectedvalues);
               
            $("#ManualQtyUpdateForm").submit();
        }
        
        function bindAddRow(){
                $("#addNewRow").remove();
                var sl = parseInt(jQuery("#total_new").val())+1;
                jQuery("#total_new").val(sl);
                jQuery("#tsctablesort1").append("<tr><td></td><td><input name='new_loc_"+sl+"' type='text' ></td><td><input name='new_upc_"+sl+"'  type='text'> </td><td></td><td></td><td> <input name='new_quantity_"+sl+"' type='text' > <span style='cursor:pointer;color:blue;float:right;' id='addNewRow' onclick='bindAddRow()'>+</span></td></tr>");

            }
      </script>
            <!-- DC DataGrid JS -->
            <script type="text/javascript" src="http://www.dreamtemplate.com/dreamcodes/datagrid/js/tsc_datagrid.js"></script>

            <!-- DC Gradient Buttons CSS -->
            <link type="text/css" rel="stylesheet" href="http://www.dreamtemplate.com/dreamcodes/gradient_buttons/css/tsc_gradient_buttons.css" />
            <script>
                function submitForm() {
                    $("#ManualQtyUpdateForm").submit();
                }
                    function speak(e){
                       
                       if (e.keyCode === 13) {
                             document.getElementsByName('query')[0].select();
                             var numOfVisibleRows = $('tr:visible').length;
                             var final='';
                             var found=0;
                             for(i =1;i< (numOfVisibleRows-1);i++) {
                                 found=1;
                                 var row = $('tr:visible')[i];
                                 var count = row.cells[4].innerText;
                                 final +=  count + ' ';
                             }

                             if(found===1) {
                                meSpeak.speak(final,{speed:140,variant:"f2"});
                             } else {
                                 meSpeak.speak('Not Found');
                             }
                        }
                        
                    }
                  
            </script>
        </head>
        <body>
            
<!--             <form name="searchProductForm" id="searchProductForm" action="/HB5/ManualQtyUpdate" method="post"> 
                 <input type="hidden" name="actionid" id="actionid" value="7">
                 <div style="margin-top: 20px;">
                     <table><tr><td><input name="searchText" id="searchText" type="text"></td><td><input id="searchBtn" type="button" value="Search By Location" class="tsc_buttons2 small grey" ></td></tr></table>
                 </div>
             </form>--> 
 <%@include file="/to_fba/CommonHeader.jsp" %>
        &gt;
        <a href="inventory_management.jsp">Inventory Management</a> &gt; Manual Qty Update<br/>
        <p> 
        </p>        
        <form name="ManualQtyUpdateForm" id="ManualQtyUpdateForm" action="/HB5/ManualQtyUpdate" method="post">     
                <input type="hidden" name="actionid" id="actionid" value="4">
                <input type="hidden" name="h_upc_to_delete" id="h_upc_to_delete" value="<%=ActionConstants.DELETE_FINAL_PACKAGIN_QTY%>">
           <!-- DC DataGrid Start -->
                    <div id="tsort-tablewrapper" style="width:90%;">
                      <div id="tsort-tableheader">
                        <div class="tsort-search">
                          <select id="tsort-columns" onchange="sorter.search('query')">
                          </select>
                            <input type="text" name="query" id="query" onkeyup="sorter.search('query');speak(event);" />
                        </div>
                        <span class="tsort-details">
                        <div>Records <span id="tsort-startrecord"></span>-<span id="tsort-endrecord"></span> of <span id="tsort-totalrecords"></span></div>
                        <div><a href="javascript:sorter.reset()">reset</a></div>
                        </span> </div>
                      <table cellpadding="0" cellspacing="0" border="0" id="tsctablesort1" class="tinytable">
                        <thead>
                          <tr>
                            <th><h3>Select</h3></th>
                            <th><h3>Location</h3></th>
                            <th><h3>UPC</h3></th>
                            <th><h3>Title</h3></th>
                            <th><h3>Expected Units</h3></th>
                            <th width="150"><h3 >Total Units</h3></th>
                          </tr>
                        </thead>
                        <tbody>
                          <%
                              ArrayList<Products> productses = (ArrayList<Products>) request.getAttribute("Products");
                              int i=0;
                              for(Products products : productses) {
                                  String strTitle = products.getTitle();
                                //  System.out.println(strTitle.contains("(Pack"));
                                  if(strTitle.contains("(Pack")) {
                                    strTitle = strTitle.substring(0, strTitle.indexOf("(Pack"));
                                  }
                                 String str = products.getTotalUnits();
                                
                          %>
                          <tr>
                              <td width="5%"><input type="checkbox" name="delete_row" value="<%=(products.getUpc())%>"/></td>
                            <td width="8%"><font size="5"><%=products.getLocation() %></font></td>
                            <td width="8%"><font size="5"><%=products.getUpc() %></font></td>
                            <td width="50%"><font size="5"><%=strTitle %></font></td>
                            <td width="5%"><font size="5"><%=( Integer.parseInt(str) <= 0 ? 0 :  Integer.parseInt(str)) %></font></td>
                            <td width="5%"><input type="text" name="Qty<%=i %>"></td>
                            <input type="hidden" name="BARCODE<%=i++%>" value="<%=products.getId()%>" /> 
                          </tr>

                          <%
                              }
                          %>
                        </tbody>
                      </table>
                      <div id="tsort-tablefooter">
                        <div id="tsort-tablenav">
                          <div> <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" /> 
                              <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" /> 
                              <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" /> 
                              <img src="http://www.dreamtemplate.com/dreamcodes/datagrid/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" /> </div>
                          <div>
                            <select id="tsort-pagedropdown">
                            </select>
                          </div>
                          <div> <a href="javascript:sorter.showall()">view all</a> </div>
                        </div>
                        <div id="tsort-tablelocation">
                          <div>
                            <select onchange="sorter.size(this.value)">
                              <option value="5">5</option>
                              <option value="10" selected="selected">10</option>
                              <option value="20">20</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span> </div>
                          <div class="tsort-page">Page <span id="tsort-currentpage"></span> of <span id="tsort-totalpages"></span></div>
                        </div>
                      </div>
                    </div>
                        
                        <div id="newRecords">
                            <input type="hidden" name="total_new" id="total_new" value="0" >
                        </div>
        <!-- DC Gradient Buttons Start -->
        <div>
           <br/>
             <a href="javascript:window.print()" class="tsc_buttons2 large grey">Print</a>
             <a href="javascript:bindAddRow()" class="tsc_buttons2 large grey">Add Row</a>
             <a href="javascript:submitForm()" class="tsc_buttons2 large grey">Update Quantity</a>
              <a href="javascript:deleteQty()" class="tsc_buttons2 large grey">Delete Rows</a>
        <!-- DC Gradient Buttons End -->

         <!-- DC Gradient Buttons End -->
          </div>
        <%
                              String strRes = (String)request.getAttribute("responseMessage");
                              if(strRes != null && !strRes.equalsIgnoreCase("")) {
        %>
           <br/>  <center><h2><b>Message :</b><%=(String)request.getAttribute("responseMessage")%> <br/><br/>
            <b>Code    :</b><%=(String)request.getAttribute("responseCode")%><h2></center><br/>
           <%
                              }
           %>
        </form>
    <!-- DC DataGrid Settings -->
    <script type="text/javascript">
        var sorter = new TINY.table.sorter('sorter','tsctablesort1',{
            headclass:'head',
            ascclass:'asc',
            descclass:'desc',
            evenclass:'tsort-evenrow',
            oddclass:'tsort-oddrow',
            evenselclass:'tsort-evenselected',
            oddselclass:'tsort-oddselected',
            paginate:true, // pagination (true,false)
            size:10, // show 10 results per page
            colddid:'tsort-columns',
            currentid:'tsort-currentpage',
            totalid:'tsort-totalpages',
            startingrecid:'tsort-startrecord',
            endingrecid:'tsort-endrecord',
            totalrecid:'tsort-totalrecords',
            hoverid:'tsort-selectedrow',
            pageddid:'tsort-pagedropdown',
            navid:'tsort-tablenav',
            sortcolumn:1, // sort column 1
            sortdir:1, // sort direction
            sum:[8], // create totalsum for column 8
          //  avg:[6,7,8,9], // create averages for column 6,7,8,9
          //  columns:[{index:6, format:'%', decimals:1},{index:7, format:'$', decimals:0}], // classify for proper sorting
            init:true // activate datagrid (true,false)
        });
        
      </script>
    <!-- DC DataGrid End -->
    <div class="tsc_clear"></div> <!-- line break/clear line -->
        </body>
    </html>

    