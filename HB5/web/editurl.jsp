<%-- 
    Document   : saveprice
    Created on : 18 Oct, 2013, 3:26:54 PM
    Author     : chand
--%>

<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    </head>
    <body>
         <%@include file="/to_fba/CommonHeader.jsp" %>
        &gt;
        <a href="inventory_management.jsp">Inventory Management</a> &gt; Populate Google Shopping URL for each product being sold in HB<br/>
        <p> 
        </p>      
        
        <% if(request.getParameter("result") != null){%><%=request.getParameter("result")%><%}%>
        <br>
      
             
        <br><br><br>
        <form action="GoogelShoppingUrl" method="POST">
            <% 
            java.util.LinkedList data =  new java.util.LinkedList();   
            if(request != null && request.getAttribute("data") != null)
               data = (java.util.LinkedList)request.getAttribute("data");  
            
                %>
            <input type="submit" value="Save" />
            <table border="1" width="75%">
                <tr>
                   
                    <th>Item Name</th>
                    <th>Barcode</th>
                    <th>Google Shopping Url</th>
                </tr>
                <% 
                for(Object row : data){
                    int i = 0;
                    %>
                    <tr>
                    <% for(Object coloumn : (String[])row){    %>
                    <td>
                        
                       <% if(i ==0){ %> 
                        <input style="border:none" type="text" name="title" value="<%=coloumn%>" size="<%=coloumn.toString().length()+2%>" readonly="readonly">
                        <%} else if(i ==1){%> 
                        <input style="border:none" type="hidden" name="barcode" value="<%=coloumn%>" readonly="readonly"> 
                        <a href="https://www.google.com/search?hl=en&tbm=shop&q=<%=coloumn%>" target="_blank"><%=coloumn%></a>
                        <%  } i++; %>
                        
                        </td>
                        <% } %>
                        <td><input type="text" name="google_shopping_page" value="" size="75"></td>
                    </tr>   
                    <% } %>
            </table>
            <input type="submit" value="Save"/>
            </form>
            
            <ol>
                <li>The purpose of this module is to collect google shopping urls for each barcode that we sell in HB. Finally click the save button to save the values.</li>
                <li>Click the barcode. It opens the google shopping for that barcode.</li>
                <li>In the result page find any row that has "from 2 sellers" or "from 21 sellers",
                click on that row link. Now copy that url where you redirected.(e.g
https://www.google.com/shopping/product/6808568532335905656?hl=en&gs_rn=32&gs_ri=psy-ab&tok=8FX854HsVQVSIxluwV-yrw&ds=sh&pq=00791090300551&cp=7&gs_id=x&xhr=t&q=apple+cider+vinegar&pf=p&safe=off&sclient=psy-ab&oq=apple+c&pbx=1&bav=on.2,or.r_cp.r_qf.&bvm=bv.58187178,d.eW0&biw=1920&bih=954&tch=1&ech=7&psi=SJK0UrjAIIy3kAfO64GIBw.1387565641013.1&sa=X&ei=n5O0Up-_EJS1kQeFj4D4BA&sqi=2&ved=0CIEBEJ8uMAA )</li>
            </li>
            <li>If the product is not found in google or if you dont see the item sold by multiple sellers, put NA.</li>
            </ol>
    </body>
</html>
