/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author US083274
 */
public class PacksInCase extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            PreparedStatement ps = con.prepareStatement("select  distinct replace(replace(replace(upc,'UPC:',''),'-',''),' ',''),packs_per_case ,length(replace(replace(upc,'UPC:',''),'-','')) from orders_placed union select distinct   replace(replace(replace(upc,'UPC:',''),'-',''),' ',''),packs_per_case,length(replace(replace(upc,'UPC:',''),'-',''))  from orders_received");
            ResultSet rs = ps.executeQuery();
            JSONArray jsarr = new JSONArray();
            JSONObject js = new JSONObject();
            while (rs.next()) {
                String upc = rs.getString(1);
                String pks = rs.getString(2);
                js = new JSONObject();
                js.put("upc", upc);
                js.put("pks", pks);
                jsarr.add(js);
            }
            js = new JSONObject();
            js.put("packs_in_case", jsarr);
            out.println(js.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
