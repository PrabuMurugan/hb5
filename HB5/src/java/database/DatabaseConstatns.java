package database;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class DatabaseConstatns {
    public static final String DB_USERNAME = "USERNAME";
    public static final String DB_PASSWORD = "PASSWORD";
    public static final String DB_PORT = "PORT";
    public static final String DB_HOST = "HOST";
    public static final String DB_SCHEMA_TAJ = "SCHEMA_TAJ";
    public static final String DB_SCHEMA_HARRI = "SCHEMA_HARRI";
}
