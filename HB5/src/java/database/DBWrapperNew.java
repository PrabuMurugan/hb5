package database;

import entities.FBABox;
import java.sql.Connection;
import entities.FinalPackingQty;
import entities.Products;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

import util.DBNew;
import util.HBLoggerNew;

public class DBWrapperNew {
    public static String HBG="harrisburgstore";
    public static String PICKUP_HBG="pickup_hb";
    public static String TJ="tajplaza";
    public static String PICKUP="pickup_hb";
    public static String BIGCOMMERCE="hb_bc";
    private static final String MODULE = "DB";

    public static Connection getConnection(HttpServletRequest request, String schema) throws Exception {
        Connection con = null;
        String user = "root";
        String password = "admin123";
        System.out.println("INSIDE DBWrapperNew , GOING TO CONNECT TO "+schema);
        Class.forName("com.mysql.jdbc.Driver");
 
       
        try {
            long lNow = System.currentTimeMillis();
            System.out.println("prabu : Asking for connection at : " + lNow);
            util.DBNew.CONNECT_TO_PROD_DEBUG=true;
            util.DBNew.CONNECT_TO_PROD_DEBUG=true;
             System.out.println("PRABU2 GOING TO CONNECT");
            con = util.DBNew.getConnection(schema);
            long lAfter = System.currentTimeMillis();
            System.out.println("Got connection : " + lAfter + ". Time taken : " + (lAfter-lNow));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR WHILE CONNECTING FIRST TIME");
            System.out.println(request.getServerPort());
        }

        return con;
    }

    public static synchronized long getSequenceNumberPerDate(String sequenceName, HttpServletRequest request, String schemaName) {
        try {
            Connection con = getConnection(request, schemaName);
            PreparedStatement updateStatement = con.prepareStatement("update daily_sequence set sequence_update_ts=?, value=? where sequence_name=?");
            PreparedStatement selectStatement = con.prepareStatement("select value from daily_sequence where sequence_name=? and date(sequence_update_ts) = ?");
            PreparedStatement insertStatement = con.prepareStatement("insert into daily_sequence(sequence_update_ts, value, sequence_name) values(?,?,?)");
            selectStatement.setString(1, sequenceName);
            selectStatement.setDate(2, new Date(System.currentTimeMillis()));
            ResultSet selected = selectStatement.executeQuery();

            long value = 1;
            if (selected.next()) {
                value = selected.getLong(1);
                selected.close();
                updateStatement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                updateStatement.setLong(2, value + 1);
                updateStatement.setString(3, sequenceName);
                updateStatement.executeUpdate();
            } else {
                selected.close();
                insertStatement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                insertStatement.setLong(2, value);
                insertStatement.setString(3, sequenceName);
                insertStatement.executeUpdate();
            }
            return value;
        } catch (Exception ex) {
            Logger.getLogger(DBNew.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<FinalPackingQty> getFinalPackingQty(String strpackingQty, Connection connection) {
        ArrayList<FinalPackingQty> finalPackingQtys = new ArrayList<FinalPackingQty>();
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside getFinalPackingQty of DBWrapperNew. strpackingQty : {0}", strpackingQty);
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {

            preparedStatement = connection.prepareStatement(SQLConstants.GET_FINAL_PACKAGIN_QTY_BY_PACKING_QTY);
            preparedStatement.setString(1, strpackingQty);
            resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    FinalPackingQty finalPackingQty = new FinalPackingQty();
                    finalPackingQty.setId(resultSet.getInt(SQLConstants.GET_FINAL_PACKAGIN_QTY_ID));
                    finalPackingQty.setAsin(resultSet.getString(SQLConstants.GET_FINAL_PACKAGIN_QTY_ASIN));
                    finalPackingQty.setSku(resultSet.getString(SQLConstants.GET_FINAL_PACKAGIN_QTY_SKU));
                    finalPackingQty.setPakingQty(resultSet.getString(SQLConstants.GET_FINAL_PACKAGIN_QTY_PAC_QTY));
                    finalPackingQty.setTitle(resultSet.getString(SQLConstants.GET_FINAL_PACKAGIN_QTY_TITLE));

                    //     LOG.log(Level.INFO,"[ " + MODULE + " ] " + "finalPackingQty : {0}", finalPackingQty);
                    finalPackingQtys.add(finalPackingQty);
                }
            }

        } catch (SQLException ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while feching Final Packing Qty. Error Message : {0}", ex.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LOG.info("[ " + MODULE + " ] " + "Leaving getFinalPackingQty of DBWrapperNew");
        return finalPackingQtys;

    }

    public ArrayList<Products> getTotalQtyPerProducts(Connection connection, HttpServletRequest request, boolean isSearchResult) {
        ArrayList<Products> productses = new ArrayList<Products>();
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside getTotalQtyPerProducts of DBWrapperNew.");
        //Connection connection = null;
        long lNow = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lNow);
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {

            //connection = DBWrapperNew.getConnection(request, "harrisburgstore");
            if (isSearchResult) {
                preparedStatement = connection.prepareStatement(SQLConstants.GET_SEARCH_RESULT_FOR_PRODUCT);
                String searchString = request.getParameter("searchText");
                preparedStatement.setString(1, searchString + "%");
            } else {
                preparedStatement = connection.prepareStatement(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT);
            }
            resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    Products products = new Products();
                    products.setBarcode(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TOTAL_BARCODE));
                    products.setId(resultSet.getInt(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_ID));
                    products.setLocation(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_LOCATION));
                    products.setPrice(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_PRICE));
                    products.setProductType(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_PRODUCT_TYPE));
                    products.setTitle(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TITLE));
                    products.setTotalUnits(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TOTAL_UNITS));
                    products.setUpc(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_UPC));
                    products.setVendor(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_UPC));
                    productses.add(products);
                }
            }

        } catch (Exception ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while feching Product Quintity per Product. Error Message : {0}", ex.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long lAfter = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lAfter + ". Time taken : " + (lAfter-lNow));
        LOG.info("[ " + MODULE + " ] " + "Leaving getTotalQtyPerProducts of DBWrapperNew");
        return productses;

    }
 public ArrayList<Products> getTotalQtyPerProductsNew(Connection connection, HttpServletRequest request) {
        ArrayList<Products> productses = new ArrayList<Products>();
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside getTotalQtyPerProducts of DBWrapperNew.");
        //Connection connection = null;
        long lNow = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lNow);
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {

            String searchby = request.getParameter("searchby");
             System.out.println("searchby : " + searchby);
            preparedStatement = connection.prepareStatement(String.format(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_NEW,searchby));
            String searchString = request.getParameter("searchText");
            System.out.println("searchString : " + searchString);
            preparedStatement.setString(1, searchString + "%");
            resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    Products products = new Products();
                    products.setBarcode(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TOTAL_BARCODE));
                    products.setId(resultSet.getInt(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_ID));
                    products.setLocation(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_LOCATION));
                    products.setPrice(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_PRICE));
                    products.setProductType(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_PRODUCT_TYPE));
                    products.setTitle(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TITLE));
                    products.setTotalUnits(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_TOTAL_UNITS));
                    products.setUpc(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_UPC));
                    products.setVendor(resultSet.getString(SQLConstants.GET_TOTAL_QTY_PER_PRODUCT_UPC));
                    productses.add(products);
                }
            }

        } catch (Exception ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while feching Product Quintity per Product. Error Message : {0}", ex.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long lAfter = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lAfter + ". Time taken : " + (lAfter-lNow));
        LOG.info("[ " + MODULE + " ] " + "Leaving getTotalQtyPerProducts of DBWrapperNew");
        return productses;

    }

//    public ArrayList<PurchageOrder> getOrdersByStatus(Connection connection, String status) {
//        HBLoggerNew.debug(MODULE, "Entry getOrdersByStatus. status : " + status);
//        ArrayList<PurchageOrder> listPO = new ArrayList<PurchageOrder>();
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        try {
//            preparedStatement = connection.prepareStatement(SQLConstants.QRY_PO_BY_STATUS);
//            preparedStatement.setString(1, status);
//            resultSet = preparedStatement.executeQuery();
//            
//            if (resultSet != null) {
//                while (resultSet.next()) {
//                    PurchageOrder purchageOrder = new PurchageOrder();
//                    purchageOrder.setCreatedate(resultSet.getDate("CREATEDATE"));
//                    purchageOrder.setCreatedby(resultSet.getString("CREATEDBY"));
//                    purchageOrder.setOrderid(resultSet.getInt("ORDERID"));
//                    purchageOrder.setOrdernumber(resultSet.getString("ORDERNUMBER"));
//                    purchageOrder.setUpdateddate(resultSet.getDate("UPDATEDDATE"));
//                    purchageOrder.setUpdatedby(resultSet.getString("UPDATEDBY"));
//                    purchageOrder.setStatus(resultSet.getString("STATUS"));
//                    purchageOrder.setVisibility(resultSet.getString("VISIBILITY"));
//                    listPO.add(purchageOrder);
//                }
//            }
//
//        } catch (Exception exception) {
//            HBLoggerNew.debug(MODULE, "Exception while fetching perchage order by status :" + exception.getMessage());
//            exception.printStackTrace();
//        } finally {
//            try {
//                if (resultSet != null) {
//                    resultSet.close();
//                }
//                if (preparedStatement != null) {
//                    preparedStatement.close();
//                }
//                connection.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        HBLoggerNew.debug(MODULE,"Exit getOrdersByStatus. No of POs : " + listPO.size());
//        return listPO;
//
//    }
    public ArrayList<FBABox> fetchBoxDetail(String[] strBoxes, Connection connection) {
        ArrayList<FBABox> faFBABoxs = new ArrayList<FBABox>();
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside fetchBoxDetail of DBWrapperNew.");
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {

            String strBoxString = "";
            for (String strBox : strBoxes) {
                strBoxString += "'" + strBox + "',";
            }
            preparedStatement = connection.prepareStatement(SQLConstants.FETECH_BOX_DETAIL);
            preparedStatement.setString(1, strBoxString.substring(0, strBoxString.length() - 1));
            resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    FBABox fBABox = new FBABox();
                    fBABox.setBox(resultSet.getString(SQLConstants.FBA_BOX_BOX));
                    fBABox.setDate_sent_on_pallet(resultSet.getDate(SQLConstants.FBA_BOX_DATE_SENT_ON_PALLET));
                    fBABox.setFnsku(resultSet.getString(SQLConstants.FBA_BOX_FNSKU));
                    fBABox.setId(resultSet.getString(SQLConstants.FBA_BOX_ID));
                    fBABox.setQuantity(resultSet.getString(SQLConstants.FBA_BOX_QUANTITY));
                    fBABox.setSku(resultSet.getString(SQLConstants.FBA_BOX_SKU));
                    fBABox.setTs(resultSet.getString(SQLConstants.FBA_BOX_TS));
                    fBABox.setWeight_specification(resultSet.getString(SQLConstants.FBA_BOX_WIGHT_SPECIFICATION));
                    faFBABoxs.add(fBABox);
                }
            }

        } catch (SQLException ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while feching Box Information. Error Message : {0}", ex.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LOG.info("[ " + MODULE + " ] " + "Leaving getTotalQtyPerProducts of DBWrapperNew");
        return faFBABoxs;

    }

    public int updateFinalPackingQty(ArrayList<FinalPackingQty> finalPackingQtys, Connection connection) {
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside updateFinalPackingQty of DBWrapperNew.");
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iResult = 0;
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(SQLConstants.UPDATE_FINAL_PACKAGIN_QTY_BY_PACKING_QTY);

            if (finalPackingQtys != null && !finalPackingQtys.isEmpty()) {
                for (FinalPackingQty finalPackingQty : finalPackingQtys) {
                    preparedStatement.setString(1, finalPackingQty.getPakingQty());
                    preparedStatement.setInt(2, finalPackingQty.getId());
                    preparedStatement.addBatch();
                }
            }

            int[] iUpdate = preparedStatement.executeBatch();
            for (int i = 0; i < iUpdate.length; i++) {
                if (iUpdate[i] != 1) {
                    throw new Exception("Failed to update finalPackingQty in Batch");
                }
            }
            connection.commit();

        } catch (Exception ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while feching Final Packing Qty. Error Message : {0}", ex.getMessage());
            try {
                iResult = -1;
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex1) {
                Logger.getLogger(DBWrapperNew.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LOG.info("[ " + MODULE + " ] " + "Leaving updateFinalPackingQty of DBWrapperNew");
        return iResult;

    }

    public int updateTotalQtyPerProducts(Connection connection, ArrayList<Products> productses, ArrayList<Products> new_productses, HttpServletRequest request) {
        LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Inside updateTotalQtyPerProducts of DBWrapperNew.");
        PreparedStatement preparedStatement = null;
 long lNow = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lNow);
        ResultSet resultSet = null;
        int iResult = 0;
      //  Connection connection = null;
        try {
            String user_id = (String) request.getSession(true).getAttribute("user");
         //   connection = DBWrapperNew.getConnection(request, "harrisburgstore");
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(SQLConstants.UPDATE_TOAL_BALANCE);
            PreparedStatement addPreparedStatement = connection.prepareStatement(SQLConstants.ADD_TOAL_BALANCE);
            PreparedStatement manualUpdatedPreparedStatement = connection.prepareStatement(SQLConstants.ADD_MANUAL_UPDATE_TABLE);

            if (productses != null && !productses.isEmpty()) {
                for (Products products : productses) {
                    preparedStatement.setInt(1, Integer.parseInt(products.getTotalUnits()));
                    preparedStatement.setInt(2, products.getId());
                    preparedStatement.addBatch();

                    Products dbProducts = getBlanceUnit(connection, products.getId());
                    manualUpdatedPreparedStatement.setString(1, dbProducts.getUpc());
                    manualUpdatedPreparedStatement.setString(2, dbProducts.getLocation());
                    manualUpdatedPreparedStatement.setInt(3, Integer.parseInt(dbProducts.getTotalUnits()));
                    manualUpdatedPreparedStatement.setInt(4, Integer.parseInt(products.getTotalUnits()));
                    manualUpdatedPreparedStatement.setInt(5, (Integer.parseInt(products.getTotalUnits()) - Integer.parseInt(dbProducts.getTotalUnits())));
                    manualUpdatedPreparedStatement.setString(6, user_id);
                    manualUpdatedPreparedStatement.addBatch();
                }
            }

            if (new_productses != null && !new_productses.isEmpty()) {
                System.out.println("New Products Added : " + new_productses );
                for (Products products : new_productses) {
                    addPreparedStatement.setString(1, products.getUpc());
                    addPreparedStatement.setString(2, products.getLocation());
                    addPreparedStatement.setInt(3, Integer.parseInt(products.getTotalUnits()));
                    addPreparedStatement.addBatch();

                    manualUpdatedPreparedStatement.setString(1, products.getUpc());
                    manualUpdatedPreparedStatement.setString(2, products.getLocation());
                    manualUpdatedPreparedStatement.setInt(3, Integer.parseInt(products.getTotalUnits()));
                    manualUpdatedPreparedStatement.setInt(4, Integer.parseInt(products.getTotalUnits()));
                    manualUpdatedPreparedStatement.setInt(5, 0);
                    manualUpdatedPreparedStatement.setString(6, user_id);
                    manualUpdatedPreparedStatement.addBatch();
                }
            }

            int[] iUpdate = preparedStatement.executeBatch();
            for (int i = 0; i < iUpdate.length; i++) {
                if (iUpdate[i] != 1) {
                    throw new Exception("Failed to update total quantity in Batch");
                }
            }
            iUpdate = addPreparedStatement.executeBatch();
            for (int i = 0; i < iUpdate.length; i++) {
                if (iUpdate[i] != 1) {
                    throw new Exception("Failed to update total quantity in Batch");
                }
            }

            iUpdate = manualUpdatedPreparedStatement.executeBatch();
            for (int i = 0; i < iUpdate.length; i++) {
                if (iUpdate[i] != 1) {
                    throw new Exception("Failed to update total quantity in Batch");
                }
            }
            connection.commit();

        } catch (Exception ex) {
            LOG.log(Level.INFO, "[ " + MODULE + " ] " + "Exception occured while updating Total Qty. Error Message : {0}", ex.getMessage());
            ex.printStackTrace();
            try {
                iResult = -1;
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex1) {
                Logger.getLogger(DBWrapperNew.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        LOG.info("[ " + MODULE + " ] " + "Leaving updateTotalQtyPerProducts of DBWrapperNew");
         long lAfter = System.currentTimeMillis();
        System.out.println("getTotalQtyPerProducts : " + lAfter + ". Time taken : " + (lAfter-lNow));
        return iResult;

    }

    private Products getBlanceUnit(Connection connection, int id) {
        Products products = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from balance_units where id=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                if (resultSet.next()) {
                    products = new Products();
                    products.setId(resultSet.getInt("id"));
                    products.setUpc(resultSet.getString("upc"));
                    products.setLocation(resultSet.getString("loc"));
                    products.setTotalUnits(resultSet.getString("total_units"));

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("[ " + MODULE + " ] " + "Count not found the search Balance Unit");
        }
        return products;
    }
    
    public boolean getDeleteBalanceUnit(Connection connection, String upcs) {
        Statement  statement = null;
    try {
             statement = connection.createStatement();
            int resultSet = statement.executeUpdate("delete from balance_units where upc in ("+ upcs+")");
            HBLoggerNew.debug(MODULE, "resultSet : " + resultSet);
            if (resultSet >=0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("[ " + MODULE + " ] " + "Count not found the search Balance Unit");
        } finally {
            if(statement != null )try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBWrapperNew.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    
        return false;
    }
    private static final Logger LOG = Logger.getLogger(DBWrapperNew.class.getName());
}
