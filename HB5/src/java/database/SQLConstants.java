/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Raj
 */
public class SQLConstants {
    public static final String GET_FINAL_PACKAGIN_QTY_BY_PACKING_QTY = "SELECT  ID, TITLE, ASIN, SKU,PACKING_QTY FROM FINAL_PACKINGQTY WHERE PACKING_QTY = ? ORDER BY ID";
 
    public static final String GET_TOTAL_QTY_PER_PRODUCT     = "SELECT b.ID,b.LOC,p.upc as BARCODE, b.UPC,ifnull(MIN(p.name),'') TITLE,p.name as VENDOR,p.PRICE,p.type as PRODUCT_TYPE,b.TOTAL_UNITS FROM BALANCE_UNITS b left join hb_bc.PRODUCT p on lpad(replace(replace(replace(b.upc,' ',''),'-',''),'UPC:',''),14,'0') = lpad(replace(replace(replace(p.upc,' ',''),'-',''),'UPC:',''),14,'0')  GROUP BY 1";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_NEW     = "select * from (SELECT b.ID,b.LOC ,p.upc as BARCODE, b.UPC,ifnull(MIN(p.name),'') TITLE,p.name as VENDOR,p.PRICE,p.type as PRODUCT_TYPE,b.TOTAL_UNITS FROM BALANCE_UNITS b left join hb_bc.PRODUCT p on lpad(replace(replace(replace(b.upc,' ',''),'-',''),'UPC:',''),14,'0') = lpad(replace(replace(replace(p.upc,' ',''),'-',''),'UPC:',''),14,'0') GROUP BY 1) as a where a.%s LIKE ?";
    public static final String GET_SEARCH_RESULT_FOR_PRODUCT = "SELECT b.ID,b.LOC,p.BARCODE, b.UPC,MIN(p.TITLE) TITLE,p.VENDOR,p.PRICE,p.PRODUCT_TYPE,b.TOTAL_UNITS FROM PRODUCTS p ,BALANCE_UNITS b WHERE tajplaza.cleanupc(p.barcode) = tajplaza.cleanupc(b.upc) AND b.LOC LIKE ? GROUP BY P.BARCODE";
    public static final String UPDATE_FINAL_PACKAGIN_QTY_BY_PACKING_QTY = "UPDATE FINAL_PACKINGQTY SET PACKING_QTY = ? WHERE ID = ?";
    public static final String UPDATE_TOAL_BALANCE = "update balance_units set total_units = ? where id = ?";
    public static final String ADD_TOAL_BALANCE = "INSERT INTO balance_units (upc,loc, total_units) VALUES (?, ?, ?)";
    public static final String ADD_MANUAL_UPDATE_TABLE = "INSERT INTO manual_updated_units (upc,loc, old_units, new_units, units_changed, user_id,date_changed) VALUES (?, ?, ?, ?, ?, ?, now())";
    public static final String GET_FINAL_PACKAGIN_QTY_ID = "ID";
    public static final String GET_FINAL_PACKAGIN_QTY_TITLE = "TITLE";
    public static final String GET_FINAL_PACKAGIN_QTY_ASIN = "ASIN";
    public static final String GET_FINAL_PACKAGIN_QTY_SKU = "SKU";
    public static final String GET_FINAL_PACKAGIN_QTY_PAC_QTY = "PACKING_QTY";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_LOCATION = "LOC";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_UPC = "UPC";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_TITLE = "TITLE";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_TOTAL_UNITS = "TOTAL_UNITS";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_TOTAL_BARCODE = "BARCODE";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_ID = "ID";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_PRICE = "PRICE";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_PRODUCT_TYPE = "PRODUCT_TYPE";
    public static final String GET_TOTAL_QTY_PER_PRODUCT_VENDOR = "VENDOR";
    
    public static final String  FETECH_BOX_DETAIL = "SELECT fnsku, sku, box, quantity, id, ts, date_sent_on_pallet, weight_specification FROM fba_box f where upper(box) in (?)";
    public static final String  FBA_BOX_BOX = "BOX";
    public static final String  FBA_BOX_DATE_SENT_ON_PALLET = "DATE_SENT_ON_PALLET";
    public static final String  FBA_BOX_FNSKU = "FNSKU";
    public static final String  FBA_BOX_SKU = "SKU";
    public static final String  FBA_BOX_QUANTITY = "QUANTITY";
    public static final String  FBA_BOX_ID = "ID";
    public static final String  FBA_BOX_TS = "TS";
    public static final String  FBA_BOX_WIGHT_SPECIFICATION = "WEIGHT_SPECIFICATION";
    public static final String  UPDATE_ARCHIVED_FBA_BOX = "UPDATE archived_fba_box set archived_timestamp=? where box=UPPER(?)";
    public static final String  GET_FBA_BOX_DETAIL = "SELECT fnsku, sku, box, quantity, id, ts, date_sent_on_pallet, weight_specification FROM fba_box f where box=UPPER(?)";
    public static final String  INSERT_ARCHIEVED_BOX = "INSERT INTO archived_fba_box (fnsku, sku, box, quantity, id, ts, date_sent_on_pallet, weight_specification,archived_timestamp) values (?,?,?,?,?,?,?,?,?)";
    public static final String  DELETE_FBA_BOX = "DELETE FROM fba_box where upper(box)=UPPER(?)";
    
    public static final String  FBA_SUMMARY = "select 'Boxes sent to FBA' as title ,\n" +
"fba.id as boxid,\n" +
"fba.ts as createddate, \n" +
"count(fba.quantity) as quantity \n" +
"from fba_box fba,\n" +
"existing_inventory_fba ifba\n" +
"where fba.date_sent_on_pallet is not null\n" +
"and fba.sku=ifba.sku and %s = '%s'\n" +
"union\n" +
"select 'Boxes not yet sent to FBA' as title ,\n" +
"fba.id as boxid,\n" +
"fba.ts as createddate, \n" +
"count(fba.quantity) as quantity \n" +
"from fba_box fba,\n" +
"existing_inventory_fba ifba\n" +
"where fba.date_sent_on_pallet is null\n" +
"and fba.sku=ifba.sku and %s = '%s'";
    public static final String FBA_SUMMARY_TITLE="title";    
    public static final String FBA_SUMMARY_BOXID="boxid";
    public static final String FBA_SUMMARY_CD="createddate";
    public static final String FBA_SUMMARY_QTY="quantity";
    public static final String QRY_PO_BY_STATUS = "SELECT * FROM PURCHAGE_ORDER WHERE STATUS = ?";
    
}


