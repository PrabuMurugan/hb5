/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import queries.DAO;

/**
 *
 * @author us083274
 */
public class GetProducts extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {
            util.DBNew.CONNECT_TO_PROD_DEBUG=true;
            con= DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
            String action=request.getParameter("ACTION");
            String currency=request.getParameter("currency");
            String sr=request.getParameter("s");
            
            
            if(action==null || action.equals("null")||action.length()==0){
           //String sql="SELECT title,id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end,vendor,product_type,'','New',sku,'',replace(replace(replace(barcode,'UPC:',''),'-',''),'\\'',''), concat('http://harrisburgstore.com/products/',handle),concat(grams,' g'),vendor,'','','','','', price,id2,'','','',''    FROM harrisburgstore.products ";
              
            //String sql="SELECT  distinct SUBSTRING(p.name,1,69),p.sku,concat('http://www.harrisburgstore.com/product_images/',image_file),replace(replace(replace(p.description,'\\t',''),'\\r',''),'\\n',''),price, 'In Stock', brand_id,'Food, Beverages & Tobacco','Food, Beverages & Tobacco' ,'New',sku,case  when p.upc='NA' or  length(p.upc)<10 or  p.upc like '%X00%' or  p.upc like '%000000%' then 'FALSE' else '' end, case  when length(p.upc)>10  and  p.upc  not like '%X00%'  and p.upc  not like '%000000%' then  tajplaza.cleanupc(p.upc) else '' end, concat('http://www.harrisburgstore.com',custom_url), concat(weight,' lb'),brand_id,'','','','','', price,'id2','','','NA0' custom_label_0   ,  'NA1'  custom_label_1  , 'NA3'   custom_label_2, 'NA3' custom_label_3,  'NA4'   custom_label_4,'NA5' custom_label_5  FROM hb_bc.product p left  join hb_bc.product_images  pi on p.id=pi.product_id  and pi.sort_order=0 where  inventory_level > 0  "; 
                DAO d=new DAO();
                String sql=d.getQuery("1");
               if(sr!=null&&sr.equals("p")){
                   sql=sql.replaceAll("gts_product_feed", "gts_product_feed_pricefalls");
               }
//            String sql="SELECT  SUBSTRING(p.title,1,69),p.id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end, vendor,product_type,case  when product_type='Grocery & Gourmet Food'  or product_type='Indian Gourmet Spices' then  'Food, Beverages & Tobacco' when product_type='Health & Personal Care' or product_type='Kitchen & Dining' or product_type='Beauty' then  'Health & Beauty'  else '' end ,'New',sku,case  when p.barcode='NA' then 'FALSE' else '' end, case  when length(p.barcode)>10 then  tajplaza.cleanupc(p.barcode) else '' end, concat('http://harrisburgstore.com/products/',handle), concat(grams,' g'),vendor,'','','','','', price,id2,'','',case when option2='NA' or option2='null' or cast(option2 as unsigned) >5000  then '' when cast(option2 as unsigned) <1000 and (low_price_other_than_us is null or price<=low_price_other_than_us) then 'best seller'  when cast(option2 as unsigned) <1000 and ( price>low_price_on_google) then 'best seller not top'  when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  (low_price_other_than_us is null or price<=low_price_other_than_us) then  'good seller' when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  ( price>low_price_other_than_us)  then 'good selelr not top' else ''  end adword_label, case when product_type='Indian Gourmet Spices' then 'Indian Gourmet Spices' else 'Non Indian' end adwords_grouping FROM harrisburgstore.products p left join harrisburgstore.google_shopping_product  g on   tajplaza.cleanupc(g.barcode) =  tajplaza.cleanupc(p.barcode) and google_shopping_page not like '%aclk%'       where  inventory_quantity > 0   ";
           PreparedStatement prest = con.prepareStatement(sql);              
             ResultSet rs= prest.executeQuery();
             StringBuffer s=new StringBuffer();
             out.println("Title\tid\tImage Link\tDescription\tPrice\tAvailability\tManufacturer\tProduct Type\tgoogle product category\tCondition\tMPN\tGTIN\tLink\tShipping Weight\tBrand\tcolor\tsize\tpattern\tage group\tgender\titem group id\tadditional image link\tcustom_label_0\tcustom_label_1\tcustom_label_2\tcustom_label_3\tcustom_label_4\tshipping\tmultipack\tTax\tshipping_label\tShipping Dimensions");
              
             while  (rs.next()){
                 //if(sr!=null  && sr.equals("p") && rs.getString(2).indexOf("DT_")>=0) {
                     
                   //  continue;
                 //}
                     
                  s=new StringBuffer();
             //    System.out.println("title: "+rs.getString(1));
                 s.append(rs.getString(1)+"\t");
                s.append(rs.getString(2)+"\t");
                s.append(rs.getString(3)+"\t");
                String desc=rs.getString(4);
                desc=desc.replaceAll("<p>","").replaceAll("</p>","").replaceAll("<ul>", "").replaceAll("</ul>", "").replaceAll("<li>","").replaceAll("</li>","").replaceAll("<b>", "").replaceAll("</b>","").replaceAll("<br/>","").replaceAll("<strong>", "").replaceAll("</strong>", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
                s.append(desc+"\t");
              if(currency!=null && currency.equals("pounds"))
                  s.append(Float.valueOf(rs.getString(5))*.59+"\t");
              else if(currency!=null && currency.equals("cad"))
                  s.append(Float.valueOf(rs.getString(5))*1.32+"\t");
              else{
                  if(sr!=null&&sr.equals("p")){
                      if(rs.getString(2).indexOf("DT_")>=0){
                          s.append((float)(Math.round((rs.getFloat(5)+6)*1.15*100))/100+"\t");
                      }else{
                           s.append((float)(Math.round(rs.getFloat(5)*1.15*100))/100+"\t");
                      }
                          
                  }
                      
                  else
                    s.append(rs.getString(5)+"\t");
              } 
                  
                s.append(rs.getString(6)+"\t");
                String brand=rs.getString(7);
                s.append(rs.getString(7)+"\t");
                s.append(rs.getString(8)+"\t");
                s.append(rs.getString(9)+"\t");
                s.append(rs.getString(10)+"\t");
                s.append(rs.getString(11)+"\t");
                s.append(rs.getString(12)+"\t");
                if(sr!=null&&sr.equals("p"))
                    s.append(""+"\t");    
                else
                    s.append(rs.getString(13)+"\t");
                s.append(rs.getString(14)+"\t");
                s.append(rs.getString(15)+"\t");
                s.append(rs.getString(16)+"\t");
                s.append(rs.getString(17)+"\t");
                s.append(rs.getString(18)+"\t");
                s.append(rs.getString(19)+"\t");
                s.append(rs.getString(20)+"\t");
                s.append(rs.getString(21)+"\t");
                s.append(rs.getString(22)+"\t");
                s.append(rs.getString(23)+"\t");
                s.append(rs.getString(24)+"\t");
                s.append(rs.getString(25)+"\t");
                s.append(rs.getString(26)+"\t");
                s.append(rs.getString(27)+"\t");
                s.append(rs.getString(28)+"\t");
                s.append(rs.getString(29)+"\t");                
                s.append(rs.getString(30)+"\t" );   
                s.append(rs.getString(31)+"\t" );  
                s.append("5 x 5 x 5" );   
               // s.append(rs.getString(31) );   
             //   s.append(rs.getString(32));  
                
               // s.append(rs.getString(32)+"\t");  
                 
                /*if(brand!=null && brand.equals("PB2"))
                     s.append("US:::0.00 USD");   
                else
                 s.append("");   */
                

                // s.append(rs.getString(26));

                out.println(s.toString());
                out.flush();
            }
          }
           else if(action.equals("SHIPMENT_FEED")){
               System.out.println("Inside SHIPMENT_FEED ");
           //String sql="SELECT title,id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end,vendor,product_type,'','New',sku,'',replace(replace(replace(barcode,'UPC:',''),'-',''),'\\'',''), concat('http://harrisburgstore.com/products/',handle),concat(grams,' g'),vendor,'','','','','', price,id2,'','','',''    FROM harrisburgstore.products ";
            String sql=" select order_id, tracking_number,carrier_code,other_carrier,ship_date from gts_shipment_feed where sent_to_google='N'"; 
//            String sql="SELECT  SUBSTRING(p.title,1,69),p.id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end, vendor,product_type,case  when product_type='Grocery & Gourmet Food'  or product_type='Indian Gourmet Spices' then  'Food, Beverages & Tobacco' when product_type='Health & Personal Care' or product_type='Kitchen & Dining' or product_type='Beauty' then  'Health & Beauty'  else '' end ,'New',sku,case  when p.barcode='NA' then 'FALSE' else '' end, case  when length(p.barcode)>10 then  tajplaza.cleanupc(p.barcode) else '' end, concat('http://harrisburgstore.com/products/',handle), concat(grams,' g'),vendor,'','','','','', price,id2,'','',case when option2='NA' or option2='null' or cast(option2 as unsigned) >5000  then '' when cast(option2 as unsigned) <1000 and (low_price_other_than_us is null or price<=low_price_other_than_us) then 'best seller'  when cast(option2 as unsigned) <1000 and ( price>low_price_on_google) then 'best seller not top'  when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  (low_price_other_than_us is null or price<=low_price_other_than_us) then  'good seller' when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  ( price>low_price_other_than_us)  then 'good selelr not top' else ''  end adword_label, case when product_type='Indian Gourmet Spices' then 'Indian Gourmet Spices' else 'Non Indian' end adwords_grouping FROM harrisburgstore.products p left join harrisburgstore.google_shopping_product  g on   tajplaza.cleanupc(g.barcode) =  tajplaza.cleanupc(p.barcode) and google_shopping_page not like '%aclk%'       where  inventory_quantity > 0   ";
           PreparedStatement prest = con.prepareStatement(sql);              
             ResultSet rs= prest.executeQuery();
             StringBuffer s=new StringBuffer();
             out.println("merchant order id\ttracking number\tcarrier code\tother carrier name\tship date");
              
             while  (rs.next()){
                  s=new StringBuffer();
                 System.out.println("Inside SHIPMENT_FEED, TRACE 1: order_id: "+rs.getString(1));
                 s.append(rs.getString(1)+"\t");
                s.append(rs.getString(2)+"\t");
                s.append(rs.getString(3)+"\t");
                s.append(rs.getString(4)+"\t");
                s.append(rs.getString(5));
                out.println(s.toString());
            }
            // prest=con.prepareStatement("update gts_shipment_feed set sent_to_google='Y' ");
            // prest.executeUpdate();
             
             }//end of shipment feed 
     else if(action.equals("CANCEL_FEED")){
           //String sql="SELECT title,id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end,vendor,product_type,'','New',sku,'',replace(replace(replace(barcode,'UPC:',''),'-',''),'\\'',''), concat('http://harrisburgstore.com/products/',handle),concat(grams,' g'),vendor,'','','','','', price,id2,'','','',''    FROM harrisburgstore.products ";
            String sql=" select order_id, reason from gts_cancel_feed where sent_to_google='N'"; 
//            String sql="SELECT  SUBSTRING(p.title,1,69),p.id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end, vendor,product_type,case  when product_type='Grocery & Gourmet Food'  or product_type='Indian Gourmet Spices' then  'Food, Beverages & Tobacco' when product_type='Health & Personal Care' or product_type='Kitchen & Dining' or product_type='Beauty' then  'Health & Beauty'  else '' end ,'New',sku,case  when p.barcode='NA' then 'FALSE' else '' end, case  when length(p.barcode)>10 then  tajplaza.cleanupc(p.barcode) else '' end, concat('http://harrisburgstore.com/products/',handle), concat(grams,' g'),vendor,'','','','','', price,id2,'','',case when option2='NA' or option2='null' or cast(option2 as unsigned) >5000  then '' when cast(option2 as unsigned) <1000 and (low_price_other_than_us is null or price<=low_price_other_than_us) then 'best seller'  when cast(option2 as unsigned) <1000 and ( price>low_price_on_google) then 'best seller not top'  when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  (low_price_other_than_us is null or price<=low_price_other_than_us) then  'good seller' when  cast(option2 as unsigned) >=1000 and  cast(option2 as unsigned) <5000 and  ( price>low_price_other_than_us)  then 'good selelr not top' else ''  end adword_label, case when product_type='Indian Gourmet Spices' then 'Indian Gourmet Spices' else 'Non Indian' end adwords_grouping FROM harrisburgstore.products p left join harrisburgstore.google_shopping_product  g on   tajplaza.cleanupc(g.barcode) =  tajplaza.cleanupc(p.barcode) and google_shopping_page not like '%aclk%'       where  inventory_quantity > 0   ";
           PreparedStatement prest = con.prepareStatement(sql);              
             ResultSet rs= prest.executeQuery();
             StringBuffer s=new StringBuffer();
             out.println("merchant order id\treason");
              
             while  (rs.next()){
                  s=new StringBuffer();
                 System.out.println("order_id: "+rs.getString(1));
                 s.append(rs.getString(1)+"\t");
                s.append(rs.getString(2));
                out.println(s.toString());
            }
           //  prest=con.prepareStatement("update gts_cancel_feed set sent_to_google='Y' ");
           //  prest.executeUpdate();
             
             }//end of cancel feed 
            /* TODO output your page here. You may use following sample code. */
            con.close();;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
     
}