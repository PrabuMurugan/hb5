/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Raj
 */
public class StartupListener  implements ServletContextListener {
    @Override
    public void contextDestroyed(ServletContextEvent arg0)
    {
        // Cleanup code goes here
    }
 
    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        DOMConfigurator.configure("/home/ec2-user/Dropbox/log4j.xml");
        
    }
}
