/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
/**
 *
 * @author Raj
 */
public class HBLoggerNew {
    private static Logger logger = null;
    static {
        DOMConfigurator.configure("/home/ec2-user/Dropbox/log4j.xml");
        logger = Logger.getRootLogger();
        
    }
    public static void debug(String module,String msg) {
        logger.debug("[ " + module + " ] " + msg);
        System.out.println("[ PRABU 1 : " + module + " ] " + msg);
    }
    public static void error(String module,String msg) {
        logger.error("[ " + module + " ] " + msg);
           System.out.println("[  PRABU  2 :    " + module + " ] " + msg);
    }
}
