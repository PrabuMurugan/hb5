/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author debasish
 */
public class ElFunctionUtil {
    public static String formatDate(Date date){
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        return df.format(date);
    }
    
    public static String formatDouble(Double value){
        NumberFormat nf = new DecimalFormat("#.##");
        
        if(value==null){
            return null;
        }
        return nf.format(value);
    }
    
}
