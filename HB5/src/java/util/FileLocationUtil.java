/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;

/**
 *
 * @author debasish
 */
public class FileLocationUtil {
    private static String OS = System.getProperty("os.name").toLowerCase();
    public static File getFile(String relativePath){
        File target = new File(getBaseLocation(), relativePath);
        
        return target;
    }
    
    public static String getBaseLocation(){
        if(OS.indexOf("nux")>=0){
            return "/home/debasish/JSP files";
        }else{
            return "C:\\Google Drive\\Dropbox\\";
        }
    }
}
