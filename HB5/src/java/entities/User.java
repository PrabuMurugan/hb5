/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Abani
 */
public class User {
    private String userName;
    private int userID;
    private String firstName;
    private String lastName;

    public User(String userName, int userID, String firstName, String lastName) {
        this.userName = userName;
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User() {
    }
    
    

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    
}
