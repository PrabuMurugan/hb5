/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Raj
 */
public class Products {
    private Integer id;
    private String productType;
    private String title;
    private String vendor;
    private String barcode;
    private String price;
    private String location;
    private String upc;
    private String totalUnits;

    public Integer getId() {
        return id;
    }

    public String getProductType() {
        return productType;
    }

    public String getTitle() {
        return title;
    }

    public String getVendor() {
        return vendor;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getPrice() {
        return price;
    }

    public String getLocation() {
        return location;
    }

    public String getUpc() {
        return upc;
    }

    public String getTotalUnits() {
        return totalUnits;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public void setTotalUnits(String totalUnits) {
        this.totalUnits = totalUnits;
    }

    @Override
    public String toString() {
        return "Products{" + "id=" + id + ", productType=" + productType + ", title=" + title + ", vendor=" + vendor + ", barcode=" + barcode + ", price=" + price + ", location=" + location + ", upc=" + upc + ", totalUnits=" + totalUnits + '}';
    }
    
}
