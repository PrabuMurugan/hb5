package entities;

/**
 *
 * @author Raj
 */
public class FinalPackingQty {
    private Integer id;
    private String title;
    private String asin;
    private String sku;
    private String pakingQty;

    public FinalPackingQty(Integer id, String title, String asin, String pakingQty) {
        this.id = id;
        this.title = title;
        this.asin = asin;
        this.pakingQty = pakingQty;
    }

    public FinalPackingQty() {
        
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAsin() {
        return asin;
    }
    public void setAsin(String asin) {
        this.asin = asin;
    }

    public String getPakingQty() {
        return pakingQty;
    }
    public void setPakingQty(String pakingQty) {
        this.pakingQty = pakingQty;
    }
    
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Override
    public String toString() {
        return "FinalPackingQty{" + "id=" + id + ", title=" + title + ", asin=" + asin + ", sku=" + sku + ", pakingQty=" + pakingQty + '}';
    }
    
}
