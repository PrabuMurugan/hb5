package entities;

import java.util.Date;


public class FBABox {
    private String fnsku;
    private String sku;
    private String box;
    private String quantity;
    private String id;
    private String ts;
    private Date date_sent_on_pallet;
    private String weight_specification;

    public String getFnsku() {
        return fnsku;
    }

    public void setFnsku(String fnsku) {
        this.fnsku = fnsku;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Date getDate_sent_on_pallet() {
        return date_sent_on_pallet;
    }

    public void setDate_sent_on_pallet(Date date_sent_on_pallet) {
        this.date_sent_on_pallet = date_sent_on_pallet;
    }

    public String getWeight_specification() {
        return weight_specification;
    }

    public void setWeight_specification(String weight_specification) {
        this.weight_specification = weight_specification;
    }

    @Override
    public String toString() {
        return "FBABox{" + "fnsku=" + fnsku + ", sku=" + sku + ", box=" + box + ", quantity=" + quantity + ", id=" + id + ", ts=" + ts + ", date_sent_on_pallet=" + date_sent_on_pallet + ", weight_specification=" + weight_specification + '}';
    }
    
}
