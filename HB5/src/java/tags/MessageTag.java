/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tags;

import fba.WebMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author debasish
 */
public class MessageTag extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        super.doTag(); //To change body of generated methods, choose Tools | Templates.
        List<WebMessage> messages = (List<WebMessage>) this.getJspContext().findAttribute("messages");
        PrintWriter out = new PrintWriter(this.getJspContext().getOut());
        if (messages != null) {
            for (WebMessage message : messages) {
                out.println("<div class='" + message.getMessageType().toString().toLowerCase() + "' >" + message.getDescription() + "</div>");

            }
        }
        out.flush();

    }
}
