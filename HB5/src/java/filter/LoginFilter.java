/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author abanipatra
 */
public class LoginFilter implements Filter {

    private static final boolean debug = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public LoginFilter() {
    }

    private void doBeforeProcessing(ServletRequest req, ServletResponse res)
            throws IOException, ServletException {
        if (debug) {
            log("Login:DoBeforeProcessing");
        }
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String accessed_url = request.getRequestURL().toString();
        String agent = request.getHeader("User-Agent").toLowerCase();
        System.out.println("User agent:"+ agent);
        if (request.getQueryString() != null) {
            accessed_url += "?" + request.getQueryString();
        }
        System.out.println("accessed_url:" + accessed_url);
        if ( accessed_url.indexOf("OrderManager") > -1 || accessed_url.indexOf("Login") > -1 || accessed_url.indexOf(".css")> -1  | accessed_url.indexOf("GetProducts") > -1  | accessed_url.indexOf("/pickup/") > -1) {
            //skip for those.
        } else {
            HttpSession session = request.getSession(true);
            if(agent.indexOf("android") > -1){
                session.setAttribute("user", "prabu");
                session.setAttribute("user_id", 103);
                session.setAttribute("authenticated", true);
            }else{
               
                Cookie[] cookies = request.getCookies();
                String cookieUserName = null;
                String cookieUserID = null;
                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        String name = cookies[i].getName();
                        String value = cookies[i].getValue();
                        if ("cUname".equals(name)) {
                            cookieUserName = value;
                        }
                        if ("cUID".equals(name)) {
                            cookieUserID = value;
                        }
                    }
                }

                if (cookieUserName != null && cookieUserID != null) {
                    System.out.println("user is in cookie:" + cookieUserName + "id:" + cookieUserID + "auth:" + session.getAttribute("authenticated"));
                    if (session.getAttribute("user") != null && (Boolean) session.getAttribute("authenticated")) {
                        Connection con = null;
                        PreparedStatement prest=null;
                        String audi_table = "url_audit";


                        
                    } else {
                        session.setAttribute("user", cookieUserName);
                        session.setAttribute("user_id", Integer.valueOf(cookieUserID));
                        session.setAttribute("authenticated", true);
                    }


                } else {

                    System.out.println("user is in cookie:" + accessed_url);
                    if (accessed_url.indexOf("login.jsp") == -1) {
                        session.setAttribute("requestedURL", accessed_url);
                        if (!(session.getAttribute("user") != null && (Boolean) session.getAttribute("authenticated"))) {
                            response.sendRedirect("/HB5/login.jsp");
                        }
                    }
                }
            }
        }
        // Write c                ode here to process the request and/or response before
        // the rest of the filter chain is invoked.

    }

    private void doAfterProcessing(ServletRequest req, ServletResponse res)
            throws IOException, ServletException {
        if (debug) {
            log("Login:DoAfterProcessing");
        }


        // Write code here to process the request and/or response after
        // the rest of the filter chain is invoked.

        // For example, a logging filter might log the attributes on the
        // request object after the request has been processed. 
	/*
         for (Enumeration en = request.getAttributeNames(); en.hasMoreElements(); ) {
         String name = (String)en.nextElement();
         Object value = request.getAttribute(name);
         log("attribute: " + name + "=" + value.toString());

         }
         */

        // For example, a filter might append something to the response.
	/*
         PrintWriter respOut = new PrintWriter(response.getWriter());
         respOut.println("<P><B>This has been appended by an intrusive filter.</B>");
         */
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        if (debug) {
            log("Login:doFilter()");
        }

        doBeforeProcessing(request, response);

        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (Throwable t) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = t;
            t.printStackTrace();
        }

        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("Login:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("Login()");
        }
        StringBuffer sb = new StringBuffer("Login(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
