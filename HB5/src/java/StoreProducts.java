/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */

public class StoreProducts extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
             {
                 PrintWriter out=null;
                 Connection con=null;
        try {
               
        response.setContentType("text/html;charset=UTF-8");
        
       
        out = response.getWriter();
           con = DBWrapperNew.getConnection(request,DBWrapperNew.HBG);
            String user_id  = "1";
            if(request.getSession().getAttribute("user") != null){
                user_id  =  (String) request.getSession().getAttribute("user");
            }
             StringBuffer s=new StringBuffer();
              String type=request.getParameter("type");
     
            /* TODO output your page here. You may use following sample code. */
           for (int i=1;i<=20;i++){
               String upc=request.getParameter("upc-"+i);
               String loc=request.getParameter("loc-"+i);
               if(loc!=null)
                   loc=loc.toUpperCase();
               String units=request.getParameter("units-"+i);
               

               if(upc!=null && upc.toLowerCase().equals("null")==false&& upc.trim().length()>2){
                    if(type.equals("outgoing_units")){
                        System.out.println("Now lets check if the unit is really in this loc or not");
                         PreparedStatement prest=con.prepareStatement("select  sum(total_units) from   balance_units where upc=? and loc=? ");
                         prest.setString(1, upc);
                         prest.setString(2, loc);
                         ResultSet rs=prest.executeQuery();
                         if(rs.next()){
                           
                             Integer units_on_shelf=rs.getInt(1);
                             Integer unitsI=Integer.valueOf(units);
                               System.out.println("Row exists in shelf,units_on_shelf:"+units_on_shelf+",units to take :"+unitsI);
                             if(units_on_shelf>0 && units_on_shelf < unitsI ){
                                 System.out.println("Shelf contains something but user trying to take more units from shelf than it is there");
                                 insertRow(con, type,upc,loc ,String.valueOf(units_on_shelf), user_id);
                                 unitsI=unitsI-units_on_shelf;
                                 units=String.valueOf(unitsI);
                                 units_on_shelf=0;
                             }
                             if(units_on_shelf==0){
                                 System.out.println("There is no units in shelf now, lets try to find the correct loc");
                                 prest=con.prepareStatement("select loc,sum(total_units) from  balance_units where upc=?  group by upc,loc");
                                 prest.setString(1, upc);
                                 rs=prest.executeQuery();
                                 Integer closestLeventhDistance=100000;
                                 while(rs.next()){
                                     String locT=rs.getString(1);
                                     Integer unitsT=rs.getInt(2);
                                     int closestLeventhDistance_Current=StringUtils.getLevenshteinDistance(loc, locT);
                                     System.out.println("Current loc "+locT +", loc scanned:"+loc+", leven distance:"+closestLeventhDistance_Current);
                                     if(closestLeventhDistance_Current<closestLeventhDistance){
                                         loc=locT;
                                        closestLeventhDistance=closestLeventhDistance_Current;
                                     }
                                 }

                             }

                         }
                         

                    }                   
                   System.out.println("UPC 6   :#"+upc+"#"+"units:"+units+",loc:"+loc);
                  insertRow(con,type,upc,loc ,units, user_id);
               }
 
           }
        out.println("<h1>Stored sucessfully <br/><br/>            <a href=incoming_to_shelf.jsp>  CLICK HERE </a> TO SCAN RECEIVE AND MORE ITEMS IN SHELVES. DO NOT USE BACK BUTTON ON BRWOSER TO GO BACK."
                + "<br/><br/><br/><a href=outgoing.jsp>  CLICK HERE </a> TO SCAN TAKE ITEMS FROM SHELVES.</h1>");
        }
        catch (Exception e){
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :"+e.getMessage()+"</h1>");
        }
        finally {            
           try{ out.close();
            con.close();}catch (Exception e2){}
        }
    }
    void insertRow(Connection con,String type,String upc,String loc ,String units, String user_id) throws Exception{
        System.out.println("schema is   table :"+type);
                     String sql="insert into  "+type+" (upc, loc,total_units, userid) values (?,?,?,?) ";
           if(loc.length()==0)
               loc="0";
           if(units.length()==0)
               units="0";
           
           PreparedStatement prest = con.prepareStatement(sql);     
           prest.setString(1, upc);
           prest.setString(2, loc);
           prest.setString(3, units);
           prest.setString(4, user_id);
           prest.executeUpdate();
           
    }
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
             {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
             {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
   
}
