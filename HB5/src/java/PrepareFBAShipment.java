/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import database.DBWrapperNew;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author prabu
 */
public class PrepareFBAShipment extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws  ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
         Connection con=null;
          try {
              con= DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
           
             StringBuffer s=new StringBuffer();
              String type=request.getParameter("type");
            
           String sql="select sku,product_name   from existing_inventory_fba where fnsku=? ";
          
      
          FileInputStream fstream0 = new FileInputStream("F:\\Google Drive\\Dropbox\\fba\\createfbashipment_template.txt");
          // Get the object of DataInputStream
         DataInputStream  in0 = new DataInputStream(fstream0);
         BufferedReader  br0 = new BufferedReader(new InputStreamReader(in0));
        String st;

        FileWriter fstream_out = new FileWriter("F:\\Google Drive\\Dropbox\\fba\\createfbashipment.txt");
        BufferedWriter file_out = new BufferedWriter(fstream_out);

                     
         while ((st = br0.readLine()) != null){
                    file_out.write(st);
                     file_out.newLine();             
         }
         
            /* TODO output your page here. You may use following sample code. */
           for (int i=1;i<=20;i++){
               String fnsku=request.getParameter("fnsku-"+i);
               String qty=request.getParameter("qty-"+i);
           PreparedStatement prest = con.prepareStatement(sql);              
           prest.setString(1, fnsku);
             ResultSet rs= prest.executeQuery();
             if  (rs.next()){ 
                 String sku=rs.getString("sku");
                 String product_name=rs.getString("product_name");
                 file_out.write(sku+"\t"+qty+"\t"+product_name);
                  file_out.newLine();                   
             }
             prest.close();
               
                
 
           }
           con.close();
           file_out.close();;
        out.println("<h1>Stored FBA Shipment file successfully at F:\\Google Drive\\Dropbox\\fba\\createfbashipment.txt.  <br/> PLEASE MAKE SURE YOU FILL THE SHIPMENT NAME ON FIRST LINE IN THE FILE.<br/>  Please wait for 30 seconds for this file to be udpated on your local computer. <br/> If you already have this file open in Microsoft Excel on your local computer, the file will not be updated. <br/> So close this file and wait for 30 seconds. ");
        }
        catch (Exception e){
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :"+e.getMessage()+"</h1>");
        }
        finally {            
            out.close();
           try{ con.close();
           }catch (Exception e3){}
           }
         
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
