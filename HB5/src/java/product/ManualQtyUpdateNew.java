package product;

import constants.ActionConstants;
import database.DBWrapperNew;
import entities.Products;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.HBLoggerNew;

public class ManualQtyUpdateNew extends HttpServlet {
private final String MODULE = "ManualQtyUpdate";

protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String strForwardPage = "";
        Connection con = null;
        try {
            String strAction = request.getParameter("actionid");
            HBLoggerNew.debug(MODULE, "actionid : " + strAction);
            
            con = DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
            if(strAction == null){
                ArrayList<Products> products =  new ArrayList<Products>();
                HBLoggerNew.debug(MODULE, "Number of products : " + products.size());
                request.setAttribute("Products", products);
                strForwardPage = "/jsp/ManualQtyUpdateNew.jsp";
            }else if(strAction.equalsIgnoreCase(ActionConstants.GET_TOTAL_QTY_PER_PRODUCT)) {
                
                ArrayList<Products> products =  new DBWrapperNew().getTotalQtyPerProductsNew(con,request);
                HBLoggerNew.debug(MODULE, "Number of products : " + products.size());
                request.setAttribute("Products", products);
                strForwardPage = "/jsp/ManualQtyUpdateNew.jsp";
                
            } else if( strAction.equalsIgnoreCase(ActionConstants.UPDATE_FINAL_PACKAGIN_QTY_BY_PACKING_QTY)) {
                HBLoggerNew.debug(MODULE, "Updating final qty");
                ArrayList<Products> productses = new ArrayList<Products>();
                
                int i=0;
                while(request.getParameter("Qty"+i) != null) {
                    String quentity = request.getParameter("Qty"+i);
                    String brcode = request.getParameter("BARCODE"+i);
                    
                    if(!quentity.equalsIgnoreCase("")) {
                        Products products = new Products();
                        products.setId(Integer.parseInt(brcode));
                        products.setTotalUnits(quentity);
                        productses.add(products);
                    }
                    
                    i++;
                }
                ArrayList<Products> new_productses = new ArrayList<Products>();
                int j=1;
                int total_new = Integer.valueOf(request.getParameter("total_new"));
                while( total_new >= j) {
                    String quentity = request.getParameter("new_quantity_"+j);
                   
                    String upc = request.getParameter("new_upc_"+j);
                    String loc = request.getParameter("new_loc_"+j);
                    HBLoggerNew.debug(MODULE, "new balance_units table values:"+quentity +" upc:"+upc+"Loc:"+loc);
                    if( upc != null && loc != null && !quentity.equalsIgnoreCase("") && !loc.equalsIgnoreCase("") && !upc.equalsIgnoreCase("")) {
                        Products products = new Products();
                       // products.setId(Integer.parseInt(brcode));
                        products.setUpc(upc);
                        products.setTotalUnits(quentity);
                        products.setLocation(loc);
                        new_productses.add(products);
                    }
                    
                    j++;
                }
                HBLoggerNew.debug(MODULE, "Test : " + productses);
                 
                int iResult = new DBWrapperNew().updateTotalQtyPerProducts(con,productses, new_productses, request);
                HBLoggerNew.debug(MODULE, "iResult : " +  iResult);
                strForwardPage = "/ManualQtyUpdateNew?actionid=7";
                if(iResult == 0 ) {
                       request.setAttribute("responseMessage", "Total Qty Updated Successfuily");
                } else {
                        request.setAttribute("responseMessage", "Internal Server Error Occured, while processing your request. Please Try After Some Time");
                }
                
                request.setAttribute("responseCode", "" + iResult);
                request.setAttribute("responsePage",  "/HB5");
                HBLoggerNew.debug(MODULE, "Updated.");
            }else if(strAction.equalsIgnoreCase(ActionConstants.SEARCH_PRODUCTS)){
                ArrayList<Products> products =  new DBWrapperNew().getTotalQtyPerProductsNew(con,request);
                if(products != null) {
                    HBLoggerNew.debug(MODULE, "Number of products : " + products.size());
                    request.setAttribute("Products", products);
                    strForwardPage = "/jsp/ManualQtyUpdateNew.jsp";
                    
                }
            } else if(strAction.equalsIgnoreCase(ActionConstants.DELETE_FINAL_PACKAGIN_QTY)) {
                con.setAutoCommit(false);
                HBLoggerNew.debug(MODULE, "strAction : " + strAction+ ". Deleting final qty package");
                String strDeleteUPC = request.getParameter("h_upc_to_delete");
                strDeleteUPC = strDeleteUPC.substring(0, strDeleteUPC.length()-1);
                HBLoggerNew.debug(MODULE, "Upcs to delete : " + strDeleteUPC);
                boolean bResult = new DBWrapperNew().getDeleteBalanceUnit(con,strDeleteUPC);
                HBLoggerNew.debug(MODULE, "bResult : " + bResult);
                if(bResult) {
                    request.setAttribute("responseCode", "" + 0);
                    request.setAttribute("responseMessage", "Balance Unit Deleted");
                    con.commit();
                } else {
                    request.setAttribute("responseCode", "" + -1);
                    request.setAttribute("responseMessage", "Internal Server Error Occured, while processing your request. Please Try After Some Time");
                    con.rollback();
                }
                
                 strForwardPage = "/ManualQtyUpdateNew?actionid=7";
            }
            request.getRequestDispatcher(strForwardPage).forward(request, response);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
