/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Abani
 */
public class StoreUPCExpiryDates extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public String TABLE_NAME="harrisburgstore.upc_expirydates";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        Connection con = null;
        try {

            response.setContentType("text/html;charset=UTF-8");
            System.out.println(" TABLE_NAME is "+TABLE_NAME);
            if(request.getParameter("table_type")!=null && request.getParameter("table_type").equals("all")){
                TABLE_NAME="harrisburgstore.upc_expirydates_all";
            }
            System.out.println(" Now TABLE_NAME is "+TABLE_NAME);
                
            
            out = response.getWriter();
            con = DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
            String user_id = "1";
            if (request.getSession().getAttribute("user") != null) {
                user_id = (String) request.getSession().getAttribute("user");
            }
            StringBuffer s = new StringBuffer();
            String type = request.getParameter("type");

            /* TODO output your page here. You may use following sample code. */
            for (int i = 1; i <= 100; i++) {
                String upc = request.getParameter("upc-" + i);
                String expirtydate = request.getParameter("expirtydate-" + i);


                if (upc != null && upc.toLowerCase().equals("null") == false && upc.trim().length() > 2) {

                    System.out.println("UPC 16   :#" + upc + "#" + "expirtydate:" + expirtydate);
                    insertRow(con, type, upc, expirtydate, user_id);
                }

            }
            out.println("<h1>Stored sucessfully <br/><br/>            <a href=\"ExpiryDates.jsp\">  CLICK HERE </a> TO SCAN RECEIVE AND MORE ITEMS. DO NOT USE BACK BUTTON ON BRWOSER TO GO BACK."
                    + "<br/><br/><br/></h1>");
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
        } finally {
            try {
                out.close();
                con.close();
            } catch (Exception e2) {
            }
        }
    }

    void insertRow(Connection con, String type, String upc, String expirtydate, String user_id) throws Exception {
        String sql = "insert into  "+TABLE_NAME+" (upc, expirydate, user_id) values (?,?,?) ";

        PreparedStatement prest = con.prepareStatement(sql);
        prest.setString(1, upc);
        prest.setString(2, expirtydate);
        prest.setString(3, user_id);
        prest.executeUpdate();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
