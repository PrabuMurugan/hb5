/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Raj
 */
@XmlType(name="entry")
@XmlAccessorType(XmlAccessType.FIELD)
public class Entry {
    
    @XmlElement(name="id",namespace = "http://www.w3.org/2005/Atom")
    private String id;
    
    @XmlElement(name="title",namespace = "http://www.w3.org/2005/Atom")
    private String title;
    
    @XmlElement(name="updated",namespace = "http://www.w3.org/2005/Atom")
    private String updated;
    
    @XmlElement(name="link", type=Link.class,namespace = "http://www.w3.org/2005/Atom")
    private Link links;
    
    @XmlElement(name="category", type=Category.class,namespace = "http://www.w3.org/2005/Atom")
    private Category category;
    
    @XmlElement(name="content", type=Content.class,namespace = "http://www.w3.org/2005/Atom")
    private Content content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Link getLinks() {
        return links;
    }

    public void setLinks(Link links) {
        this.links = links;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Entry{" + "id=" + id + ", title=" + title + ", updated=" + updated + ", links=" + links + ", category=" + category + ", content=" + content + '}';
    }
    
    
}
