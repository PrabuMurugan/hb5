/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Raj
 */
@XmlType(name="link")
@XmlAccessorType(XmlAccessType.FIELD)
public class Link {
    
    @XmlAttribute(name="rel")
    private String rel;
    
    @XmlAttribute(name="title")
    private String title;
    
    @XmlAttribute(name="href")
    private String href;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "Link{" + "rel=" + rel + ", title=" + title + ", href=" + href + '}';
    }
    
    
}
