/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Raj
 */
@XmlType(name="properties", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
@XmlAccessorType(XmlAccessType.FIELD)
public class Properties {
    
    @XmlElement(name="OrderID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String orderid;
    
    @XmlElement(name="SellerID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String sellerid;
    
    @XmlElement(name="StoreID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String storeid;
    
    @XmlElement(name="MarketplaceID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String marketplaceid ;
    
    @XmlElement(name="OrderStatusID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String orderstatusid;
    
    @XmlElement(name="CustomerID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String customerid;
    
    @XmlElement(name="PackageTypeID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String packagetypeid;
    
    @XmlElement(name="OrderDate", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String orderdate;
    
    @XmlElement(name="PayDate", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String paydate;
    
    @XmlElement(name="ShipDate", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipdate;
    
    @XmlElement(name="Quantity", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String quantity;
    
    @XmlElement(name="OrderNumber", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String ordernumber;
    
    @XmlElement(name="ShipName", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipname;
    
    @XmlElement(name="ShipCompany", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipcompany;
    
    @XmlElement(name="ShipStreet1", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipstreet1;
    
    @XmlElement(name="ShipStreet2", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipstreet2;
    
    @XmlElement(name="ShipCity", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipcity;
    
    @XmlElement(name="ShipState", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipstate;
    
    @XmlElement(name="ShipPostalCode", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipspostalcode;
    
    @XmlElement(name="ShipCountryCode", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipcountrycode;
    
    @XmlElement(name="ShipPhone", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipphone;
    
    @XmlElement(name="AddressVerified", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String addressverified;
    
    @XmlElement(name="ShippingAmount", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shppingamount;
    
    @XmlElement(name="OrderTotal", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String ordertotal;
    
    @XmlElement(name="Username", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String username;
    
    @XmlElement(name="NotesFromBuyer", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String notesfrombuyer;
    
    @XmlElement(name="NotesToBuyer", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String notestobuyer;
    
    @XmlElement(name="InternalNotes", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String internalnotes;
    
    @XmlElement(name="ImportKey", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String importkey;
    
    @XmlElement(name="WeightOz", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String weightoz;
    
    @XmlElement(name="Width", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String width;
    
    @XmlElement(name="Length", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String length;
    
    @XmlElement(name="Height", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String height;
    
    @XmlElement(name="CreateDate", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String createdate;
    
    @XmlElement(name="ModifyDate", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String modifieddate;
    
    @XmlElement(name="Active", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String active;
    
    @XmlElement(name="ServiceID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String serviceid;
    
    @XmlElement(name="ShipStreet3", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String shipstreet3;
    
    @XmlElement(name="AmountPaid", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String amountpaid;
    
    @XmlElement(name="BuyerEmail", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String buyeremail;
    
    @XmlElement(name="InsuredValue", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String insuredvalue;
    
    @XmlElement(name="InsuranceProvider", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String insuranceprovider;
    
    @XmlElement(name="InsuranceCost", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String insurancecost;
    
    @XmlElement(name="ProviderID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String providerid;
    
    @XmlElement(name="Confirmation", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String confirmation;
    
    @XmlElement(name="ConfirmationCost", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String confirmationcost;
    
    @XmlElement(name="HoldUntil", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String holduntil;
    
    @XmlElement(name="ImportBatch", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String importbatch;
    
    @XmlElement(name="RequestedShippingService", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String requestedshippingservice;
    
    @XmlElement(name="WarehouseID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String warehouseid;
    
    @XmlElement(name="CustomsContents", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String customscontets;
    
    @XmlElement(name="NonDelivery", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String nondelivery;
    
    @XmlElement(name="ResidentialIndicator", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String residentialindicator;
    
    @XmlElement(name="ExternalUrl", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String externalurl;
    
    @XmlElement(name="AdditionalHandling", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String additionalhandling;
    
    @XmlElement(name="SaturdayDelivery", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String saturdaydelivery;
    
    @XmlElement(name="RateError", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String rateerror;
    
    @XmlElement(name="OtherCost", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String othercost;
    
    @XmlElement(name="ExternalPaymentID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String externalpaymentid;
    
    @XmlElement(name="NonMachinable", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String nonmachinable;
    
    @XmlElement(name="ShowPostage", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String showpostage;
    
    @XmlElement(name="PackingSlipID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String packingshipid;
    
    @XmlElement(name="EmailTemplateID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String emailtemplateid;
    
    @XmlElement(name="RequestedServiceID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String requestedserviceid;
    
    @XmlElement(name="Gift", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String gift;
    
    @XmlElement(name="GiftMessage", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String giftmessage;
    
    @XmlElement(name="ExportStatus", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String exportstatus;
    
    @XmlElement(name="TaxAmount", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String taxamount;
    
    @XmlElement(name="CustomField1", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String customfield1;
    
    @XmlElement(name="CustomField2", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String customfield2;
    
    @XmlElement(name="OrderID", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String customfield3;
    
    @XmlElement(name="AssignedUser", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String assigneduser;
    
    @XmlElement(name="Source", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
    private String source;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getSellerid() {
        return sellerid;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setSellerid(String sellerid) {
        this.sellerid = sellerid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getMarketplaceid() {
        return marketplaceid;
    }

    public void setMarketplaceid(String marketplaceid) {
        this.marketplaceid = marketplaceid;
    }

    public String getOrderstatusid() {
        return orderstatusid;
    }

    public void setOrderstatusid(String orderstatusid) {
        this.orderstatusid = orderstatusid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getPackagetypeid() {
        return packagetypeid;
    }

    public void setPackagetypeid(String packagetypeid) {
        this.packagetypeid = packagetypeid;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getShipdate() {
        return shipdate;
    }

    public void setShipdate(String shipdate) {
        this.shipdate = shipdate;
    }

        public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getShipname() {
        return shipname;
    }

    public void setShipname(String shipname) {
        this.shipname = shipname;
    }

    public String getShipcompany() {
        return shipcompany;
    }

    public void setShipcompany(String shipcompany) {
        this.shipcompany = shipcompany;
    }

    public String getShipstreet1() {
        return shipstreet1;
    }

    public void setShipstreet1(String shipstreet1) {
        this.shipstreet1 = shipstreet1;
    }

    public String getShipstreet2() {
        return shipstreet2;
    }

    public void setShipstreet2(String shipstreet2) {
        this.shipstreet2 = shipstreet2;
    }

    public String getShipcity() {
        return shipcity;
    }

    public void setShipcity(String shipcity) {
        this.shipcity = shipcity;
    }

    public String getShipstate() {
        return shipstate;
    }

    public void setShipstate(String shipstate) {
        this.shipstate = shipstate;
    }

    public String getShipspostalcode() {
        return shipspostalcode;
    }

    public void setShipspostalcode(String shipspostalcode) {
        this.shipspostalcode = shipspostalcode;
    }

    public String getShipcountrycode() {
        return shipcountrycode;
    }

    public void setShipcountrycode(String shipcountrycode) {
        this.shipcountrycode = shipcountrycode;
    }

    public String getShipphone() {
        return shipphone;
    }

    public void setShipphone(String shipphone) {
        this.shipphone = shipphone;
    }

    public String getAddressverified() {
        return addressverified;
    }

    public void setAddressverified(String addressverified) {
        this.addressverified = addressverified;
    }

    public String getShppingamount() {
        return shppingamount;
    }

    public void setShppingamount(String shppingamount) {
        this.shppingamount = shppingamount;
    }

    public String getOrdertotal() {
        return ordertotal;
    }

    public void setOrdertotal(String ordertotal) {
        this.ordertotal = ordertotal;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNotesfrombuyer() {
        return notesfrombuyer;
    }

    public void setNotesfrombuyer(String notesfrombuyer) {
        this.notesfrombuyer = notesfrombuyer;
    }

    public String getNotestobuyer() {
        return notestobuyer;
    }

    public void setNotestobuyer(String notestobuyer) {
        this.notestobuyer = notestobuyer;
    }

    public String getInternalnotes() {
        return internalnotes;
    }

    public void setInternalnotes(String internalnotes) {
        this.internalnotes = internalnotes;
    }

    public String getImportkey() {
        return importkey;
    }

    public void setImportkey(String importkey) {
        this.importkey = importkey;
    }

    public String getWeightoz() {
        return weightoz;
    }

    public void setWeightoz(String weightoz) {
        this.weightoz = weightoz;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getShipstreet3() {
        return shipstreet3;
    }

    public void setShipstreet3(String shipstreet3) {
        this.shipstreet3 = shipstreet3;
    }

    public String getAmountpaid() {
        return amountpaid;
    }

    public void setAmountpaid(String amountpaid) {
        this.amountpaid = amountpaid;
    }

    public String getBuyeremail() {
        return buyeremail;
    }

    public void setBuyeremail(String buyeremail) {
        this.buyeremail = buyeremail;
    }

    public String getInsuredvalue() {
        return insuredvalue;
    }

    public void setInsuredvalue(String insuredvalue) {
        this.insuredvalue = insuredvalue;
    }

    public String getInsuranceprovider() {
        return insuranceprovider;
    }

    public void setInsuranceprovider(String insuranceprovider) {
        this.insuranceprovider = insuranceprovider;
    }

    public String getInsurancecost() {
        return insurancecost;
    }

    public void setInsurancecost(String insurancecost) {
        this.insurancecost = insurancecost;
    }

    public String getProviderid() {
        return providerid;
    }

    public void setProviderid(String providerid) {
        this.providerid = providerid;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public String getConfirmationcost() {
        return confirmationcost;
    }

    public void setConfirmationcost(String confirmationcost) {
        this.confirmationcost = confirmationcost;
    }

    public String getHolduntil() {
        return holduntil;
    }

    public void setHolduntil(String holduntil) {
        this.holduntil = holduntil;
    }

    public String getImportbatch() {
        return importbatch;
    }

    public void setImportbatch(String importbatch) {
        this.importbatch = importbatch;
    }

    public String getRequestedshippingservice() {
        return requestedshippingservice;
    }

    public void setRequestedshippingservice(String requestedshippingservice) {
        this.requestedshippingservice = requestedshippingservice;
    }

    public String getWarehouseid() {
        return warehouseid;
    }

    public void setWarehouseid(String warehouseid) {
        this.warehouseid = warehouseid;
    }

    public String getCustomscontets() {
        return customscontets;
    }

    public void setCustomscontets(String customscontets) {
        this.customscontets = customscontets;
    }

    public String getNondelivery() {
        return nondelivery;
    }

    public void setNondelivery(String nondelivery) {
        this.nondelivery = nondelivery;
    }

    public String getResidentialindicator() {
        return residentialindicator;
    }

    public void setResidentialindicator(String residentialindicator) {
        this.residentialindicator = residentialindicator;
    }

    public String getExternalurl() {
        return externalurl;
    }

    public void setExternalurl(String externalurl) {
        this.externalurl = externalurl;
    }

    public String getAdditionalhandling() {
        return additionalhandling;
    }

    public void setAdditionalhandling(String additionalhandling) {
        this.additionalhandling = additionalhandling;
    }

    public String getSaturdaydelivery() {
        return saturdaydelivery;
    }

    public void setSaturdaydelivery(String saturdaydelivery) {
        this.saturdaydelivery = saturdaydelivery;
    }

    public String getRateerror() {
        return rateerror;
    }

    public void setRateerror(String rateerror) {
        this.rateerror = rateerror;
    }

    public String getOthercost() {
        return othercost;
    }

    public void setOthercost(String othercost) {
        this.othercost = othercost;
    }

    public String getExternalpaymentid() {
        return externalpaymentid;
    }

    public void setExternalpaymentid(String externalpaymentid) {
        this.externalpaymentid = externalpaymentid;
    }

    public String getNonmachinable() {
        return nonmachinable;
    }

    public void setNonmachinable(String nonmachinable) {
        this.nonmachinable = nonmachinable;
    }

    public String getShowpostage() {
        return showpostage;
    }

    public void setShowpostage(String showpostage) {
        this.showpostage = showpostage;
    }

    public String getPackingshipid() {
        return packingshipid;
    }

    public void setPackingshipid(String packingshipid) {
        this.packingshipid = packingshipid;
    }

    public String getEmailtemplateid() {
        return emailtemplateid;
    }

    public void setEmailtemplateid(String emailtemplateid) {
        this.emailtemplateid = emailtemplateid;
    }

    public String getRequestedserviceid() {
        return requestedserviceid;
    }

    public void setRequestedserviceid(String requestedserviceid) {
        this.requestedserviceid = requestedserviceid;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public String getGiftmessage() {
        return giftmessage;
    }

    public void setGiftmessage(String giftmessage) {
        this.giftmessage = giftmessage;
    }

    public String getExportstatus() {
        return exportstatus;
    }

    public void setExportstatus(String exportstatus) {
        this.exportstatus = exportstatus;
    }

    public String getTaxamount() {
        return taxamount;
    }

    public void setTaxamount(String taxamount) {
        this.taxamount = taxamount;
    }

    public String getCustomfield1() {
        return customfield1;
    }

    public void setCustomfield1(String customfield1) {
        this.customfield1 = customfield1;
    }

    public String getCustomfield2() {
        return customfield2;
    }

    public void setCustomfield2(String customfield2) {
        this.customfield2 = customfield2;
    }

    public String getCustomfield3() {
        return customfield3;
    }

    public void setCustomfield3(String customfield3) {
        this.customfield3 = customfield3;
    }

    public String getAssigneduser() {
        return assigneduser;
    }

    public void setAssigneduser(String assigneduser) {
        this.assigneduser = assigneduser;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "Properties{" + "orderid=" + orderid + ", sellerid=" + sellerid + ", storeid=" + storeid + ", marketplaceid=" + marketplaceid + ", orderstatusid=" + orderstatusid + ", customerid=" + customerid + ", packagetypeid=" + packagetypeid + ", orderdate=" + orderdate + ", paydate=" + paydate + ", shipdate=" + shipdate + ", ordernumber=" + ordernumber + ", shipname=" + shipname + ", shipcompany=" + shipcompany + ", shipstreet1=" + shipstreet1 + ", shipstreet2=" + shipstreet2 + ", shipcity=" + shipcity + ", shipstate=" + shipstate + ", shipspostalcode=" + shipspostalcode + ", shipcountrycode=" + shipcountrycode + ", shipphone=" + shipphone + ", addressverified=" + addressverified + ", shppingamount=" + shppingamount + ", ordertotal=" + ordertotal + ", username=" + username + ", notesfrombuyer=" + notesfrombuyer + ", notestobuyer=" + notestobuyer + ", internalnotes=" + internalnotes + ", importkey=" + importkey + ", weightoz=" + weightoz + ", width=" + width + ", length=" + length + ", height=" + height + ", createdate=" + createdate + ", modifieddate=" + modifieddate + ", active=" + active + ", serviceid=" + serviceid + ", shipstreet3=" + shipstreet3 + ", amountpaid=" + amountpaid + ", buyeremail=" + buyeremail + ", insuredvalue=" + insuredvalue + ", insuranceprovider=" + insuranceprovider + ", insurancecost=" + insurancecost + ", providerid=" + providerid + ", confirmation=" + confirmation + ", confirmationcost=" + confirmationcost + ", holduntil=" + holduntil + ", importbatch=" + importbatch + ", requestedshippingservice=" + requestedshippingservice + ", warehouseid=" + warehouseid + ", customscontets=" + customscontets + ", nondelivery=" + nondelivery + ", residentialindicator=" + residentialindicator + ", externalurl=" + externalurl + ", additionalhandling=" + additionalhandling + ", saturdaydelivery=" + saturdaydelivery + ", rateerror=" + rateerror + ", othercost=" + othercost + ", externalpaymentid=" + externalpaymentid + ", nonmachinable=" + nonmachinable + ", showpostage=" + showpostage + ", packingshipid=" + packingshipid + ", emailtemplateid=" + emailtemplateid + ", requestedserviceid=" + requestedserviceid + ", gift=" + gift + ", giftmessage=" + giftmessage + ", exportstatus=" + exportstatus + ", taxamount=" + taxamount + ", customfield1=" + customfield1 + ", customfield2=" + customfield2 + ", customfield3=" + customfield3 + ", assigneduser=" + assigneduser + ", source=" + source + '}';
    }
    
    
}
