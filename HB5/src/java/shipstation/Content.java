/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Raj
 */
@XmlType(name="content")
@XmlAccessorType(XmlAccessType.FIELD)
public class Content {
    
    @XmlAttribute(name="type")
    private String type;
    
    @XmlElement(name="properties", type=Properties.class ,namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata")
    private Properties properties;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Content{" + "type=" + type + ", properties=" + properties + '}';
    }
    
    
}
