/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Raj
 */
@XmlType(name="category")
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {
    
    @XmlAttribute(name="term")
    private String term;
    
    @XmlAttribute(name="scheme")
    private String scheme;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @Override
    public String toString() {
        return "Category{" + "term=" + term + ", scheme=" + scheme + '}';
    }
    
    
    
}
