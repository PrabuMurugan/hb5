/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Raj
 */
@XmlType(name="title")
public class Title {
    
    
    private String  type;
    private String title;
    
    @XmlAttribute(name="type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlValue
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Title{" + "type=" + type + ", title=" + title + '}';
    }

   
}
