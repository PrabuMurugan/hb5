/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raj
 */
@XmlRootElement(name = "feed",namespace = "http://www.w3.org/2005/Atom")
public class Feed {
    

    private Title title;
    
    private String id;
   
    private String updated;
     
    private List<Link> links;  
    
    private List<Entry> entries;

    @XmlElement(name="title", type = Title.class,namespace = "http://www.w3.org/2005/Atom")
    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    @XmlElement(name="id", namespace = "http://www.w3.org/2005/Atom")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name="updated",namespace = "http://www.w3.org/2005/Atom")
    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @XmlElement(name = "link", type=Link.class,namespace = "http://www.w3.org/2005/Atom")
    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    @XmlElement(name = "entry", type=Entry.class,namespace = "http://www.w3.org/2005/Atom")
    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "Feed{" + "title=" + title + ", id=" + id + ", updated=" + updated + ", links=" + links + ", entries=" + entries + '}';
    }
    
}
