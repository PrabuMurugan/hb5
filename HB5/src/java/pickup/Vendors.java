/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pickup;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.HBLoggerNew;

/**
 *
 * @author Abani
 */
public class Vendors extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        System.out.println("request for vendor list");
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.PICKUP);
            String sql = "select  * from pickup_hb.vendors order by vendor";
            PreparedStatement prest = con.prepareStatement(sql);
            ResultSet rs = prest.executeQuery();
            JSONObject resp = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            while (rs.next()) {
                JSONObject JObject = new JSONObject();
                JObject.put("vendor", rs.getString("vendor"));
                JObject.put("product_count", rs.getString("product_count"));
                JObject.put("url", rs.getString("url"));
                jsonArray.add(JObject);
            }
            resp.put("vendors", jsonArray);
            System.out.println("vendors:"+jsonArray);
            out.print("getVendors("+resp +");");
            out.flush();
        } catch (Exception ex) {
            //HBLoggerNew.error("Pickup Vendors", ex.getMessage());
            ex.printStackTrace();
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
                //HBLoggerNew.error("Pickup Vendors", ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
