/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package packing;

import constants.ActionConstants;
import database.DBWrapperNew;
import entities.FinalPackingQty;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.HBLoggerNew;

/**
 *
 * @author Raj
 */
public class FinalPackingServlet extends HttpServlet {

    private final String MODULE = "FinalPackingServlet";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String strForwardPage = "";
        Connection con = null;
        try {
            String strAction = request.getParameter("actionid");
            HBLoggerNew.debug(MODULE, "actionid : " + strAction);
            strAction = strAction == null ? ActionConstants.GET_FIANL_PACKING_QTY : strAction;
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            if (strAction.equalsIgnoreCase(ActionConstants.GET_FIANL_PACKING_QTY)) {
                ArrayList<FinalPackingQty> finalPackingQtys = new DBWrapperNew().getFinalPackingQty("NA",con);
                if (finalPackingQtys != null && !finalPackingQtys.isEmpty()) {
                    HBLoggerNew.debug(MODULE, "Number of finalPackingQtys : " + finalPackingQtys.size());
                    request.setAttribute("finalPackingQtys", finalPackingQtys);
                }
                strForwardPage = "/jsp/FinalPackingQty.jsp";
            } else if (strAction.equalsIgnoreCase(ActionConstants.UPDATE_FINAL_PACKING_QTY)) {
                HBLoggerNew.debug(MODULE, "Updating final qty");
                Map parameterMap = request.getParameterMap();
                ArrayList<FinalPackingQty> finalPackingQtys = new ArrayList<FinalPackingQty>();

                Iterator iterator = parameterMap.keySet().iterator();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (key.contains("Qty-")) {
                        String strValString = request.getParameter(key);

                        if (strValString != null && !strValString.equalsIgnoreCase("")) {
                            HBLoggerNew.debug(MODULE, "key : " + key.substring(4) + "\t Qty1 : " + strValString);
                            FinalPackingQty finalPackingQty = new FinalPackingQty();
                            finalPackingQty.setId(Integer.parseInt(key.substring(4)));
                            finalPackingQty.setPakingQty(strValString);
                            finalPackingQtys.add(finalPackingQty);
                        }

                    }
                }
               
                int iResult = new DBWrapperNew().updateFinalPackingQty(finalPackingQtys, con);
                HBLoggerNew.debug(MODULE, "iResult : " + iResult);
                strForwardPage = "/FinalPackingServlet?actionid=1";
                if (iResult == 0) {
                    request.setAttribute("responseMessage", "Final Qty Updated Successfuily");
                } else {
                    request.setAttribute("responseMessage", "Internal Server Error Occured, while processing your request. Please Try After Some Time");
                }

                request.setAttribute("responseCode", "" + iResult);
                request.setAttribute("responsePage", "/HB5/FinalPackingServlet?actionid=1");
                HBLoggerNew.debug(MODULE, "Updated.");
            } 
            request.getRequestDispatcher(strForwardPage).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
