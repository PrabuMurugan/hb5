/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package packing;
import fba.*;

import database.DBWrapperNew;
import entities.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Abani
 */
public class StoreCustomerPacking extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if("update".equals(action)){
            request.getRequestDispatcher("/packing/update_customerpacking.jsp").forward(request, response);
        }else{
            List<User> users = new ArrayList<User>();
            Connection con = null;

            try {
                con = DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
                String SQL = "select * from harrisburgstore.users order by username";
                PreparedStatement userQuery = con.prepareStatement(SQL);
                ResultSet rs = userQuery.executeQuery();
                while (rs.next()) {
                    User user = new User(rs.getString("username"), rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
                    users.add(user);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            System.out.println("list size:"+ users.size());
            String username = (String) request.getSession().getAttribute("user");
            int user_id = 9999;
            if((Integer) request.getSession().getAttribute("user_id")!=null)
                user_id=(Integer) request.getSession().getAttribute("user_id");
            request.setAttribute("users", users);
            request.setAttribute("username", username);
            request.setAttribute("user_id", user_id);
            request.getRequestDispatcher("/packing/track_customerpacking.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        Connection con = null;
  
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            Date today = new Date();
            SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
            String todayDateString = sf.format(today);
            

            try {
                 con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
                String username = (String) request.getParameter("userName");
                String order_ids = (String) request.getParameter("order_ids");
                String[] order_arr=order_ids.split("\n");
                String user_id = (String) request.getParameter("userID");
                
                System.out.println("order_ids:"+order_ids);
                
                StringBuffer s = new StringBuffer();
                
         
                String insertSQL = "insert into harrisburgstore.customerpacking (order_id, user_id, user_name ) values (?, ?, ?)";


                PreparedStatement pInsertQuery = con.prepareStatement(insertSQL);
                
                for(int i=0;i<order_arr.length;i++){
                    if( order_arr[i].trim().length()<=1)
                        continue;
                    //if(order_arr[i].length()>50)
                      //  order_arr[i]=StringUtils.substring(order_arr[i], 0,49);
                    pInsertQuery.setString(1, order_arr[i].trim());
                    pInsertQuery.setString(2, user_id);
                    pInsertQuery.setString(3, username);
                    pInsertQuery.executeUpdate();
                    pInsertQuery.clearParameters();
                    
                    
                }
      

                pInsertQuery.close();
                con.close();
                out.println("<a href='/HB5'>HOME</a> > <a href='/HB5/inventory_management.jsp'> Inventory Management </a> > <a href='/HB5/StoreCustomerPacking'>Track customer packing</a><h1 style='color:green'> INFO : Inserted in the database successfully </h1>");

            } catch (Exception e) {
                e.printStackTrace();
                out.println("<a href='/HB5'>HOME</a> > <a href='/HB5/inventory_management.jsp'> Inventory Management </a> <a href='/HB5/StoreCustomerPacking'>Track customer packing</a><h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
            } finally {
                out.flush();
                out.close();
                try {
                    con.close();
                } catch (Exception e3) {
                }
            }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
