/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
public class BoxDetailsSave extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        List<WebMessage> messages = new ArrayList<WebMessage>();
        Connection con = null;
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            // con.setAutoCommit(false);
            PreparedStatement updateStmt = con.prepareStatement("update fba_box set quantity=?, weight_specification=? where box=? and fnsku=?");
            PreparedStatement deleteStatement = con.prepareStatement("delete from fba_box where box=? and fnsku=?");

            for (int i = 0;; i++) {
                String fnsku = request.getParameter("fnsku-" + i);
                if (fnsku == null) {
                    break;
                }
                String box = request.getParameter("box");
                String quantity = request.getParameter("quantity-" + i);
                String weight = request.getParameter("weight-" + i);

                if (Integer.parseInt(quantity) == 0) {
                    deleteStatement.setString(1, box);
                    deleteStatement.setString(2, fnsku);
                    deleteStatement.executeUpdate();
                } else {
                    updateStmt.setString(1, quantity);
                    updateStmt.setString(2, weight);
                    updateStmt.setString(3, box);
                    updateStmt.setString(4, fnsku);
                    updateStmt.executeUpdate();
                }


            }
            //BoxDetailsServlet
            //con.commit();
            WebMessage successMessage = new WebMessage(WebMessage.MessageType.INFO, "Save Successful");
            messages.add(successMessage);
        } catch (Exception ex) {

            Logger.getLogger(BoxDetailsSave.class.getName()).log(Level.SEVERE, null, ex);
            WebMessage errorMessage = new WebMessage(WebMessage.MessageType.FATAL, ex.getMessage());
            messages.add(errorMessage);

        } finally {
            try {
                con.close();
            } catch (Exception ex) {
            }
        }
        request.setAttribute("messages", messages);
        request.getRequestDispatcher("/BoxDetailsServlet").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
