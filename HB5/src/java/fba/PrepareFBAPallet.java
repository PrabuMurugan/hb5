/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
public class PrepareFBAPallet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String [] boxIDs = request.getParameterValues("box");
            List<ItemDetails> items = ProductBoxSearchUtil.getItems(request, boxIDs);
            HashMap<String, Double> boxWeights = new HashMap<String, Double>();
            double palletWeight = 0;
            for(ItemDetails item:items){
                Double boxWeight = boxWeights.get(item.getBox());
                palletWeight+=item.getQuantity()*item.getWeight();
                if(boxWeight==null){
                    boxWeight=0d;
                    
                }
                boxWeights.put(item.getBox(), boxWeight+item.getWeight()*item.getQuantity());
            }
            
            DecimalFormat df = new DecimalFormat("#.##");
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Detail of Boxes </title>");
            out.println("<style>\n" +
            "    body,input,select{\n" +
            "        font-size:150%;\n" +
            "    }\n" +
            "/* Apply padding to td elements that are direct children of the tr element. */\n" +
            "tr.row > td\n" +
            "{\n" +
            "  padding-bottom: 2em;\n" +
            "}\n" +
            " </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<label for='pallet_weight'>Total Pallet Weight</label>");
            out.println("<input name='pallet_weight' value='"+df.format(palletWeight)+"' readonly='true'></input>");
            out.println("<table border='1'>");
            out.println("<tr>");
            out.println("<td>");
            out.println("Box");
            out.println("</td>");
            out.println("<td>");
            out.println("Box Weight");
            out.println("</td>");
            out.println("<td>");
            out.println("FNSKU");
            out.println("</td>");
            out.println("<td>");
            out.println("Quantity");
            out.println("</td>");
            out.println("<td>");
            out.println("Product Name");
            out.println("</td>");
            out.println("<td>");
            out.println("Item Total Weight");
            out.println("</td>");
            
            out.println("</tr>");
            
            
            
            String lastBox = null;
            
            for(ItemDetails item:items){
                out.println("<tr>");
                if(!item.getBox().equals(lastBox)){
                    
                    out.println("<td>");
                    out.println(item.getBox());
                    out.println("</td>");
                    out.println("<td>");
                    out.println(df.format(boxWeights.get(item.getBox())));
                    out.println("</td>");
                }else{
                    
                    out.println("<td> ");
                    
                    out.println("</td>");
                    out.println("<td> ");
                    
                    out.println("</td>");
                    
                }
                lastBox=item.getBox();
                
                out.println("<td>");
                out.println(item.getFnsku());
                out.println("</td>");
                
                out.println("<td>");
                out.println(item.getQuantity());
                out.println("</td>");
                
                out.println("<td>");
                out.println(item.getName());
                out.println("</td>");
                
                out.println("<td>");
                out.println(df.format(item.getWeight()*item.getQuantity()));
                out.println("</td>");
                
                
                out.println("</tr>");
            }
            out.println("</table>");
            
            out.println("<form action=\"GenerateFile\">");
            out.println("<label for=\"MerchantShipmentName\">Enter Merchant Shipment Name</label>");
            out.println("<input type=\"text\" name=\"MerchantShipmentName\" value=\"\"/>");
            out.println("<br/>");
            out.println("<input type=\"submit\" value=\"Save File\"/>");
            out.println("</form>");
            

            out.println("</body>");
            out.println("</html>");
            request.getSession().setAttribute("ItemDetails", items);
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
        } finally {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
