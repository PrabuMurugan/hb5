/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import entities.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abani
 */
public class StoreTrackFBALabelling extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if("update".equals(action)){
            request.getRequestDispatcher("/to_fba/update_fba_labelling.jsp").forward(request, response);
        }else{
            List<User> users = new ArrayList<User>();
            Connection con = null;

            try {
                con = DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
                String SQL = "select * from harrisburgstore.users order by username";
                PreparedStatement userQuery = con.prepareStatement(SQL);
                ResultSet rs = userQuery.executeQuery();
                while (rs.next()) {
                    User user = new User(rs.getString("username"), rs.getInt("id"), rs.getString("firstname"), rs.getString("lastname"));
                    users.add(user);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            System.out.println("list size:"+ users.size());
            String username = (String) request.getSession().getAttribute("user");
            int user_id = 9999;
            if((Integer) request.getSession().getAttribute("user_id")!=null)
                user_id=(Integer) request.getSession().getAttribute("user_id");
            request.setAttribute("users", users);
            request.setAttribute("username", username);
            request.setAttribute("user_id", user_id);
            request.getRequestDispatcher("/to_fba/track_fba_labelling.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        Connection con = null;
 
       
        if("search".equals(action)){
            try{

            RequestDispatcher rd = request.getRequestDispatcher("to_fba/update_fba_labelling.jsp");
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
             String searchSQL = "select * from  harrisburgstore.fba_labelled_items where fnsku=? order by id desc";
             PreparedStatement pSearchQuery = con.prepareStatement(searchSQL);
             pSearchQuery.setString(1, request.getParameter("fnsku") );
             ResultSet rs = pSearchQuery.executeQuery();
             List<FBADetail> fBADetails = new ArrayList<FBADetail>();
             while(rs.next()){
                 FBADetail fBADetail = new FBADetail(rs.getLong("id"), rs.getString("fnsku"), rs.getLong("qty"), rs.getDate("add_date"), rs.getString("user_name"), rs.getString("item_name"), rs.getString("packaged_and_label"));
                 fBADetails.add(fBADetail);
             }
             request.setAttribute("showResult", true);
             request.setAttribute("fBADetails", fBADetails);
             request.setAttribute("totalResult", fBADetails.size());
             rd.forward(request, response);
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                 try {
                    con.close();
                } catch (Exception e3) {
                }
            }
        }else if("update".equals(action)){
             try{
            RequestDispatcher rd = request.getRequestDispatcher("to_fba/update_fba_labelling.jsp");
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
             String updateSQL = "update harrisburgstore.fba_labelled_items set qty=?, last_updated_date=?, last_updated_by=? where id=?";
             PreparedStatement pUpdateQuery = con.prepareStatement(updateSQL);
             int total = Integer.valueOf(request.getParameter("total"));
             String username = (String) request.getSession().getAttribute("user");
             java.sql.Date today = new java.sql.Date(new Date().getTime());
             for (int i = 0; i < total; i++) {
                 Long qty =  Long.valueOf(request.getParameter("qty-"+i));
                 Long oldQty = Long.valueOf(request.getParameter("qty-old-"+i));
                 if(qty != oldQty){
                    pUpdateQuery.setLong(1, qty);
                    pUpdateQuery.setDate(2, today );
                    pUpdateQuery.setString(3, username );
                    pUpdateQuery.setLong(4, Long.valueOf(request.getParameter("id-"+i)) );
                    pUpdateQuery.executeUpdate();
                    pUpdateQuery.clearParameters();
                 }
             }
             
            
             request.setAttribute("updated", true);
             rd.forward(request, response);
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                 try {
                    con.close();
                } catch (Exception e3) {
                }
            }
        }else{
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            Date today = new Date();
            SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
            String todayDateString = sf.format(today);
            

            try {
                 con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
                String username = (String) request.getParameter("userName");
                String user_id = (String) request.getParameter("userID");

                StringBuffer s = new StringBuffer();
                String box = request.getParameter("box");
                if (box != null) {
                    box = box.toUpperCase();
                }


                String insertSQL = "insert into harrisburgstore.fba_labelled_items(fnsku, add_date, qty, item_name, user_id, user_name, packaged_and_label) values (?, ?, ?, ?, ?, ?, ?)";


                PreparedStatement pInsertQuery = con.prepareStatement(insertSQL);
                HashMap<String, ItemDetails> quantityMap = new HashMap<String, ItemDetails>();
                for (int i = 0; i < 50; i++) {

                    String fnsku = request.getParameter("fnsku-" + i);
                    if (fnsku != null && fnsku.length() > 0) {
                        fnsku = fnsku.toUpperCase();
                        long quantity = Long.parseLong(request.getParameter("qty-" + i));
                        String item_name = request.getParameter("itemname-" + i);
                        String packaged_and_label = request.getParameter("packaged_and_label-"+i);
                        if(packaged_and_label == null || (!"1".equals(packaged_and_label)) ){
                            packaged_and_label = "0";
                        }
                        ItemDetails prevItem = quantityMap.get(fnsku);
                        if (prevItem == null) {
                            prevItem = new ItemDetails();
                            prevItem.setQuantity(0L);
                            prevItem.setWeight(0);

                        }
                        prevItem.setPackaged_and_label(packaged_and_label);
                        prevItem.setQuantity(prevItem.getQuantity() + quantity);
                        prevItem.setName(item_name);
                        quantityMap.put(fnsku, prevItem);
                    }

                }

                for (String fnsku : quantityMap.keySet()) {
                    ItemDetails item = quantityMap.get(fnsku);


                    pInsertQuery.setString(1, fnsku);
                    pInsertQuery.setDate(2, new java.sql.Date(today.getTime()));
                    pInsertQuery.setLong(3, item.getQuantity());
                    pInsertQuery.setString(4, item.getName());
                    pInsertQuery.setString(5, user_id);
                    pInsertQuery.setString(6, username);
                    pInsertQuery.setInt(7, Integer.valueOf(item.getPackaged_and_label()));

                    pInsertQuery.executeUpdate();
                    pInsertQuery.clearParameters();
                }


                pInsertQuery.close();
                con.close();
                out.println("<a href='/HB5'>HOME</a> > <a href='/HB5/to_fba'>Scan FBA labels and prepare FBA shipment </a> > <a href='/HB5/to_fba/track_fba_labelling.jsp'>Track new FNSKUs labelled</a><h1 style='color:green'> INFO : Inserted in the database successfully </h1>");

            } catch (Exception e) {
                e.printStackTrace();
                out.println("<a href='/HB5'>HOME</a> > <a href='/HB5/to_fba'>Scan FBA labels and prepare FBA shipment </a> > <a href='/HB5/StoreTrackFBALabelling'>Track new FNSKUs labelled</a><h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
            } finally {
                out.flush();
                out.close();
                try {
                    con.close();
                } catch (Exception e3) {
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
