/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

/**
 *
 * @author debasish
 */
public class WebMessage {
    public static enum MessageType{
        FATAL, ERROR, WARNING, INFO
    }
    private MessageType messageType;
    private String description;

    public WebMessage(){}
    public WebMessage(MessageType type, String description){
        this.messageType = type;
        this.description = description;
    }
    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
