/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author debasish
 */
public class FNSKUDetails {
    private String details;
    private int totalQuantity = 0;
    private String packageType;
    private List<BoxDetails> boxes = new ArrayList<BoxDetails>();

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<BoxDetails> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<BoxDetails> boxes) {
        this.boxes = boxes;
    }
    
    public String getPackageType() {
       return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }
    
    public static class BoxDetails{
        private String boxName;
        private int quantityInBox = 0;    
        private String timeSent;
       

        public String getTimeSent() {
            return timeSent;
        }

        public void setTimeSent(String timeSent) {
            this.timeSent = timeSent;
        }

        public String getBoxName() {
            return boxName;
        }

        public void setBoxName(String boxName) {
            this.boxName = boxName;
        }

        public int getQuantityInBox() {
            return quantityInBox;
        }

        public void setQuantityInBox(int quantityInBox) {
            this.quantityInBox = quantityInBox;
        }
       
    }
}
