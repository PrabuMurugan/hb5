/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

/**
 *
 * @author debasish
 */
public class ItemDetails {

    private String fnsku;
    private long quantity;
    private String name;
    private String box;
    private double weight;
    private Double expectedWeight;
    private String sku;
    private boolean sent;
    private String packaged_and_label;

    public String getPackaged_and_label() {
        return packaged_and_label;
    }

    public void setPackaged_and_label(String packaged_and_label) {
        this.packaged_and_label = packaged_and_label;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public Double getExpectedWeight() {
        return expectedWeight;
    }

    public void setExpectedWeight(Double expectedWeight) {
        this.expectedWeight = expectedWeight;
    }

    
    
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getFnsku() {
        return fnsku;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public void setFnsku(String fnsku) {
        this.fnsku = fnsku;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
