/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import java.util.Date;

/**
 *
 * @author Abani
 */
public class FBADetail {
    long id;
    String fnsku;
    Long qty;
    Date addDate;
    String userName;
    String itemName;
    String packaged_and_label;

    public FBADetail(String fnsku, Long qty, Date addDate, String userName, String itemName, String packaged_and_label) {
        this.fnsku = fnsku;
        this.qty = qty;
        this.addDate = addDate;
        this.userName = userName;
        this.itemName = itemName;
        this.packaged_and_label = packaged_and_label;
    }
    
     public FBADetail(long id, String fnsku, Long qty, Date addDate, String userName, String itemName, String packaged_and_label) {
        this.id = id;
        this.fnsku = fnsku;
        this.qty = qty;
        this.addDate = addDate;
        this.userName = userName;
        this.itemName = itemName;
        this.packaged_and_label = packaged_and_label;
    }

    
    public String getFnsku() {
        return fnsku;
    }

    public void setFnsku(String fnsku) {
        this.fnsku = fnsku;
    }

    public Long getQty() {
        return qty;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPackaged_and_label() {
        return packaged_and_label;
    }

    public void setPackaged_and_label(String packaged_and_label) {
        this.packaged_and_label = packaged_and_label;
    }

    
    
    
    
}
