/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
public class GenerateFile extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        try {
            /* TODO output your page here. You may use following sample code. */
            String updateSQL = "update fba_box set date_sent_on_pallet=curdate() where box=?";
            conn = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            PreparedStatement pStmt = conn.prepareStatement(updateSQL);

            FileInputStream fstream0 = null;
            FileWriter fstream_out = null;
            try {
                fstream0 = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\createfbashipment_template.txt");
                fstream_out = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\createfbashipment.txt");
            } catch (Exception ex) {
                fstream0 = new FileInputStream("/home/debasish/JSP files/createfbashipment_template.txt");
                fstream_out = new FileWriter("/home/debasish/JSP files/createfbashipment1.txt");
            }
            // Get the object of DataInputStream
            DataInputStream in0 = new DataInputStream(fstream0);
            BufferedReader br0 = new BufferedReader(new InputStreamReader(in0));
            String st;


            PrintWriter file_out = new PrintWriter(fstream_out);


            while ((st = br0.readLine()) != null) {
                file_out.print(st.replaceAll("\\?\\?\\?\\?\\?", request.getParameter("MerchantShipmentName")));
                file_out.println();
            }

            /* TODO output your page here. You may use following sample code. */
            List<ItemDetails> items = (List<ItemDetails>) request.getSession().getAttribute("ItemDetails");
            file_out.println("MerchantSKU\tQuantity");
            HashMap<String, ItemDetails> itemMap = new HashMap<String, ItemDetails>();
            for (ItemDetails item : items) {
                pStmt.setString(1, item.getBox());
                pStmt.executeUpdate();
                ItemDetails itemInMap = itemMap.get(item.getFnsku());
                if (itemInMap == null) {
                    itemMap.put(item.getFnsku(), item);
                } else {
                    itemInMap.setQuantity(item.getQuantity() + itemInMap.getQuantity());
                }

            }

            for (ItemDetails item : itemMap.values()) {
                file_out.println((item.getSku() != null ? item.getSku() : item.getFnsku()) + "\t" + item.getQuantity() + "\t\t\t\t\t\"" + item.getName() + "\"");
            }
            file_out.flush();
            file_out.close();
            conn.close();

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Save File</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Stored FBA Shipment file successfully at C:\\Google Drive\\Dropbox\\fba\\createfbashipment.txt.  <br/> PLEASE MAKE SURE YOU FILL THE SHIPMENT NAME ON FIRST LINE IN THE FILE.<br/>  Please wait for 30 seconds for this file to be udpated on your local computer. <br/> If you already have this file open in Microsoft Excel on your local computer, the file will not be updated. <br/> So close this file and wait for 30 seconds. ");

            out.println("</body>");
            out.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
        } finally {
            out.close();
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
