/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
public class GetBoxWeightFromBoxLabel extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DecimalFormat df = new DecimalFormat("#.##");
        SimpleDateFormat sf = new SimpleDateFormat("MM-dd-yyyy");
        Connection con = null;
        try {
            /* TODO output your page here. You may use following sample code. */
            String boxLabel = request.getParameter("box");
            if (boxLabel == null || boxLabel.length() == 0) {
                return;
            }
            boxLabel = boxLabel.toUpperCase();
            String query = "Select sum(quantity * weight_specification),date_sent_on_pallet from fba_box where box=? group by date_sent_on_pallet";
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, boxLabel);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                double val = rs.getDouble(1);
                Date date_sent = rs.getDate("date_sent_on_pallet");
                if (val == 0) {
                    response.sendError(500);
                    return;
                }
                if(date_sent != null)
                    out.print(df.format(val)+"|"+sf.format(date_sent));
                else
                    out.print(df.format(val)+"|null");
            } else {
                response.sendError(500);
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(500);
        } finally {
            out.close();
            try { con.close();} catch (Exception ex) { }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
