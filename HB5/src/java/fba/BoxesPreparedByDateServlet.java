/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.ElFunctionUtil;

/**
 *
 * @author debasish
 */
public class BoxesPreparedByDateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {
            String boxTS = request.getParameter("ts");
            String boxInit = request.getParameter("boxInit");
            String sql;
            if (boxInit != null) {
                sql = "SELECT f.fnsku, f.sku, f.box, f.quantity, f.ts, f.date_sent_on_pallet, f.weight_specification, e.product_name from fba_box f left outer join existing_inventory_fba e on (f.fnsku=e.fnsku) where date(ts)=? and substring(box,1,2)=?";
            } else {
                sql = "SELECT f.fnsku, f.sku, f.box, f.quantity, f.ts, f.date_sent_on_pallet, f.weight_specification, e.product_name from fba_box f left outer join existing_inventory_fba e on (f.fnsku=e.fnsku) where date(ts)=?";
            }

            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setDate(1, new Date(Long.parseLong(boxTS)));
            System.out.println("Date=" + ElFunctionUtil.formatDate(new Date(Long.parseLong(boxTS))));
            if (boxInit != null) {
                stmt.setString(2, boxInit);
            }

            Map<String, List<ItemDetails>> boxes = new HashMap<String, List<ItemDetails>>();
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String box = rs.getString(3);
                List<ItemDetails> itemsInBox = boxes.get(box);
                if (itemsInBox == null) {
                    itemsInBox = new ArrayList<ItemDetails>();
                    boxes.put(box, itemsInBox);
                }
                ItemDetails item = new ItemDetails();
                item.setBox(box);
                item.setFnsku(rs.getString(1));
                item.setName(rs.getString(8));
                item.setQuantity(rs.getLong(4));
                item.setWeight(rs.getDouble(7));
                item.setSku(rs.getString(2));
                item.setSent(rs.getDate(6) != null);
                itemsInBox.add(item);
            }
            System.out.println(boxes.keySet());
            request.setAttribute("boxes", boxes);
            request.getRequestDispatcher("to_fba/BoxesPreparedByServlet.jsp").forward(request, response);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
