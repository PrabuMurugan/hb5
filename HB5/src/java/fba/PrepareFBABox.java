package fba;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author us083274
 */
public class PrepareFBABox extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);

            StringBuffer s = new StringBuffer();
            String box = request.getParameter("box");
            if(box !=null){
                box = box.toUpperCase();
            }

            String sql = "select sku,product_name   from tajplaza.existing_inventory_fba where fnsku=? ";
            String insertSQL = "insert into fba_box(fnsku, sku, box, quantity, weight_specification) values (?, ?, ?, ?, ?)";
           
            
            PreparedStatement pSkuQuery = con.prepareStatement(sql);
            PreparedStatement pInsertQuery = con.prepareStatement(insertSQL);
            HashMap<String, ItemDetails> quantityMap = new HashMap<String, ItemDetails>();
            for(int i=0;i<50;i++){
                
                String fnsku = request.getParameter("fnsku-"+i);
                if(fnsku!=null && fnsku.length()>0){
                    fnsku = fnsku.toUpperCase();
                    long quantity = Long.parseLong(request.getParameter("qty-"+i));
                    double weight = Double.parseDouble(request.getParameter("weight-"+i));
                    ItemDetails prevItem = quantityMap.get(fnsku);
                    if(prevItem==null){
                        prevItem = new ItemDetails();
                        prevItem.setQuantity(0L);
                        prevItem.setWeight(0);
                    }
                    prevItem.setQuantity(prevItem.getQuantity()+quantity);
                    prevItem.setWeight(prevItem.getWeight()+weight);
                    quantityMap.put(fnsku, prevItem);
                }
                
            }
            
            for(String fnsku:quantityMap.keySet()){
                ItemDetails item = quantityMap.get(fnsku);
                
                pSkuQuery.clearParameters();
                pSkuQuery.setString(1, fnsku);
                ResultSet skuResult = pSkuQuery.executeQuery();
                if(skuResult.next()){
                    String sku = skuResult.getString(1);
                    pInsertQuery.setString(2, sku);
                   
                }else{
                    pInsertQuery.setString(2, null);
                }
               
                pInsertQuery.setString(1, fnsku);

                pInsertQuery.setString(3, box);
                pInsertQuery.setLong(4, item.getQuantity());
                pInsertQuery.setDouble(5, item.getWeight());
                pInsertQuery.executeUpdate();
                pInsertQuery.clearParameters();
            }
            
            
            pSkuQuery.close();
            pInsertQuery.close();
            con.close();
            out.println("<h1 style='color:green'> INFO : Inserted in the database successfully </h1>");
            
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + e.getMessage() + "</h1>");
        } finally {
            out.flush();
            out.close();
            try {
                con.close();
            } catch (Exception e3) {
            }
        }
        

    }
    
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
