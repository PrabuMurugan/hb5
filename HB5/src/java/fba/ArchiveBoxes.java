/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import constants.ActionConstants;
import database.DBWrapperNew;
import database.SQLConstants;
import entities.FBABox;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.HBLoggerNew;
import util.TagUtils;

/**
 *
 * @author debasish
 */
public class ArchiveBoxes extends HttpServlet {
    private final String MODULE="ArchiveBoxes";
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HBLoggerNew.debug(MODULE, "Inside processRequest of ArchiveBoxes");
        response.setContentType("text/html;charset=UTF-8");
        Connection con = null;
        PreparedStatement insertStmt = null;
        PreparedStatement deleteStmt = null;
        PreparedStatement selectStmt = null;
        Timestamp ts = new Timestamp(System.currentTimeMillis());
       
        try {
            String strAction = request.getParameter("actionid");
          
            if (strAction == null) {
                HBLoggerNew.debug(MODULE, "Initial Request For Archive Box");
            
            } else if (strAction.equalsIgnoreCase(ActionConstants.ARCHIVE_BOX)) {
                HBLoggerNew.debug(MODULE, "Requesting For Archiving Box");
                String[] boxIDs = request.getParameterValues("box");
                HBLoggerNew.debug(MODULE, "Number of Boxex to Delete : " + boxIDs.length);
                
                for (String box : boxIDs) {
                    if (box != null && !box.equalsIgnoreCase("")) {
                        HBLoggerNew.debug(MODULE, "box : " + box);
                    }
                }
                
                List<WebMessage> messages = new ArrayList<WebMessage>();
              
                con = DBWrapperNew.getConnection(request,  DBWrapperNew.TJ);
                con.setAutoCommit(false); 
                insertStmt = con.prepareStatement(SQLConstants.INSERT_ARCHIEVED_BOX);
                deleteStmt = con.prepareStatement(SQLConstants.DELETE_FBA_BOX);
                selectStmt = con.prepareStatement(SQLConstants.GET_FBA_BOX_DETAIL);
                HBLoggerNew.debug(MODULE, "Queries are prepared.");
                for (String boxID : boxIDs) {
                    if (boxID != null && !boxID.equalsIgnoreCase("")) {
                        
                          HBLoggerNew.debug(MODULE, "Archiving box number : " + boxID);
                          selectStmt.setString(1,boxID);
                          ResultSet rs = selectStmt.executeQuery();
                          boolean bFound = false;
                          FBABox fBABox = new FBABox();
                          while(rs != null && rs.next()) {
                              HBLoggerNew.debug(MODULE, ""+boxID+ " found in database");
                              bFound = true;
                              
                              fBABox.setFnsku(rs.getString(SQLConstants.FBA_BOX_FNSKU));
                              fBABox.setSku(rs.getString(SQLConstants.FBA_BOX_SKU));
                              fBABox.setSku(rs.getString(SQLConstants.FBA_BOX_BOX));
                              fBABox.setId(rs.getString(SQLConstants.FBA_BOX_ID));
                              fBABox.setTs(rs.getString(SQLConstants.FBA_BOX_TS));
                              fBABox.setDate_sent_on_pallet(rs.getDate(SQLConstants.FBA_BOX_DATE_SENT_ON_PALLET));
                              fBABox.setWeight_specification(rs.getString(SQLConstants.FBA_BOX_WIGHT_SPECIFICATION));
                              HBLoggerNew.debug(MODULE, "Box information : " + fBABox);
                          }
                          if(bFound) {
                              // Insert into archived_fba_box
                              insertStmt.setString(1, fBABox.getFnsku());
                              insertStmt.setString(2,fBABox.getSku());
                              insertStmt.setString(3, fBABox.getBox());
                              insertStmt.setString(4, fBABox.getQuantity());
                              insertStmt.setString(5, fBABox.getId());
                              insertStmt.setString(6, fBABox.getTs());
                              if(fBABox.getDate_sent_on_pallet() != null)
                                insertStmt.setDate(7, new java.sql.Date(fBABox.getDate_sent_on_pallet().getTime()));
                              else
                                insertStmt.setDate(7, null); 
                              insertStmt.setString(8, fBABox.getWeight_specification());
                              insertStmt.setTimestamp(9, ts);
                              insertStmt.execute();
                              
                              deleteStmt.setString(1, boxID);
                              deleteStmt.execute();
                              con.commit();
                              HBLoggerNew.debug(MODULE, "Insert done in archive table and delete done in fba table.");
                              messages.add(new WebMessage(WebMessage.MessageType.INFO, boxID + " archived successfully"));
                          } else {
                              HBLoggerNew.debug(MODULE, "Box not found in database");
                              messages.add(new WebMessage(WebMessage.MessageType.ERROR, boxID + " not found"));
                              continue;
                          }
                          if(rs != null && !rs.isClosed()) {
                              rs.close();
                          }
                    }
                }
              
                request.setAttribute("messages", messages.size() > 0 ? messages : null);
            }

        } catch (Exception ex) {
            HBLoggerNew.error(MODULE, "Exception occured while archiving box : " + ex.getMessage());
            try {
                if(con != null) con.rollback();
                ex.printStackTrace();
                request.setAttribute("messages", TagUtils.createMessageList(new WebMessage(WebMessage.MessageType.FATAL, ex.getMessage())));
            } catch (SQLException ex1) {
                Logger.getLogger(ArchiveBoxes.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if(insertStmt != null && ! insertStmt.isClosed()) {
                    insertStmt.close();
                }
                if(deleteStmt != null && !deleteStmt.isClosed()) {
                    deleteStmt.close();
                }
                if(selectStmt != null && !selectStmt.isClosed()) {
                    selectStmt.close();
                }
                if(con != null && !con.isClosed()) {
                    con.close();
                }
            } catch (Exception ex) {
                HBLoggerNew.error(MODULE, "Exception occured while removing resources : " + ex.getMessage());
            }
        }
        HBLoggerNew.debug(MODULE, "Leaving processRequest of ArchiveBoxes");
        request.getRequestDispatcher("to_fba/DeleteBoxes.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
