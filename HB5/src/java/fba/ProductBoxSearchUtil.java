/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author debasish
 */
public class ProductBoxSearchUtil {

    public static List<ItemDetails> getItems(HttpServletRequest request, String... boxIDs) throws Exception {
        List<ItemDetails> itemList = new ArrayList<ItemDetails>();
        Connection con = null;
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            PreparedStatement getBoxDetailsStmt = con.prepareStatement("select fba_box.fnsku, quantity, ifnull(product_name,'Unknown - FNSKU will be written to the file'), weight_specification, fba_box.sku from fba_box left join existing_inventory_fba on (fba_box.sku = existing_inventory_fba.sku) where fba_box.box = ?");
            HashSet<String> boxUniqueIDs = new HashSet<String>();
            for (String boxID : boxIDs) {
                boxID = boxID.toUpperCase();
                boxUniqueIDs.add(boxID);
            }
            for (String box : boxUniqueIDs) {
                box = box.toUpperCase();
                getBoxDetailsStmt.setString(1, box);
                ResultSet res = getBoxDetailsStmt.executeQuery();
                while (res.next()) {
                    ItemDetails details = new ItemDetails();
                    details.setFnsku(res.getString(1));
                    details.setQuantity(res.getLong(2));
                    details.setName(res.getString(3));
                    details.setWeight(res.getDouble(4));
                    details.setSku(res.getString(5));
                    details.setBox(box);

                    itemList.add(details);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        return itemList;
    }

    public static List<BoxDetails> getUndelieveredBoxes(HttpServletRequest request) throws Exception {
        List<BoxDetails> itemList = new ArrayList<BoxDetails>();
        Connection con = null;
        try {
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
            String hide = request.getParameter("hide");
            String strQuery = "select box, sum(quantity * weight_specification) from fba_box where fba_box.date_sent_on_pallet is null ";
            strQuery += ((hide!=null && hide.equalsIgnoreCase("on") ? " and box not like 'SKID%'" : ""));
            PreparedStatement getBoxDetailsStmt = con.prepareStatement(strQuery + " group by box");


            ResultSet res = getBoxDetailsStmt.executeQuery();
            while (res.next()) {

                BoxDetails box = new BoxDetails();
                box.setName(res.getString(1));
                box.setWeight(res.getDouble(2));
                itemList.add(box);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        return itemList;
    }
}
