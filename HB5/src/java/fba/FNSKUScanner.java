/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
    public class FNSKUScanner extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {
            
            String fnsku = request.getParameter("fnsku");
            if(fnsku != null) {
                con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);
                String packageType = "";
                String packageTypeQuery = "SELECT packaged_and_label from harrisburgstore.fba_labelled_items fb where fb.fnsku=? order by fb.add_date desc limit 1";
                PreparedStatement pstmt = con.prepareStatement(packageTypeQuery);
                pstmt.setString(1, fnsku);
                ResultSet prs = pstmt.executeQuery();
                if(prs.next()){
                    packageType = prs.getInt("packaged_and_label") == 0 ? "Just Labelled" : "Packaged and Labelled" ;
                }
                String query = "SELECT f.box, sum(f.quantity), e.product_name, f.date_sent_on_pallet from fba_box f left join existing_inventory_fba e on (e.fnsku = f.fnsku) where (f.fnsku = '"+fnsku+"' or (lower(e.product_name) like lower( '%"+fnsku+"%'))) group by f.box order by f.id desc";
                PreparedStatement stmt = con.prepareStatement(query);
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    FNSKUDetails details = new FNSKUDetails();
                    details.setPackageType(packageType);
                    details.setDetails(rs.getString(3));
                    do {
                        FNSKUDetails.BoxDetails boxDetails = new FNSKUDetails.BoxDetails();
                        boxDetails.setBoxName(rs.getString(1));
                        boxDetails.setQuantityInBox(rs.getInt(2));
                        Date date = rs.getDate(4);
                        if (date != null) {
                            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                            boxDetails.setTimeSent(df.format(date));
                        }
                        details.setTotalQuantity(details.getTotalQuantity() + boxDetails.getQuantityInBox());
                        details.getBoxes().add(boxDetails);
                    } while (rs.next());

                    request.setAttribute("fnskuDetails", details);
                } else {
                    throw new Exception("FNSKU not available");
                }
            } else {
                request.removeAttribute("fnskuDetails");
            }
            request.getRequestDispatcher("to_fba/FNSKUScan.jsp").forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + ex.getMessage() + "</h1>");
        } finally {
            out.close();
            try {
                if(con != null && !con.isClosed())con.close();
            } catch (Exception ex) { ex.printStackTrace();      }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
