/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import java.util.List;

/**
 *
 * @author debasish
 */
public class BoxDetails {
    private String name;
    private double weight;
    private List<ItemDetails> items;

    public List<ItemDetails> getItems() {
        return items;
    }

    public void setItems(List<ItemDetails> items) {
        this.items = items;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    
    
}
