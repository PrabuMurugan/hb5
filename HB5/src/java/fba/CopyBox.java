/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fba;

import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author debasish
 */
public class CopyBox extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection con = null;
        try {

            String box = request.getParameter("box");
            if (box == null || box.length() == 0) {
                throw new IllegalArgumentException("No input");
            }
            con = DBWrapperNew.getConnection(request, DBWrapperNew.TJ);

            String insertString = "insert into fba_box(fnsku, sku, box, quantity,ts, weight_specification) values (?,?,?,?,current_timestamp(),?)";

            List<ItemDetails> items = ProductBoxSearchUtil.getItems(request, box);
            con.setAutoCommit(false);
            for (int i = 0; i < 300; i++) {

                String boxNew = request.getParameter("box-" + i);
                if (boxNew != null && boxNew.length() > 0) {
                    boxNew = boxNew.toUpperCase();
                    List<ItemDetails> itemsNew = ProductBoxSearchUtil.getItems(request, boxNew);
                    if (itemsNew.size() > 0) {
                        throw new IllegalArgumentException("Box " + boxNew + " Already exists");
                    }
                    PreparedStatement insertStatement = con.prepareStatement(insertString);
                    for (ItemDetails item : items) {
                        insertStatement.setString(1, item.getFnsku());
                        insertStatement.setString(2, item.getSku());
                        insertStatement.setString(3, boxNew);
                        System.out.println(item.getQuantity());
                        insertStatement.setLong(4, item.getQuantity());
                        insertStatement.setDouble(5, item.getWeight());
                        insertStatement.addBatch();
                    }
                    insertStatement.executeBatch();

                }

            }

            con.commit();

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CopyBox</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Copied boxes successfully</h1>");
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            ex.printStackTrace();
            out.println("<h1 style='color:red'> ERROR :" + ex.getMessage() + "</h1>");
        } finally {
            out.close();
            try {
                con.close();
            } catch (Exception ex) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
