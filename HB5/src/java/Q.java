/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import queries.DAO;

/**
 *
 * @author US083274
 */
public class Q extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<script src='//code.jquery.com/jquery-1.11.3.min.js'></script><script src='//code.jquery.com/jquery-migrate-1.2.1.min.js'></script>");
        out.println("<style> th,td{border:1px solid;}  tr:nth-child(even) { background-color: #CCC;}</style>");
         out.println("<link rel=\"stylesheet\" href=\"to_fba/common.css\" type=\"text/css\" />");
        request.getRequestDispatcher("to_fba/CommonHeader.jsp").include(request, response);
        out.println("-&gt");
        out.println("<a href=\"inventory_management.jsp\">Inventory Management</a><br/>");
        String category_in=request.getParameter("category");
        if(category_in!=null)
            category_in=URLDecoder.decode(category_in);
        String sub_category_in=request.getParameter("sub_category");
        if(sub_category_in!=null)
            sub_category_in=URLDecoder.decode(sub_category_in);
        
        String sno_in=request.getParameter("sno");
        if(sno_in!=null)
            sno_in=URLDecoder.decode(sno_in);
        
        System.out.println("category_in:"+category_in);
        String currentCategory = null;
        String currentSubCategory = null;
         BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("/home/ec2-user/Dropbox/web_sql.txt")));
        try {
            /* TODO output your page here. You may use following sample code. */
            String s;
            if(category_in!=null  || sub_category_in!=null || sno_in!=null){
                if(request.getParameter("origin")!=null && request.getParameter("origin").length()>0)
                        out.println("<h1>  <a href="+request.getParameter("origin")+"> Home </a></h1>");
                    else
                         out.println("<h1>  <a href=Q> Home </a></h1>");
            }
            ArrayList<String> WEB_SQL = new ArrayList();
               
                ArrayList cat_arr=new ArrayList();
                ArrayList sub_cat_arr=new ArrayList();
                while ((s = br001.readLine()) != null )   {
                   // System.out.println("Lenght o fs.split:"+s.split("\t").length);
                    if(s.split("\t").length<3)
                        continue;
                        String sno=s.split("\t")[0];
                        String display=s.split("\t")[1];
                        String category=s.split("\t")[2];
                        String sub_category=s.split("\t")[3];
                        String sql_name=s.split("\t")[4];
                        String sql=s.split("\t")[5];
                        if(sno.equals(sno_in)){
                            currentCategory = category;
                            currentSubCategory = sub_category;
                            out.println("<a href=\"/HB5/Q?category="+currentCategory.replaceAll(" ", "+") +"&sub_category="+currentSubCategory.replaceAll(" ", "+") +" \">"+currentCategory+"</a> -> ");
                            out.println("<a href=\"/HB5/Q?category="+currentCategory.replaceAll(" ", "+") +"&sub_category="+currentSubCategory.replaceAll(" ", "+") +" \">"+currentSubCategory+"</a> -> "+ sql_name+"<br/>");
                        }
                       System.out.println("display:"+display);
                        if(display!=null && display.trim().equals("Y")){
                            WEB_SQL.add(sno+"||"+category+"||"+sub_category+"||"+sql);
                          
                            if(category_in==null  && sub_category_in==null && sno_in==null){
                                if(cat_arr.contains(category)==false)
                                    out.println("<h1> <a href=Q?category="+URLEncoder.encode(category)+">"+category+"</a></h1>");
                                cat_arr.add(category);
                            } 
                            System.out.println("category_in:"+ category_in+",category:"+category);
                            if(category_in!=null && category_in.equals(category) && sub_category_in==null && sno_in==null){
                                if(sub_cat_arr.contains(sub_category)==false)
                                    out.println("<h2> <a href=Q?category="+URLEncoder.encode(category)+"&sub_category="+URLEncoder.encode(sub_category)+">"+sub_category+"</a></h2>");
                                sub_cat_arr.add(sub_category);
                                
                            }
                            if(category_in!=null && category_in.equals(category) && sub_category_in!=null && sub_category_in.equals(sub_category)&& sno_in==null){
                                
                                    out.println("<h3> <a href=Q?sno="+URLEncoder.encode(sno)+">"+sql_name+"</a></h3>");
                                
                                
                            }

                            
                        }
                }
                  
                if(sno_in!=null){
                     DAO d=new DAO();
                     System.out.println("Going to call getAndExecuteQuery for sno:"+sno_in);
                     String q1=request.getParameter("q1");
                     String q2=request.getParameter("q2");
  
                     String q3=request.getParameter("q3");
                     String q4=request.getParameter("q4");
                     
                     String str=d.getAndExecuteQuery(sno_in,q1,q2,q3,q4 );
                     String strNumberofRows = new String(str);
                     int index = strNumberofRows.indexOf("<tr>");
    
                    int occurrences = 0;
                    while (index != -1) {
                        occurrences++;
                        strNumberofRows = strNumberofRows.substring(index + 1);
                        index = strNumberofRows.indexOf("<tr>");
                    }
                    System.out.println("Number of rows : " + occurrences);
                    
                    if(occurrences == 1 && sno_in.equalsIgnoreCase("18")) {
                    System.out.println("No row selected");     
                    sno_in="69";
                    str=d.getAndExecuteQuery(sno_in,q1,q2,q3,q4 ); 
                    strNumberofRows = new String(str);
                    index = strNumberofRows.indexOf("<tr>");
                    occurrences = 0;
                    while (index != -1) {
                        occurrences++;
                        strNumberofRows = strNumberofRows.substring(index + 1);
                        index = strNumberofRows.indexOf("<tr>");
                    }
                        
                    }
                    String sound = occurrences > 1 ? "found" : "NotFound";
                   
                    String strVoice = "<script> parent.playsound('"+sound+"');</script>";
                     strVoice = strVoice+str;
                     System.out.println("STR : " + strVoice);
                      if(sound.equalsIgnoreCase("found") && (sno_in.equalsIgnoreCase("18") || sno_in.equalsIgnoreCase("69") )) {
                        System.out.println("This is query for identify inventory to sent. Modifying string for highlisting fist time asin");
                        String strVoice2 = "<script> function updateRows() {var tabref = document.getElementById('table1');var table = tabref.rows.length;for(i=0;i<table;i++) {var innerTxt = tabref.rows[i].cells[0].innerHTML;if(innerTxt.indexOf('<h2>') !== -1) {tabref.rows[i].cells[tabref.rows[i].cells.length-1].innerHTML+='<br><br><font color=\"red\">FIRST TIME NEW ITEM, CHECK ON AMAZON AND CONFIRM ITS SAME ITEM WHAT YOU HAVE</font>';}}}updateRows(); </script>";
                                System.out.println("Rakesh : "+str);
                               str = str.replace("<table","<table id='table1' "); 
                               System.out.println("Rakesh : "+str.contains("<table"));
                               System.out.println("Rakesh : "+str);
                        strVoice = str+strVoice2;
                    }
                     strVoice=strVoice+"<br/>"+"sno:"+sno_in+"q1:"+q1+",ffq2:"+q2;
                     out.println(strVoice);
                }                
                
             
        }catch (Exception e)      {
            e.printStackTrace();
        } finally {     
            br001.close();
            out.close();
        }
    }
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
