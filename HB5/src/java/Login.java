/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import database.DBWrapperNew;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author abanipatra
 */
public class Login extends HttpServlet {

    public static final int ONE_DAY = 60 * 60 * 24;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, "action:" + request.getParameter("action"));
        try {
            if (session.getAttribute("user") != null && (Boolean) session.getAttribute("authenticated")) {
                response.sendRedirect("inventory_management.jsp");
            } else if ("doLogin".equals(request.getParameter("action"))) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                Connection con = null;
                try {
                    String dbUsername = "";
                    String dbPassword = "";
                    int fileUserId = 0;
                    System.out.println("PRABU002 GOING TO CONNECT");
                    con = DBWrapperNew.getConnection(request, DBWrapperNew.HBG);
                    PreparedStatement prest = con.prepareStatement("select * from users where username=?");
                    prest.setString(1, username);
                    ResultSet rs = prest.executeQuery();
                    if (rs.next()) {
                        dbUsername = rs.getString("username");
                        dbPassword = rs.getString("password");
                        fileUserId = rs.getInt("id");
                    }
                    if (dbUsername.equals(username) && dbPassword.equals(password)) {
                        session.setAttribute("user", username);
                        session.setAttribute("user_id", fileUserId);

                        session.setAttribute("authenticated", true);

                        Cookie uCookie = new Cookie("cUname", username);
                        uCookie.setMaxAge(ONE_DAY);
                        response.addCookie(uCookie);
                        Cookie uidCookie = new Cookie("cUID", String.valueOf(fileUserId));
                        uidCookie.setMaxAge(ONE_DAY);
                        response.addCookie(uidCookie);
                        if (session.getAttribute("requestedURL") != null) {
                            String requestedURL = (String) session.getAttribute("requestedURL");
                            requestedURL = "/" + requestedURL.substring(requestedURL.indexOf("HB5"));
                            response.sendRedirect(requestedURL);
                        } else {
                            if (session.getAttribute("nextPage") == null) {
                                response.sendRedirect("inventory_management.jsp");
                            } else {
                                response.sendRedirect(session.getAttribute("nextPage").toString());
                            }
                        }


                    } else {
                        session.setAttribute("authenticated", false);
                        response.sendRedirect("/HB5/login.jsp");
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                    response.sendRedirect("/HB5/login.jsp");
                }finally{
                    con.close();
                }
            } else {
                response.sendRedirect("/HB5/login.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("/HB5/login.jsp");
        }


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
