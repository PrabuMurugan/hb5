/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author prabu
 */
public class HBFreshAwaitingOrdersSummary extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();   
        String request_type=request.getParameter("request_type");
        String all_orders=request.getParameter("all_orders");
        String label_printS=request.getParameter("label_print");
        Boolean all_ordersB=false;
        Boolean label_printB=false;
        
         if(all_orders!=null && all_orders.equals("true"))
             all_ordersB=true;
         if(label_printS!=null && label_printS.equals("true"))
             label_printB=true;         
        String str="";
        try{
           str= HBFreshAwaitingOrdersSummaryDAO.getString(request_type,all_ordersB,label_printB);
        }catch (Exception e){
            e.printStackTrace();;
            str=e.getMessage();
        }
        if(label_printB){
            out.println(str);
        }else{
            out.println(" <body style='margin:0px'><table border=.1px>"+str+"</table></body>");
        }
        
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrdersDifferentShippingAddress</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrdersDifferentShippingAddress at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
