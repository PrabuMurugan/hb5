/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;
import database.DBWrapperNew;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;


/**
 *
 * @author prabu
 */
public class OrdersDifferentShippingAddressDAO  {
   
    public static void main(String args[]) throws  Exception{
       
        String str=getString();
        System.out.println("STR:"+str);
       
    }
    public static String getString()       throws ServletException, IOException{
           ArrayList<String> fraudCustomersShippingAddresses=new ArrayList();
        try{
          Connection con = DBWrapperNew.getConnection(null, DBWrapperNew.BIGCOMMERCE);
           PreparedStatement ps= con.prepareStatement("select Billing_Name,Billing_Address,Billing_City,Billing_State,Billing_Zip,Shipping_Name,Shipping_Address,Shipping_City,Shipping_State,Shipping_Zip,Email_ID from hb_bc.fraudulent_Customer_Details ");
           
          ResultSet rs=ps.executeQuery();
          while(rs.next()){
              FraudCustomersVO f=new FraudCustomersVO();
              f.setBilling_Name(rs.getString("Billing_Name"));
              f.setShipping_Address(rs.getString("Billing_Address"));
              f.setBilling_Zip(rs.getString("Billing_Zip"));
              f.setShipping_Address(rs.getString("Shipping_Address"));
              f.setShipping_Zip(rs.getString("Shipping_Zip"));
              f.setEmail_ID(rs.getString("Email_ID"));
              f.setBilling_City(rs.getString("billing_city"));
              f.setShipping_City(rs.getString("shipping_city"));
              fraudCustomersShippingAddresses.add(rs.getString("Shipping_Address") );
              
          }
            con.close();            
        }catch (Exception e2){e2.printStackTrace();}

        StringBuffer retStr=new StringBuffer();

        JSONArray jsarr=new JSONArray();
           //Shipped orders to figure if same customer ordered in the past
            String webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=2&is_deleted=false&limit=250&page=1&sort=date_created:desc";
            String str=getJSONString(webPage);
           JSONArray tmpJsarr4=new JSONArray();
           if(str!=null && str.length()>0){
               tmpJsarr4=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr4.size();i++){
                   jsarr.add(tmpJsarr4.get(i));
               }           
               
           }
           if(tmpJsarr4.size()>250){
                webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=2&is_deleted=false&limit=250&page=2&sort=date_created:desc";
               str=getJSONString(webPage);
                tmpJsarr4=JSONArray.fromObject(str);  
                 for(int i=0;i<tmpJsarr4.size();i++){
                      jsarr.add(tmpJsarr4.get(i));
                 }                    
           }  
           
           //Cancelled orders
             webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=5&is_deleted=false&limit=100&page=1&sort=date_created:desc";
            str=getJSONString(webPage);
            tmpJsarr4=new JSONArray();
           if(str!=null && str.length()>0){
               tmpJsarr4=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr4.size();i++){
                   jsarr.add(tmpJsarr4.get(i));
               }           
               
           }
 
           
            webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=11&is_deleted=false&limit=250&page=1&sort=date_created:desc";
           str=getJSONString(webPage);
           JSONArray tmpJsarr1=new JSONArray();
           if(str!=null && str.length()>0){
               tmpJsarr1=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr1.size();i++){
                   jsarr.add(tmpJsarr1.get(i));
               }           
               
           }
           if(tmpJsarr1.size()>250){
                webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=11&is_deleted=false&limit=250&page=2&sort=date_created:desc";
               str=getJSONString(webPage);
                tmpJsarr1=JSONArray.fromObject(str);  
                 for(int i=0;i<tmpJsarr1.size();i++){
                      jsarr.add(tmpJsarr1.get(i));
                 }                    
           }
                 
           
            webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=12&is_deleted=false&sort=date_created:desc";
           str=getJSONString(webPage);
           if(str!=null && str.length()>0){
               JSONArray tmpJsarr2=JSONArray.fromObject(str);
               for(int i=0;i<tmpJsarr2.size();i++){
                   jsarr.add(tmpJsarr2.get(i));
               }
           }
           
           webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=13&is_deleted=false&sort=date_created:desc";
           str=getJSONString(webPage);
           System.out.println("Current JSON string:"+str);
           if(str!=null && str.length()>0){
               JSONArray tmpJsarr3=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr3.size();i++){
                   jsarr.add(tmpJsarr3.get(i));
               }
               
           }
 
           
           retStr.append("<link rel='stylesheet' href='to_fba/common.css' type='text/css' />");
           retStr.append("<style>th,td { border:1px solid}</style>");
           retStr.append("<table width='90%'>");
           retStr.append("<tr><th>Order ID</th><th>Status</th><th>Order Date</th><th>Total </th><th>Staff Notes</th><th>Notes</th></tr>");
           HashMap<String,String> shipping_streetMap=new HashMap();
           
           for (int i=0;i<jsarr.size();i++){
               JSONObject js=(JSONObject) jsarr.get(i);
               Integer order_id=(Integer) js.get("id");
                Integer status_id=(Integer)js.getInt("status_id");
               String email="";
               if(js.containsKey("billing_address") && js.getJSONObject("billing_address").containsKey("email") )
                   email= js.getJSONObject("billing_address").getString("email");
               if(email.equals("CustomerSpecialRequests@kroger.com"))
               {
                   System.out.println("Skipping CustomerSpecialRequests@kroger.com");
                   continue;
                   
               }
               if(order_id!=null &&  ( order_id ==  36669 )){
                   System.out.println("Debug");
               }
               String order_idStr="<a href='https://www.harrisburgstore.com/admin/index.php?ToDo=viewOrders&searchId=0&searchQuery="+order_id+"' target=_blank> "+order_id+"</a>";
               String date_created=(String) js.get("date_created");
               String total_inc_tax=(String) js.get("total_inc_tax");
               String ip_address=(String) js.get("ip_address");
               String payment_method=(String) js.get("payment_method");
               String base_shipping_cost=(String) js.get("base_shipping_cost");
                
               String staff_notes="";
               if(js.containsKey("staff_notes") && js.get("staff_notes") instanceof String)
                   staff_notes=(String) js.get("staff_notes");
               String status=(String) js.get("status");
               JSONObject billing_address=  js.getJSONObject("billing_address");
               
               String billing_zip=billing_address.getString("zip");
               String billing_city=(String) billing_address.get("city");
               String billing_street=(billing_address).getString("street_1");
               
               
               JSONObject shipping_address=  js.getJSONObject("shipping_addresses");
               String shipping_url=shipping_address.getString("url");
              
               str=getJSONString(shipping_url);
               if(str.length()==0){
                  continue;
               }
               //System.out.println("Converting str to shipping address json:"+str);
               JSONArray shipping_addressJR=JSONArray.fromObject(str);               
               String shipping_zip=((JSONObject)shipping_addressJR.get(0)).getString("zip");
               String shipping_street=((JSONObject)shipping_addressJR.get(0)).getString("street_1");
               String shipping_city=((JSONObject)shipping_addressJR.get(0)).getString("city") ;               
                 boolean fraudReviewNeeded=false;
                 String suggested_staff_notes="";
               if(js.containsKey("coupon_discount") && js.getInt("coupon_discount")>1){
                   JSONObject coupons=  js.getJSONObject("coupons");
                   String coupons_url=coupons.getString("url");
                    String tmpStr=getJSONString(coupons_url); 
                    if(tmpStr!=null && tmpStr.length()>0){
                       JSONArray couponssJR=JSONArray.fromObject(tmpStr);               
                       String coupon_code=((JSONObject)couponssJR.get(0)).getString("code");
                       if(coupon_code!=null && coupon_code.equals("PICKUP03") &&Float.valueOf(base_shipping_cost) >0 ){
                         fraudReviewNeeded=true;
                       if(staff_notes.length()==0)
                           suggested_staff_notes="Sorry, this PICKUP coupon code is to be used only for orders that will be picked at Mechanicsburg location. Either you can pick from our Mechanicsburg location or please  pay the  below paypal invoice to get the order shipped. ";
                           
                       }
                        
                    }
                   
               }
                 
               if(status_id!=2 && status_id!=5 && shipping_streetMap.containsKey(shipping_street)){
                     fraudReviewNeeded=true;
                   if(staff_notes.length()==0)
                       suggested_staff_notes=" Why two orders to same street address, Just check if they are legitimate.  Another orderID with same street name: "+shipping_streetMap.get(shipping_street);
                     
               }
               if(fraudCustomersShippingAddresses.contains(shipping_street )){
                     fraudReviewNeeded=true;
                   if(staff_notes.length()==0)
                       suggested_staff_notes=" Customer shipping street matching an entry in hb_bc.fraudulent_Customer_Details table";
                     
               }               
               if(shipping_streetMap.containsKey(shipping_street))
                   shipping_streetMap.put(shipping_street, shipping_streetMap.get(shipping_street)+","+order_id);
               else
                 shipping_streetMap.put(shipping_street, String.valueOf(order_id));
               if(status_id ==2 || status_id ==5){
                  // System.out.println("We dont worry about order that is shipped or cancelled if the except multiple orders to same address");
                   continue;
               }
             
               
                       
               String billing_zip_2digits=StringUtils.substring(billing_zip, 0, 2);
               String  shipping_2digits=StringUtils.substring(shipping_zip, 0, 2);
               
              /* String billing_zip_2digits=billing_zip;
               String  shipping_2digits=shipping_zip;               
             */
               if(billing_zip_2digits.equals(shipping_2digits)==false && Float.valueOf(total_inc_tax)>50 && payment_method.equals("PayPal")==false)
            //if(billing_zip.equals(shipping_zip)==false && Float.valueOf(total_inc_tax)>50 && payment_method.equals("PayPal")==false)               
               {
                   fraudReviewNeeded=true;
                   if(staff_notes.length()==0)
                       suggested_staff_notes=" Why customer entered different billing address and shipping to different state.Check the Zip code of IP address at <a target=_blank href=http://www.ip2location.com/"+ip_address+">http://www.ip2location.com/"+ip_address+"</a>. If the IP address points to different state, that doubles the risk.";
               }else if(billing_zip.equals(shipping_zip)==false && Float.valueOf(total_inc_tax)>25 && payment_method.equals("PayPal")==false){
                   //if(billing_city!=null &&shipping_city!=null && billing_city.equals(shipping_city)  ==false)
                   if(true)
                   {
                       fraudReviewNeeded=true;
                       if(staff_notes.length()==0)
                           suggested_staff_notes=" Billing and Shipping city did not match. Check the Zip code of IP address at <a target=_blank href=http://www.ip2location.com/"+ip_address+">http://www.ip2location.com/"+ip_address+"</a>. If the IP address points to different state, that doubles the risk.";
                       
                   }else if( Float.valueOf(total_inc_tax)>100){
                       fraudReviewNeeded=true;
                       if(staff_notes.length()==0)
                           suggested_staff_notes=" Billing and Shipping zip code does not match. But first 2 digits of zip and city matches. Its possible customer shipping to same city. Check the Zip code of IP address at <a target=_blank href=http://www.ip2location.com/"+ip_address+">http://www.ip2location.com/"+ip_address+"</a>. If the IP address points to different state, that doubles the risk.";
                       
                   }
                       
                   
               }
                else if(billing_street.equals(shipping_street)==false && Float.valueOf(total_inc_tax)>25 && payment_method.equals("PayPal")==false){
                   //if(billing_city!=null &&shipping_city!=null && billing_city.equals(shipping_city)  ==false)
                       fraudReviewNeeded=true;
                       if(staff_notes.length()==0)
                           suggested_staff_notes=" Billing and Shipping strets did not match. Check the Zip code of IP address at <a target=_blank href=http://www.ip2location.com/"+ip_address+">http://www.ip2location.com/"+ip_address+"</a>. If the IP address points to different state, that doubles the risk.";
                       
                       
                   
               }               
               if(fraudReviewNeeded==false && Float.valueOf(total_inc_tax)>600)
               {
                   fraudReviewNeeded=true;
                   if(staff_notes.length()==0)
                       suggested_staff_notes=" Why such big order"; 
               }
               /*if(fraudReviewNeeded==false && Float.valueOf(total_inc_tax)>100)
               {
                   try{
                       
                   
                   JSONObject products=js.getJSONObject("products");
                     String product_url=products.getString("url");
                      str=getJSONString(product_url);
                     JSONArray productsJR=JSONArray.fromObject(str);     
                     for(int p=0;p<productsJR.size();p++){
                         JSONObject pr=productsJR.getJSONObject(p);
                         Long quantity=pr.getLong("quantity");
                         String item_name=pr.getString("name");
                         if(quantity>10){
                           fraudReviewNeeded=true;
                           if(staff_notes.length()==0)
                               suggested_staff_notes="Order size >$100, check why customer ordered "+quantity +" of "+item_name; 
                            break; 
                         }
                         
                     }
                   }catch (Exception e2){e2.printStackTrace();}
               }       */        
                if(fraudReviewNeeded){ 
                   retStr.append("<tr>");
                   retStr.append("<td>"+order_idStr+"</td>");
                   retStr.append("<td>"+status+"</td>");
                   retStr.append("<td>"+date_created+"</td>");
                   retStr.append("<td>"+total_inc_tax+"</td>");
                   retStr.append("<td>"+staff_notes+"</td>");
                   retStr.append("<td>"+suggested_staff_notes+"</td>");
                   insertIntoDB( order_id ,suggested_staff_notes);
                   
               }
               
           }
           retStr.append("</table>");
          
           return retStr.toString();
    }
    public static void insertIntoDB(Integer order_id,String suggested_staff_notes) {
        try{
            Connection con = DBWrapperNew.getConnection(null, DBWrapperNew.BIGCOMMERCE);
             PreparedStatement ps=con.prepareStatement("select 1 from    hb_bc.OrdersDifferentShippingAddress  where order_id=?");
             ps.setInt(1, order_id);
             ResultSet rs=ps.executeQuery();
             if(rs.next())
                         return;
            ps=con.prepareStatement("insert into hb_bc.OrdersDifferentShippingAddress (order_id,suggested_staff_notes) values (?,?)");
            ps.setInt(1, order_id);
            ps.setString(2, suggested_staff_notes);
            ps.executeUpdate();
            con.close();
        }catch (Exception e2){
            e2.printStackTrace();
        }
        
        
    }
  public static String getJSONString(String strAPI) throws ServletException, IOException {
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }        
}

