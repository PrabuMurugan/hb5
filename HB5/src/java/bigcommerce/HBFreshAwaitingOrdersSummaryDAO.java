/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author prabu
 */
public class HBFreshAwaitingOrdersSummaryDAO {
    public static void main(String args[]) throws  Exception{
       // String str=getString("unique_ordered_products",true);
       String str=getString("customer_ordered_products",true,false);
        
        System.out.println("STR:"+str);
    }
    public static String getString(String request_type, boolean all_orders,boolean label_print) throws  Exception{
        StringBuffer retStr=new StringBuffer();

        JSONArray jsarr=new JSONArray();
        
            String webPage ="";
            if(all_orders==false)
              webPage=  "https://store-nxvl9uh.mybigcommerce.com/api/v2/orders.json?status_id=11&is_deleted=false&limit=250&page=1&sort=date_created:desc";
            else
                webPage=  "https://store-nxvl9uh.mybigcommerce.com/api/v2/orders.json?is_deleted=false&limit=250&page=1&sort=date_created:desc";
           String str=getJSONString(webPage);
           JSONArray tmpJsarr1=new JSONArray();
           TreeMap<String,Integer> unique_ordered_products=new TreeMap();
           TreeMap<String,String> customer_ordered_products=new TreeMap();
           if(str!=null && str.length()>0){
               tmpJsarr1=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr1.size();i++){
                   JSONObject order=(JSONObject)tmpJsarr1.get(i) ;
                   String id=order.getString("id");
                    JSONObject billing_address=order.getJSONObject("billing_address");
                    String customer_name=billing_address.getString("first_name")+" "+billing_address.getString("last_name")+" ("+billing_address.getString("phone")+")";
                   JSONObject products=((JSONObject)tmpJsarr1.get(i)).getJSONObject("products");
                     String product_url=products.getString("url");
                      str=getJSONString(product_url);
                     JSONArray productsJR=JSONArray.fromObject(str);   
                      String current_productS="";
                     for(int j=0;j<productsJR.size();j++){
                         JSONObject product=(JSONObject)productsJR.get(j);
                         String name=product.getString("name");
                         Integer current_quantity=product.getInt("quantity");
                         String event_date=null;
                         if(all_orders==false && product.containsKey("event_date")){
 
                             event_date=product.getString("event_date");
                             if(event_date.indexOf("00:00:00 +000")>=0){
                                 event_date=StringUtils.substringBefore(event_date, "00:00:00 +000").trim();
                                 //Fri, 17 Jul 2015 00:00:00 +0000
                                
                                 Date ev_date=new SimpleDateFormat("EEE, d MMM yyyy").parse(event_date);
                                  Date today = new Date();    
                                  if(ev_date.getDate()!=today.getDate()){
                                      System.out.println("This product is not today. skip it");
                                      continue;
                                  }

                                 
                             }
                                     
                         }
                         
                         JSONArray product_options=product.getJSONArray("product_options");
                         for (int k=0;k<product_options.size();k++){
                              String display_name=product_options.getJSONObject(k).getString("name");
                              String display_value=product_options.getJSONObject(k).getString("display_value");
                              
                              //name+=";"+display_name+"->"+display_value;
                              name+=display_name+":"+display_value+";";
                         }
                                 
                        Integer quantity=current_quantity;
                         if(unique_ordered_products.containsKey(name)){
                             Integer existing_quantity=unique_ordered_products.get(name);
                             quantity+=existing_quantity;
                         }
                         unique_ordered_products.put(name, quantity);
                         
                         current_productS+=name+"->"+current_quantity ;
                         
                     }
                     if(current_productS!=null && current_productS.length()>0){
                         customer_name+=";"+id;
                         customer_ordered_products.put(customer_name, current_productS);
                         jsarr.add(order);
                         
                     }
                     
               }           
               
           }
           Iterator iter=unique_ordered_products.keySet().iterator();
           StringBuffer unique_ordered_productsS=new StringBuffer();
           while(iter.hasNext()){
               String key=(String)iter.next();
               Integer  value= unique_ordered_products.get(key);
               System.out.println(key+"\t"+value);
               unique_ordered_productsS.append("<tr><td>"+key+"</td><td>"+value+"</td></tr>");
               
           }
            HashMap sortByValue=(HashMap)sortByValue(customer_ordered_products);
           //TreeMap sortByValue=customer_ordered_products;
            iter=sortByValue.keySet().iterator();
            StringBuffer customer_ordered_productsS=new StringBuffer();
            
            if(label_print){
                customer_ordered_productsS.append(" <style>  div {padding-top:10px;font-weight:bold;font-size:110%;padding:5px}</style></head><body style='margin:10px ;padding-top:10px;'> ");
            }
           while(iter.hasNext()){
               String key=(String)iter.next();
               String  value= (String)sortByValue.get(key);
               System.out.println(key+"\t"+value);
               if(label_print==false)
                 customer_ordered_productsS.append("<tr><td>"+key+"</td><td>"+value+"</td></tr>");
               else{
                   if((key+value).length()>300)
                       customer_ordered_productsS.append("<div style=font-size:70%;font-weight:normal> "+value+"<br/> "+key+"</div><p style='page-break-after:always;'></p><p><!-- pagebreak --></p>");
                   else
                      customer_ordered_productsS.append("<div > "+value+"<br/> "+key+"</div><p style='page-break-after:always;'></p><p><!-- pagebreak --></p>");                       
                   
               }
                  
               
              // System.out.println(customer_ordered_productsS);
              // System.out.println("Size of the string:"+customer_ordered_productsS.length());
                  
               
           }   
           if(request_type!=null && request_type.equals("customer_ordered_products"))
               return customer_ordered_productsS.toString();
           if(request_type!=null && request_type.equals("unique_ordered_products"))
               return unique_ordered_productsS.toString();
           
           return "";
    }
static Map sortByValue(Map map) {
     List list = new LinkedList(map.entrySet());
     Collections.sort(list, new Comparator() {
          public int compare(Object o1, Object o2) {
               return ((Comparable) ((Map.Entry) (o1)).getValue())
              .compareTo(((Map.Entry) (o2)).getValue());
          }
     });

    Map result = new LinkedHashMap();
    for (Iterator it = list.iterator(); it.hasNext();) {
        Map.Entry entry = (Map.Entry)it.next();
        result.put(entry.getKey(), entry.getValue());
    }
    return result;
}     
  public static String getJSONString(String strAPI) throws ServletException, IOException {
        String token = "5da566b3163d2f7e189a6aeedd9e0719b77124fc";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }        
}
