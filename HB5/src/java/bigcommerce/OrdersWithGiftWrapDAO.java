/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;


/**
 *
 * @author prabu
 */
public class OrdersWithGiftWrapDAO {
    public static void main(String args[]) throws ServletException, IOException{
        String str=getString();
        System.out.println("STR:"+str);
    }
    public static String getString() throws ServletException, IOException{
        StringBuffer retStr=new StringBuffer();

        JSONArray jsarr=new JSONArray();
            String webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=11&is_deleted=false&limit=250&page=1&sort=date_created:desc";
           String str=getJSONString(webPage);
           JSONArray tmpJsarr1=new JSONArray();
           if(str!=null && str.length()>0){
               tmpJsarr1=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr1.size();i++){
                   jsarr.add(tmpJsarr1.get(i));
               }           
               
           }
           if(tmpJsarr1.size()>250){
                webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=11&is_deleted=false&limit=250&page=2&sort=date_created:desc";
               str=getJSONString(webPage);
                tmpJsarr1=JSONArray.fromObject(str);  
                 for(int i=0;i<tmpJsarr1.size();i++){
                      jsarr.add(tmpJsarr1.get(i));
                 }                    
           }
                 
           
            webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=12&is_deleted=false&sort=date_created:desc";
           str=getJSONString(webPage);
           if(str!=null && str.length()>0){
               JSONArray tmpJsarr2=JSONArray.fromObject(str);
               for(int i=0;i<tmpJsarr2.size();i++){
                   jsarr.add(tmpJsarr2.get(i));
               }
           }
           
           webPage = "https://www.harrisburgstore.com/api/v2/orders.json?status_id=13&is_deleted=false&sort=date_created:desc";
           str=getJSONString(webPage);
          // System.out.println("Current JSON string:"+str);
           if(str!=null && str.length()>0){
               JSONArray tmpJsarr3=JSONArray.fromObject(str);

               for(int i=0;i<tmpJsarr3.size();i++){
                   jsarr.add(tmpJsarr3.get(i));
               }
               
           }
           retStr.append("<link rel='stylesheet' href='to_fba/common.css' type='text/css' />");
           retStr.append("<style>th,td { border:1px solid}</style>");
           retStr.append("<table width='90%'>");
           retStr.append("<tr><th>Order ID</th><th>Status</th><th>Order Date</th><th>Total </th><th>Staff Notes</th><th>Notes</th></tr>");
           for (int i=0;i<jsarr.size();i++){
               JSONObject js=(JSONObject) jsarr.get(i);
               Integer order_id=(Integer) js.get("id");
                String wrapping_cost_inc_tax=(String) js.get("wrapping_cost_inc_tax");
               if(order_id!=null && order_id.equals("14839")){
                   System.out.println("Debug");
               }
               if(Float.valueOf(wrapping_cost_inc_tax)==0){
                   continue;
               }
               String order_idStr="<a href='https://www.harrisburgstore.com/admin/index.php?ToDo=viewOrders&searchId=0&searchQuery="+order_id+"' target=_blank> "+order_id+"</a>";
               String date_created=(String) js.get("date_created");
               String total_inc_tax=(String) js.get("total_inc_tax");
               String payment_method=(String) js.get("payment_method");
               
                retStr.append("<tr>");
               retStr.append("<td>"+order_idStr+"</td>");
              
               retStr.append("<td>"+date_created+"</td>");
               retStr.append("<td>"+total_inc_tax+"</td>");
 

           }
           retStr.append("</table>"); 
           return retStr.toString();
    }
  public static String getJSONString(String strAPI) throws ServletException, IOException {
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }        
}
