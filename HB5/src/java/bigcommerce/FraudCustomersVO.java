/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

/**
 *
 * @author prabu
 */
public class FraudCustomersVO {
    String   Billing_Name,Billing_Address,Billing_City,Billing_State,Billing_Zip,Shipping_Name,Shipping_Address,Shipping_City,Shipping_State,Shipping_Zip,Email_ID ,ip_address,billing_city,shipping_city;

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getShipping_city() {
        return shipping_city;
    }

    public void setShipping_city(String shipping_city) {
        this.shipping_city = shipping_city;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getBilling_Address() {
        return Billing_Address;
    }

    public void setBilling_Address(String Billing_Address) {
        this.Billing_Address = Billing_Address;
    }

    public String getBilling_City() {
        return Billing_City;
    }

    public void setBilling_City(String Billing_City) {
        this.Billing_City = Billing_City;
    }

    public String getBilling_Name() {
        return Billing_Name;
    }

    public void setBilling_Name(String Billing_Name) {
        this.Billing_Name = Billing_Name;
    }

    public String getBilling_State() {
        return Billing_State;
    }

    public void setBilling_State(String Billing_State) {
        this.Billing_State = Billing_State;
    }

    public String getBilling_Zip() {
        return Billing_Zip;
    }

    public void setBilling_Zip(String Billing_Zip) {
        this.Billing_Zip = Billing_Zip;
    }

    public String getEmail_ID() {
        return Email_ID;
    }

    public void setEmail_ID(String Email_ID) {
        this.Email_ID = Email_ID;
    }

    public String getShipping_Address() {
        return Shipping_Address;
    }

    public void setShipping_Address(String Shipping_Address) {
        this.Shipping_Address = Shipping_Address;
    }

    public String getShipping_City() {
        return Shipping_City;
    }

    public void setShipping_City(String Shipping_City) {
        this.Shipping_City = Shipping_City;
    }

    public String getShipping_Name() {
        return Shipping_Name;
    }

    public void setShipping_Name(String Shipping_Name) {
        this.Shipping_Name = Shipping_Name;
    }

    public String getShipping_State() {
        return Shipping_State;
    }

    public void setShipping_State(String Shipping_State) {
        this.Shipping_State = Shipping_State;
    }

    public String getShipping_Zip() {
        return Shipping_Zip;
    }

    public void setShipping_Zip(String Shipping_Zip) {
        this.Shipping_Zip = Shipping_Zip;
    }
}
